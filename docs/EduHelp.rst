EduHelp package
===============

Submodules
----------

EduHelp.asgi module
-------------------

.. automodule:: EduHelp.asgi
   :members:
   :undoc-members:
   :show-inheritance:

EduHelp.settings module
-----------------------

.. automodule:: EduHelp.settings
   :members:
   :undoc-members:
   :show-inheritance:

EduHelp.urls module
-------------------

.. automodule:: EduHelp.urls
   :members:
   :undoc-members:
   :show-inheritance:

EduHelp.wsgi module
-------------------

.. automodule:: EduHelp.wsgi
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: EduHelp
   :members:
   :undoc-members:
   :show-inheritance:

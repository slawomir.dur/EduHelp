register.migrations package
===========================

Submodules
----------

register.migrations.0001\_initial module
----------------------------------------

.. automodule:: register.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

register.migrations.0002\_auto\_20210506\_2012 module
-----------------------------------------------------

.. automodule:: register.migrations.0002_auto_20210506_2012
   :members:
   :undoc-members:
   :show-inheritance:

register.migrations.0003\_auto\_20210506\_2014 module
-----------------------------------------------------

.. automodule:: register.migrations.0003_auto_20210506_2014
   :members:
   :undoc-members:
   :show-inheritance:

register.migrations.0004\_alter\_grade\_options module
------------------------------------------------------

.. automodule:: register.migrations.0004_alter_grade_options
   :members:
   :undoc-members:
   :show-inheritance:

register.migrations.0005\_coursepresence module
-----------------------------------------------

.. automodule:: register.migrations.0005_coursepresence
   :members:
   :undoc-members:
   :show-inheritance:

register.migrations.0006\_alter\_coursepresence\_student module
---------------------------------------------------------------

.. automodule:: register.migrations.0006_alter_coursepresence_student
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: register.migrations
   :members:
   :undoc-members:
   :show-inheritance:

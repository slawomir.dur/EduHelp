import { getCsrfToken } from './CsrfToken.js'

export async function fetchData(api_endpoint){
  const answer = await fetch(api_endpoint);
  return answer.json();
}

export async function postData(api_endpoint, object, on_success = (response) => {}, on_failure = (error) => {}){
  fetch(api_endpoint, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': getCsrfToken()
    },
    body: JSON.stringify(object)
  }).then(response=>{
    if (!response.ok){
      throw Error(response.statusText);
    }
    return response;
  }).then(response=>{
    on_success(response)
  }).catch(error=>on_failure(error));
}

function allYears() {
  return API.get('register_api/years/');
}

function allSemesters() {
  return API.get('register_api/semesters/');
}

export class API {
  static async get(api_endpoint) {
    return fetchData(api_endpoint);
  }

  static async post(api_endpoint, object, on_success = null, on_failure = null) {
    if (on_success == null) {
      on_success = () => {};
    }
    if (on_failure == null) {
      on_failure = (error) => {this.standardOnFailure(error)};
    }
    return postData(api_endpoint, object, on_success, on_failure)
  }

  static async standardOnFailure(error) {
    alert(error);
  }

  static async ownID(){
    return this.get('register_api/own_id/');
  }

  static async currentUser() {
    return this.get('/register_api/current_user/');
  }

  static async currentYear() {
    return this.get('/register_api/current_year/');
  }

  static async currentSemester() {
    return this.get('/register_api/current_semester/');
  }

  static async semestersForYear(year) {
    return this.get(`/register_api/semesters/${year}/`);
  }

  static async gradeValuesTypes() {
    return this.get('/register_api/grade_values_types/');
  }

  static async lessonsForCourse(course) {
    return this.get(`/register_api/lessons/${course}/`);
  }

  static async sendMessage(object, on_success = null, on_failure = null) {
    return this.post('/register_api/send_message/', object, on_success, on_failure);
  }

  static async gradesForCourse(course, student = null) {
    if (student == null) {
      return this.get(`/register_api/grades_for_course/${course}/`)
    }
    return this.get(`/register_api/grades_for_course/${course}/${student}/`)
  }

  static async lessonsForCourse(course) {
    return this.get(`/register_api/lessons/${course}`);
  }

  static async homeworkForCourse(course) {
    return this.get(`/register_api/homework/${course}`);
  }

  static async testsForCourse(course) {
    return this.get(`/register_api/tests/${course}`);
  }
}

export class StudentAPI extends API {
  static async allYears() {
    return allYears();
  }

  static async allYearsForStudent(student) {
    return this.get(`register_api/years/${student}`);
  }

  static async allSemesters() {
    return allSemesters();
  }

  static async courses(semester = null) {
    if (semester == null) {
      return this.get('register_api/courses/');
    }
    return this.get(`register_api/courses/${semester}/`);
  }

  static async coursesForStudent(student, semester = null) {
    if (semester == null) {
      return this.get(`register_api/courses_for_student/${student}/`)
    }
    return this.get(`register_api/courses_for_student/${student}/${semester}/`);
  }
  
  static async remarks() {
    return this.get('register_api/remarks/');
  }

  static async remarksForStudent(student) {
    return this.get(`register_api/remarks_for_student/${student}/`);
  }
}

export class TeacherAPI extends API {
  static async allYears() {
    return allYears();
  }

  static async allSemesters() {
    return allSemesters();
  }

  static async allClasses() {
    return this.get('/register_api/teacher/all_classes/');
  }

  static async classes() {
    return this.get('/register_api/teacher/classes/');
  }

  static async classesForSemester(semester) {
    return this.get(`/register_api/teacher/classes/${semester}/`);
  }

  static async coursesForClass(class_) {
    return this.get(`/register_api/teacher/courses/${class_}/`);
  }

  static async coursesForClassAndSemester(class_, semester) {
    return this.get(`/register_api/teacher/courses/${class_}/${semester}/`);
  }

  static async topics(subject, level) {
    return this.get(`/register_api/teacher/topics/${subject}/${level}/`);
  }

  static async students(class_) {
    return this.get(`/register_api/teacher/students/${class_}/`);
  }

  static async newLesson(object, on_success = null, on_failure = null) {
    return this.post('/register_api/teacher/new_lesson/', object, on_success, on_failure);
  }

  static async remarks(class_) {
    return this.get(`/register_api/remarks/${class_}/`);
  }

  static async addRemark(object, on_success = null, on_failure = null) {
    return this.post('/register_api/remark/create/', object, on_success, on_failure);
  }

  static async addGrade(object, on_success = null, on_failure = null) {
    return this.post('/register_api/grade/add/', object, on_success, on_failure);
  }

  static async deleteGrade(object, on_success = null, on_failure = null) {
    return this.post('/register_api/grade/delete/', object, on_success, on_failure);
  }

  static async editLesson(object, on_success = null, on_failure = null) {
    return this.post('/register_api/lesson/edit/', object, on_success, on_failure);
  }

  static async deleteLesson(object, on_success = null, on_failure = null) {
    return this.post('/register_api/lesson/delete/', object, on_success, on_failure);
  }

  static async addHomework(object, on_success = null, on_failure = null) {
    return this.post('/register_api/teacher/new_homework/', object, on_success, on_failure);
  }

  static async addTest(object, on_success = null, on_failure = null) {
    return this.post('/register_api/teacher/new_test/', object, on_success, on_failure);
  }
}


export class CaretakerAPI extends API {
  static async students() {
    return this.get('register_api/caretaker/students/');
  }
}
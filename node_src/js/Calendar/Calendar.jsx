import React, {Component} from 'react';

import './style.css';
import { modes } from './utils.js'
import {Switch} from './Switch.jsx'
import { DayTable } from './DayTable.jsx'
import { Card, Container, Row, Col } from 'react-bootstrap';
import { WeekMonthToggle } from './WeekMonthToggle.jsx';



export class Calendar extends Component {

    constructor(props) {
        super(props);
        const today = new Date();
        this.state = {
            today: today,
            displayed_month: {
                month: today.getMonth(),
                year: today.getFullYear()
            },
            mode: modes.month
        }
    }

    onPreviousMonth = () => {
        let month = this.state.displayed_month.month - 1;
        let year = this.state.displayed_month.year;
        if (month == -1) {
            month = 11;
            year = year - 1;
        }
        this.setState(() => {
            return {
                displayed_month: {
                    month: month,
                    year: year
                }
            }
        })
    }

    onNextMonth = () => {
        let month = this.state.displayed_month.month + 1;
        let year = this.state.displayed_month.year;
        if (month == 12) {
            month = 0;
            year = year + 1;
        }
        this.setState(() => {
            return {
                displayed_month: {
                    month: month,
                    year: year
                }
            }
        })
    }

    onMonthClick = () => {
        this.setState(() => {
            return {
                mode: modes.month
            }
        })
    }

    onWeekClick = () => {
        this.setState(() => {
            return {
                mode: modes.week
            }
        })
    }

    render() {
        return (
            <Card>
                <Card.Header>
                    <Container>
                        <Row>
                            <Col sm={4}><Switch disabled={this.state.mode==modes.week} date={this.state.displayed_month} onPrevious={this.onPreviousMonth.bind(this)} onNext={this.onNextMonth.bind(this)}/></Col>
                            <Col sm={5}></Col>
                            <Col sm={3}><WeekMonthToggle onMonthClick={this.onMonthClick.bind(this)} onWeekClick={this.onWeekClick.bind(this)} /></Col>
                        </Row>
                    </Container>
                </Card.Header>
                <Card.Body>
                    <DayTable 
                        mode={this.state.mode} 
                        displayed_month={this.state.displayed_month} 
                        today={this.state.today} 
                        events={this.props.events}
                        onEditEventClick={this.props.onEditEventClick?.bind(this)}
                        onRemoveEventClick={this.props.onRemoveEventClick?.bind(this)}
                        allowEditing={this.props.allowEditing}
                        allowRemoval={this.props.allowRemoval}
                    />
                </Card.Body>
            </Card>
        )
    }
}

import React, {Component} from 'react';
import { Table, Card, Button } from 'react-bootstrap';
import './style.css'
import { daysInMonth, getDayOfWeek, modes, getLastMonday } from './utils.js';

export class DayTable extends Component {

    getRows = () => {
        if (this.props.mode == modes.month) {
            return this.getMonthRows();
        }
        return this.getWeekRows();
    }

    getMonthRows = () => {
        const days = daysInMonth(this.props.displayed_month.month, this.props.displayed_month.year);
        const startDate = new Date(this.props.displayed_month.year, this.props.displayed_month.month, 1);
        return this.getRowsInternal(days, startDate);
    }

    getWeekRows = () => {
        const days = 7;
        const startDate = getLastMonday(this.props.today);
        return this.getRowsInternal(days, startDate);
    }

    getRowsInternal = (days, startDate) => {
        // const daysMonth = daysInMonth(startDate.getMonth(), startDate.getFullYear());
        let rows = [];
        for (let i = 0; i < days; ++i) {
            const day = startDate.getDate()+i;
            const date = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()+i);
            let eventsForThisDay = []
            if (this.props.events) {
                eventsForThisDay = this.props.events.filter((event) => {
                    const eventDate = new Date(Date.parse(event.date));
                    const dayEqual = date.getDate() == eventDate.getDate();
                    const monthEqual = date.getMonth() == eventDate.getMonth();
                    const yearEqual = date.getFullYear() == eventDate.getFullYear();
                    return dayEqual && monthEqual && yearEqual;
                })
            }
            rows.push({
                date: date,
                events: eventsForThisDay
            })
        }
        return rows;
    }

    render() {
        const rows = this.getRows();
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Wydarzenia</th>
                        <th>Dzień tygodnia</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        rows.map(row => {
                            return(
                                <DayRow 
                                    date={row.date} 
                                    events={row.events} 
                                    onEditClick={this.props.onEditEventClick?.bind(this)} 
                                    onRemoveClick={this.props.onRemoveEventClick?.bind(this)}
                                    allowEditing={this.props.allowEditing}
                                    allowRemoval={this.props.allowRemoval}
                                />
                            )
                        })
                    }
                </tbody>
            </Table>
        )
    }
}

class DayRow extends Component {
    render() {
        const date = this.props.date;
        const day = getDayOfWeek(date); 
        return (
            <tr>
                <td>{date.toLocaleDateString()}</td>
                <td>
                    <div style={{display: 'flex'}}>
                    {
                        this.props.events.map(event=>{
                            return (
                                <Event
                                    id={event.id}
                                    title={event.title}
                                    subtitle={event.subtitle}
                                    description={event.description}
                                    text={event.text}
                                    allowEditing={this.props.allowEditing}
                                    allowRemoval={this.props.allowRemoval}
                                    onEditClick={this.props.onEditClick?.bind(this)}
                                    onRemoveClick={this.props.onRemoveClick?.bind(this)}
                                />
                            )
                        })
                    }
                    </div>
                </td>
                <td>{day}</td>
            </tr>
        )
    }
}

class Event extends Component {
    render() {
        const eventData = {
            id: this.props.id,
            title: this.props.title,
            description: this.props.description,
            text: this.props.text,
            allowEditing: this.props.allowEditing,
            allowRemoval: this.props.allowRemoval
        }
        let editBtn = null;
        let removeBtn = null;
        if (this.props.allowEditing == true) {
            editBtn = <Button className="mr-3" size="sm" variant="success" onClick={()=>{this.props.onEditClick?.(eventData)}}>Edytuj</Button>
        }
        if (this.props.allowRemoval == true) {
            removeBtn = <Button className="ml-3" size="sm" variant="danger" onClick={()=>{this.props.onRemoveClick?.(eventData)}}>Usuń</Button>
        }
        return (
            <div className="c_box">
                <div className="c_title_box">
                    <p className="c_title">
                        {this.props.title}
                    </p>
                    <p className="c_subtitle">
                        {this.props.subtitle}
                    </p>
                    <p className="c_description">
                        {this.props.description}
                    </p>
                </div>
                {editBtn}
                {removeBtn}
            </div>
        )
    }
}
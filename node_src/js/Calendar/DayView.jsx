import React, {Component} from 'react';
import './style.css';

export class DayView extends Component {
    render() {
        return (
            <div className="c_dayview" onClick={()=>this.props.onClick()}>
                <p>{this.props.day}</p>
                <div className="c_dayview_eventbox">
                {
                    this.props.events.map((event) => {
                        return (
                            <Event info={event} />
                        )
                    })
                }
                </div>
            </div>
        )
    }
}

class Event extends Component {
    render() {
        return (
            <div className="c_dayview_event">
                <div className="c_dayview_event_title">{this.props.info.title}</div>
                <p className="c_dayview_event_span">{this.props.info.from_hour} - {this.props.info.to_hour}</p>
            </div>
        )
    }
}
import React, { Component } from 'react';
import { Container, Row, Col, Button, Alert } from 'react-bootstrap';

import './style.css';
import { getMonth, months } from './utils.js';


export class Switch extends Component {
    render() {
        let month = null;
        if (this.props.date) {
            month = months[this.props.date.month]
        }
        let prevBtn = <Button variant="outline-dark" onClick={() => this.props.onPrevious?.()}>&lt;</Button>
        let nextBtn = <Button variant="outline-dark" onClick={() => this.props.onNext?.()}>&gt;</Button>
        if (this.props.disabled == true) {
            prevBtn = <Button disabled variant="outline-dark" onClick={() => this.props.onPrevious?.()}>&lt;</Button>
            nextBtn = <Button disabled variant="outline-dark" onClick={() => this.props.onNext?.()}>&gt;</Button>
        }
        return (
            <Container>
                <Row>
                    <Col>{prevBtn}</Col>
                    <Col>{month}</Col>
                    <Col>{nextBtn}</Col>
                    <Col>{this.props?.date.year}</Col>
                </Row>
            </Container>
        )
    }
}
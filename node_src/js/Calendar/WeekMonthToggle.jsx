import React, { Component } from 'react';
import './style.css'

import {ButtonGroup, ToggleButton} from 'react-bootstrap';


const radios = [
    {name: 'Miesiąc', value: '1'},
    {name: 'Tydzień', value: '2'}
]

export class WeekMonthToggle extends Component {
    constructor(props){
        super(props)
        this.state = {
            radioValue: '1'
        }
    }

    setRadioValue = (radioValue) => {
        this.setState(() => {
            return {
                radioValue: radioValue
            }
        })
    }

    render() {
        return (
            <ButtonGroup className="mb-2">
                <ToggleButton
                    key={'1'}
                    id={`radio-1`}
                    type="radio"
                    variant="outline-success"
                    name="radio"
                    value={'1'}
                    checked={this.state.radioValue == '1'}
                    onChange={(e) => {
                        this.setRadioValue(e.currentTarget.value);
                        this.props.onMonthClick?.();
                    }}
                >
                Miesiąc
                </ToggleButton>
                <ToggleButton
                    key={'2'}
                    id={`radio-2`}
                    type="radio"
                    variant="outline-success"
                    name="radio"
                    value={'2'}
                    checked={this.state.radioValue == '2'}
                    onChange={(e) => {
                        this.setRadioValue(e.currentTarget.value);
                        this.props.onWeekClick?.();
                    }}
                >
                Aktualny tydzień
                </ToggleButton>
            </ButtonGroup>
        )
    }
}
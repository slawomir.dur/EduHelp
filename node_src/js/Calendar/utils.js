export function today() {
    return new Date();
}

export const daysOfWeek = {
    0: 'Niedziela',
    1: 'Poniedziałek',
    2: 'Wtorek',
    3: 'Środa',
    4: 'Czwartek',
    5: 'Piątek',
    6: 'Sobota'
}

export const months = {
    0: 'styczeń',
    1: 'luty',
    2: 'marzec',
    3: 'kwiecień',
    4: 'maj',
    5: 'czerwiec',
    6: 'lipiec',
    7: 'sierpień',
    8: 'wrzesień',
    9: 'październik',
    10: 'listopad',
    11: 'grudzień'
}

export function daysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
}

export function getDayOfWeek(date) {
    const day = date.getDay();
    return daysOfWeek[day];
}

export function getMonth(date) {
    const month = date.getMonth();
    return months[month];
}

export const modes = {
    month: 'month',
    week: 'week'
}

export function getLastMonday(date) {
    let day = date.getDate() - date.getDay();
    let month = date.getMonth();
    let year = date.getFullYear();
    if (day < 0) {
        month = month - 1;
        if (month < 0) {
            month = 11;
            year = year - 1;
        }
        day = daysInMonth(month, year) - 1
    }
    return new Date(year, month, day);
}
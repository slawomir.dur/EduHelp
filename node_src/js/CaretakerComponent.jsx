import React from 'react'
import { Col, Row, Container, Tabs, Tab } from 'react-bootstrap';
import { CaretakerAPI } from './APICalls.js';
import { mapping } from './ComponentMapping.js'
import { MainComponent } from './MainComponent.jsx'
import { find, proceed_if_has_one } from './Utilities.js';
import { ComboBox } from './Components.jsx'

export class CaretakerComponent extends MainComponent {
    constructor(props) {
        super(props, mapping.CARETAKER);

        this.state = {
            students: null
        }
        this.fetch_initial_data();
    }

    fetch_initial_data = async () => {
        const students = await CaretakerAPI.students();
        proceed_if_has_one(students, 'students', () => {
            const chosen_student = students.students[0];
            this.setState(() => {
                return {
                    students: students.students,
                    chosen_student: chosen_student.id
                }
            })
        }, () => {alert('Brak przypisanych uczniów.')})
    }

    set_student = (student_) => {
        let student = find(student_, this.state.students);
        this.setState(() => {
          return {
            chosen_student: student
          }
        })
      }

    render() {
        if (this.state.chosen_student) {
            return (
                <Container>
                    <Row className="mb-3">
                        <Col md>
                            <ComboBox 
                                id={"floatingStudentGrid"}
                                label={"Uczeń"} 
                                aria_label={"Student select"} 
                                initial_value={this.state.chosen_student.id} 
                                onChange={this.set_student.bind(this)} 
                                things={this.state.students} 
                                value_prop={"id"} 
                                repr_prop={"name_surname"} 
                            />
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Col md>
                            <Tabs className="mb-3">
                                {
                                    Object.keys(this.components).map(value => {
                                        const Component = this.components[value]['component'];
                                        return (
                                            <Tab eventKey={value} title={this.components[value]['title']}>
                                                <Component mode={this.mode} student={this.state.chosen_student}/>
                                            </Tab>
                                        )
                                    })
                                }
                            </Tabs>
                        </Col>
                    </Row>
                </Container>
            )
        } else {
            return null;
        }
    }
}
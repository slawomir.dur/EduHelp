export const mapping = {
    TEACHER: "teacher_component",
    STUDENT: "student_component",
    CARETAKER: "caretaker_component"
}
import {
  Modal as ReactModal,
  FloatingLabel,
  Form,
  Toast as ReactToast,
  ToastContainer,
  Col,
  Row,
  Container,
  Button,
  Popover,
  Overlay,
  Table,
  Tabs,
  Tab,
  Accordion
} from 'react-bootstrap'
import React, { Component } from 'react'
import { fetchData, postData, API, TeacherAPI, StudentAPI } from './APICalls.js'
import { proceed_if_has_one, find } from './Utilities.js'
import { mapping } from './ComponentMapping.js'
import { COURSES_ERROR, CLASSES_ERROR, SEMESTERS_ERROR, DB_WRITE_SUCCESS_INFO, DB_WRITE_ERROR_INFO } from './Strings.js'
import {Calendar} from './Calendar/Calendar.jsx'

export class Modal extends Component {
  render() {
    return (
      <ReactModal
        show={this.props.show_modal}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <ReactModal.Header closeButton onClick={() => { this.props.close() }}>
          <ReactModal.Title id="contained-modal-title-vcenter">
            {this.props.title}
          </ReactModal.Title>
        </ReactModal.Header>
        <ReactModal.Body>
          {this.props.body}
        </ReactModal.Body>
        <ReactModal.Footer>
          {this.props.footer}
        </ReactModal.Footer>
      </ReactModal >
    )
  }
}

export class ComboBox extends Component {
  render() {
    return (
      <FloatingLabel controlId={this.props.id} label={this.props.label}>
        <Form.Select aria-label={this.props.aria_label} value={this.props.initial_value} onChange={(e) => this.props.onChange(e.target.value)}>
          {
            this.props.things &&
            this.props.things.map(c => {
              return (
                <option key={c[this.props.value_prop]} value={c[this.props.value_prop]}>{c[this.props.repr_prop]}</option>
              )
            })
          }
        </Form.Select>
      </FloatingLabel>
    )
  }
}

export class LineEdit extends Component {
  render() {
    return (
      <Form.Control defaultValue={this.props.defaultValue} value={this.props.value} onChange={(e) => this.props.onChange(e.target.value)} />
    )
  }
}

export class TextEdit extends Component {
  render() {
    return (
      <Form.Control defaultValue={this.props.defaultValue} value={this.props.value} style={{ height: "10.0rem" }} as="textarea" onChange={(e) => this.props.onChange(e.target.value)} />
    )
  }
}

export class Toast extends Component {
  render() {
    return (
      <ToastContainer position="bottom-end">
        <ReactToast show={this.props.show_toast} onClose={() => { this.props.onClose() }}>
          <ReactToast.Header>
            {this.props.header}
          </ReactToast.Header>
          <ReactToast.Body>{this.props.body}</ReactToast.Body>
        </ReactToast>
      </ToastContainer>
    )
  }
}

const mode = {
  CREATE_NEW: 'CREATE_NEW',
  EDIT_EXISTING: 'EDIT_EXISTING'
}

export class GradeEditView extends Component {
  mode;

  constructor(props) {
    super(props);

    if (this.props.class && this.props.course && this.props.student && this.props.value && this.props.type) {
      this.mode = mode.EDIT_EXISTING;
    } else {
      this.mode = mode.CREATE_NEW;
    }

    if (this.mode == mode.EDIT_EXISTING) {
      this.state = {
        value: this.props.value,
        type: this.props.type,
        chosen_class: this.props.class,
        chosen_course: this.props.course,
        chosen_student: this.props.student,
        show_modal: false,
        show_toast: false,
        description: "",
        grade_id: this.props.grade_id
      }
    } else {
      this.state = {
        values: null,
        value: null,
        types: null,
        type: null,
        classes: null,
        chosen_class: null,
        courses_with_class: null,
        chosen_course: null,
        students: null,
        chosen_student: null,
        description: "",
        show_modal: false,
        show_toast: false
      }
    }
    this.fetch_initial_data(this.mode);
  }

  create_initial_state = (mode_) => {
    if (mode_ == mode.EDIT_EXISTING) {
      this.state = {
        value: this.props.value,
        type: this.props.type,
        chosen_class: this.props.class,
        chosen_course: this.props.course,
        chosen_student: this.props.student,
        show_modal: false,
        show_toast: false,
        description: ""
      }
    } else {
      this.state = {
        values: null,
        value: null,
        types: null,
        type: null,
        classes: null,
        chosen_class: null,
        courses_with_class: null,
        chosen_course: null,
        students: null,
        chosen_student: null,
        description: "",
        show_modal: false,
        show_toast: false
      }
    }
  }

  fetch_initial_data = async (mode_) => {
    if (mode_ == mode.CREATE_NEW) {
      const classes_json = await fetchData('/register_api/teacher/classes/');
      const courses_json = await fetchData(`/register_api/teacher/courses/${classes_json.classes[0].id}`);
      const students_json = await fetchData(`/register_api/teacher/students/${classes_json.classes[0].id}`);
      const grade_values_types_json = await fetchData('/register_api/grade_values_types/');

      this.setState(() => {
        return {
          values: grade_values_types_json.values,
          types: grade_values_types_json.types,
          value: grade_values_types_json.values[0].value,
          type: grade_values_types_json.types[0].id,
          students: students_json.students,
          chosen_student: students_json.students[0],
          classes: classes_json.classes,
          chosen_class: classes_json.classes[0],
          courses_with_class: courses_json.courses,
          chosen_course: courses_json.courses[0]
        }
      });
    } else {
      const grade_values_types_json = await fetchData('/register_api/grade_values_types/');
      this.setState(() => {
        return {
          values: grade_values_types_json.values,
          types: grade_values_types_json.types
        }
      })
    }
  }

  set_students = (students_) => {
    this.setState(() => {
      return {
        students: students_,
        chosen_student: students_[0].id
      }
    })
  }

  set_student = (student_) => {
    let student = find(student_, this.state.students);
    this.setState(() => {
      return {
        chosen_student: student
      }
    })
  }


  set_grade_value = (value) => {
    this.setState(() => {
      return {
        value: value
      }
    })
  }

  set_grade_type = (type) => {
    this.setState(() => {
      return {
        type: type
      }
    })
  }

  set_classes = (classes_) => {
    this.setState(() => {
      return {
        classes: classes_
      }
    });
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  set_class = async (class_) => {
    const courses = await fetch(`/register_api/teacher/courses/${class_.id}`);
    const courses_json = await courses.json();
    const students = await fetch(`/register_api/teacher/students/${class_.id}`);
    const students_json = await students.json();


    this.setState(() => {
      return {
        chosen_class: class_,
        courses_with_class: courses_json.courses,
        chosen_course: courses_json.courses[0],
        students: students_json.students,
        chosen_student: students_json.students[0].id
      }
    });
  }

  change_class = (class_id) => {
    for (let class_ of this.state.classes) {
      if (class_.id == class_id) {
        this.set_class(class_);
        break;
      }
    }
  }

  set_courses = (courses_) => {
    this.setState(() => {
      return {
        courses_with_class: courses_
      }
    })
  }

  change_course = (course_id) => {
    for (let course_ of this.state.courses_with_class) {
      if (course_.id == course_id) {
        this.set_course(course_);
        break;
      }
    }
  }

  set_course = (course_) => {
    this.setState(() => {
      return {
        chosen_course: course_
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  change_grade = (grade) => {
    this.setState(() => {
      return {
        value: grade
      }
    })
  }

  change_type = (type_) => {
    console.log(type_);
    this.setState(() => {
      return {
        type: type_
      }
    })
  }

  set_description = (description_) => {
    this.setState(() => {
      return {
        description: description_
      }
    })
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    });
  }

  post_edit = () => {
    if (this.props.post_edit !== undefined) {
      this.props.post_edit();
    }
  }

  proceed_submit = (boolVal) => {
    if (boolVal) {
      if (this.mode == mode.CREATE_NEW) {
        postData('/register_api/grade/add/', {
          student: this.state.chosen_student,
          course: this.state.chosen_course,
          grade: this.state.values[this.state.value - 1],
          type: this.state.types[this.state.type - 1],
          description: this.state.description
        }, () => { this.show_toast(true); this.post_edit(); }, (e) => alert(e))
      } else {
        postData('/register_api/grade/add/', {
          student: this.state.chosen_student,
          course: this.state.chosen_course,
          grade: this.state.values[this.state.value - 1],
          type: this.state.types[this.state.type - 1],
          description: this.state.description,
          grade_id: this.state.grade_id
        }, () => { this.show_toast(true); this.post_edit(); }, (e) => alert(e))
      }
    }
  }

  show_toast = (boolVal) => {
    this.setState(() => {
      return {
        show_toast: boolVal
      }
    });
  }

  render() {
    let middle = null;
    if (this.mode == mode.CREATE_NEW) {
      middle = this.state.classes && this.state.courses_with_class && this.state.students && <Container><Row className="mb-3">
        <Col md>
          <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.change_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
        </Col>
        <Col md>
          <ComboBox id={"floatingCourseGrid"} label={"Kurs"} aria_label={"Course select"} initial_value={this.state.chosen_course.id} onChange={this.change_course.bind(this)} things={this.state.courses_with_class} value_prop={"id"} repr_prop={"name"} />
        </Col>
      </Row>
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingStudentGrid"} label={"Uczeń"} aria_label={"Student select"} initial_value={this.state.chosen_student.id} onChange={this.set_student.bind(this)} things={this.state.students} value_prop={"id"} repr_prop={"name_surname"} />
          </Col>
        </Row>
      </Container>
    } else {
      middle = <div></div>
    }

    if (this.state.chosen_class && this.state.chosen_course && this.state.chosen_student && this.state.values && this.state.types && this.state.value && this.state.type) {
      let toast_header = (this.mode == mode.CREATE_NEW) ? "Dodawanie oceny" : "Edytowanie oceny";
      let toast_body = (this.mode == mode.CREATE_NEW) ? "Dodawanie oceny zakończone pomyślnie" : "Edytowanie oceny zakończone pomyślnie";
      return (
        this.state.chosen_class && this.state.chosen_course && this.state.chosen_student && this.state.values && this.state.types && this.state.value && this.state.type &&
        <div>
          <Toast show_toast={this.state.show_toast} header={toast_header} body={toast_body} onClose={() => this.show_toast(false)} />
          <ReactModal
            show={this.state.show_modal}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <ReactModal.Header closeButton onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>
              <ReactModal.Title id="contained-modal-title-vcenter">
                Wystawianie oceny
              </ReactModal.Title>
            </ReactModal.Header>
            <ReactModal.Body>
              <h4>Podsumowanie</h4>
              <div>
                <p>Klasa: {this.state.chosen_class.str_name}</p>
                <p>Uczeń: {this.state.chosen_student.name} {this.state.chosen_student.surname}</p>
                <p>Przedmiot: {this.state.chosen_course.subject.name}</p>
                <p>Ocena: {this.state.values[this.state.value - 1].name}</p>
                <p>Rodzaj oceny: {this.state.types[this.state.type - 1].name}</p>
                <p>Opis: {this.state.description}</p>
              </div>
            </ReactModal.Body>
            <ReactModal.Footer>
              <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(true); }}>Zapisz</Button>
              <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>Anuluj</Button>
            </ReactModal.Footer>
          </ReactModal>
          <Form onSubmit={(event) => this.on_submit(event)}>
            {middle}
            <Container>
              <Row className="mb-3">
                <Col md>
                  <ComboBox id={"floatingGradeValueGrid"} label={"Ocena"} aria_label={"Grade select"} initial_value={this.state.value} onChange={this.change_grade.bind(this)} things={this.state.values} value_prop={"value"} repr_prop={"name"} />
                </Col>
                <Col md>
                  <ComboBox id={"floatingGradeTypeId"} label={"Rodzaj oceny"} aria_label={"GradeType select"} initial_value={this.state.type} onChange={this.change_type.bind(this)} things={this.state.types} value_prop={"id"} repr_prop={"name"} />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col md>
                  <Form.Label>Opis oceny</Form.Label>
                  <LineEdit onChange={this.set_description.bind(this)} />
                </Col>
              </Row>
              <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Wystaw ocenę</Button>
            </Container>
          </Form>
        </div>
      );
    } else {
      return null;
    }



  }
}


export class ClassGradesView extends Component {
  constructor(props) {
    super(props);
  }


}

export class SingleGradeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      grade_to_edit: null,
      show_modal: null,
      edit: null,
      remove: null,
      show_overlay: false,
      target: null
    }
    this.overlay_target = React.createRef();
  }

  on_edit = (grade) => {
    this.setState(() => {
      return {
        grade_to_edit: grade,
        show_modal: true,
        edit: true,
        remove: false,
        show_overlay: false
      }
    })
  }

  on_remove = (grade) => {
    this.setState(() => {
      return {
        grade_to_edit: grade,
        show_modal: true,
        edit: false,
        remove: true,
        show_overlay: false
      }
    })
  }

  set_show_overlay_and_target = (boolVal, target) => {
    this.setState(() => {
      return {
        target: target,
        show_overlay: boolVal
      }
    })
  }

  post_edit = () => {
    if (this.props.post_edit !== undefined) {
      this.props.post_edit();
      this.setState(() => {
        return {
          show_modal: false
        }
      })
    }
  }

  delete_grade = () => {
    postData('/register_api/grade/delete/', {
      grade_id: this.state.grade_to_edit.id
    }, () => { alert('Poprawnie usunięto ocenę'); this.post_edit(); }, (error) => alert(error))
  }

  cancel_deleting = () => {

  }

  render() {
    let title = null;
    let body = null;
    let footer = null;
    if (this.state.edit) {
      title = `Edycja oceny ${this.state.grade_to_edit.type.name}: ${this.state.grade_to_edit.value.name}`;
      body = <GradeEditView class={this.props.class_} course={this.props.course} student={this.props.student} value={this.props.grade.value.value} type={this.props.grade.type.id} post_edit={this.post_edit.bind(this)} grade_id={this.state.grade_to_edit.id} />

    } else if (this.state.remove) {
      title = `Usuwanie oceny ${this.state.grade_to_edit.type.name}: ${this.state.grade_to_edit.value.name}`;
      body = <p>Czy na pewno chcesz usunąć ocenę?</p>
      footer = <span>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.delete_grade()}>Usuń</Button>
        <Button style={{ margin: "0.2rem" }} onClick={() => { this.setState(() => { return { show_modal: false, edit: false, remove: false } }) }}>Anuluj</Button>
      </span>
    }

    let edit_widget = null;
    if (this.props.allow_edit) {
      edit_widget = <Col md>
        <Button onClick={() => this.on_edit(this.props.grade)} variant="success" size="sm" style={{ margin: "0.1rem" }}>Edytuj</Button>
        <Button onClick={() => this.on_remove(this.props.grade)} variant="danger" size="sm" style={{ margin: "0.1rem" }}>Usuń</Button>
      </Col>
    }

    return (
      <div ref={this.overlay_target}>
        <Modal show_modal={this.state.show_modal} close={() => { this.setState(() => { return { show_modal: false, edit: false, remove: false } }) }} title={title} body={body} footer={footer} />
        <div className="edu-tooltip" onClick={(e) => { this.set_show_overlay_and_target(!this.state.show_overlay, e.target); }} style={{ backgroundColor: `${this.props.grade.type.color}`, padding: "0.2rem 0.4rem", margin: "4px", cursor: "pointer" }}>{this.props.grade.value.value}</div>
        <Overlay
          container={this.overlay_target}
          containerPadding={20}
          placement="top"
          show={this.state.show_overlay}
          target={this.state.target}
        >
          <Popover>
            <Popover.Header as="h3">{this.props.grade.type.name}: {this.props.grade.value.name}</Popover.Header>
            <Popover.Body>
              <strong>Wartość: </strong>{this.props.grade.value.value}<br />
              <strong>Nazwa: </strong>{this.props.grade.value.name}<br />
              <strong>Rodzaj: </strong>{this.props.grade.type.name}<br />
              <strong>Waga: </strong>{this.props.grade.type.value}<br />
              <strong>Opis: </strong>{this.props.grade.description}<br />
              {edit_widget}
            </Popover.Body>
          </Popover>
        </Overlay>
      </div>


    )
  }
}

export class CaretakersAddresseeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      caretakers: null,
      chosen_caretaker: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_data();
    } else {
      this.fetch_student_data();
    }
  }

  fetch_teacher_data = async () => {
    const all_classes = await fetchData('/register_api/teacher/all_classes/');
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const caretakers = await fetchData(`/register_api/caretakers/${class_.id}`);
      proceed_if_has_one(caretakers, 'caretakers', () => {
        const caretaker = caretakers.caretakers[0];
        this.setState(() => {
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            caretakers: caretakers.caretakers,
            chosen_caretaker: caretaker
          }
        })
        this.props.set_addressee(caretaker);
      });
    });
  }

  fetch_student_data = async () => {

  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const caretakers = await fetchData(`/register_api/caretakers/${class_.id}`);
    proceed_if_has_one(caretakers, 'caretakers', () => {
      const caretaker = caretakers.caretakers[0];
      this.setState(() => {
        return {
          chosen_class: class_,
          caretakers: caretakers.caretakers,
          chosen_caretaker: caretaker
        }
      })
      this.props.set_addressee(caretaker);
    });
  }

  set_caretaker = (caretaker_id) => {
    let caretaker = find(caretaker_id, this.state.caretakers);
    this.setState(() => {
      return {
        chosen_student: caretaker
      }
    });
    this.props.set_addressee(caretaker);
  }

  render() {
    if (this.state.caretakers && this.state.chosen_caretaker && this.state.chosen_class && this.state.classes) {
      return (
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
          </Col>
          <Col md>
            <ComboBox id={"floatingStudentsGrid"} label={"Rodzic"} aria_label={"Caretaker select"} initial_value={this.state.chosen_caretaker.id} onChange={this.set_caretaker.bind(this)} things={this.state.caretakers} value_prop={"id"} repr_prop={"name_surname"} />
          </Col>
        </Row>
      )
    } else {
      return null;
    }
  }
}

export class TeachersAddresseeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teachers: null,
      chosen_teacher: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const teachers = await fetchData('/register_api/all_teachers/');
    proceed_if_has_one(teachers, 'teachers', () => {
      const teacher = teachers.teachers[0];
      this.setState(() => {
        return {
          teachers: teachers.teachers,
          chosen_teacher: teacher
        }
      });
      this.props.set_addressee(teacher);
    }, () => alert('No teachers'));
  }

  set_teacher = (teacher_id) => {
    const teacher = find(teacher_id, this.state.teachers);
    this.setState(() => {
      return {
        chosen_teacher: teacher
      }
    });
    this.props.set_addressee(teacher);
  }

  render() {
    if (this.state.teachers && this.state.chosen_teacher) {
      return (
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingTeachersGrid"} label={"Nauczyciel"} aria_label={"Teacher select"} initial_value={this.state.chosen_teacher.id} onChange={this.set_teacher.bind(this)} things={this.state.teachers} value_prop={"id"} repr_prop={"name_surname"} />
          </Col>
        </Row>
      )
    } else {
      return null;
    }
  }
}

export class StudentsAddresseeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      students: null,
      chosen_student: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_data();
    } else {
      this.fetch_student_data();
    }
  }

  fetch_teacher_data = async () => {
    const all_classes = await fetchData('/register_api/teacher/all_classes/');
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const students = await fetchData(`/register_api/teacher/students/${class_.id}`);
      proceed_if_has_one(students, 'students', () => {
        const student = students.students[0];
        this.setState(() => {
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            students: students.students,
            chosen_student: student
          }
        })
        this.props.set_addressee(student);
      });
    });
  }

  fetch_student_data = async () => {
    
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const students = await fetchData(`/register_api/teacher/students/${class_.id}`);
    proceed_if_has_one(students, 'students', () => {
      const student = students.students[0];
      this.setState(() => {
        return {
          chosen_class: class_,
          students: students.students,
          chosen_student: student
        }
      })
      this.props.set_addressee(student);
    });
  }

  set_student = (student_id) => {
    let student = find(student_id, this.state.students);
    this.setState(() => {
      return {
        chosen_student: student
      }
    });
    this.props.set_addressee(student);
  }

  render() {
    if (this.state.chosen_class && this.state.classes && this.state.chosen_student && this.state.students) {
      return (
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
          </Col>
          <Col md>
            <ComboBox id={"floatingStudentsGrid"} label={"Uczeń"} aria_label={"Student select"} initial_value={this.state.chosen_student.id} onChange={this.set_student.bind(this)} things={this.state.students} value_prop={"id"} repr_prop={"name_surname"} />
          </Col>
        </Row>
      )
    } else {
      return null;
    }
  }
}

export class WholeClassAddresseeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const all_classes = await fetchData('/register_api/teacher/all_classes/');
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      this.setState(() => {
        return {
          classes: all_classes.classes,
          chosen_class: class_
        }
      });
      this.props.set_addressee(class_);
    });
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    this.setState(() => {
      return {
        chosen_class: class_
      }
    })
    this.props.set_addressee(class_);
  }

  render() {
    if (this.state.classes && this.state.chosen_class) {
      return (
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
          </Col>
        </Row>
      )
    } else {
      return null;
    }
  }
}

export class ReceivedMessagesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const received = await fetchData('/register_api/received_messages/');
    this.setState(() => {
      return {
        messages: received.received
      }
    })
  }

  remove_marked = (marked_messages) => {
    postData('/register_api/received_messages/delete/', marked_messages, () => { this.forceUpdate(); alert('Usunięto wiadomości'); }, (error) => alert(error));
  }

  refresh = async () => {
    this.fetch_initial_data();
  }


  render() {
    if (this.state.messages) {
      return (
        <MessagesComponent refresh={this.refresh.bind(this)} remove_marked={this.remove_marked.bind(this)} is_sent={false} messages={this.state.messages} />
      )
    } else {
      return null;
    }
  }
}

export class SentMessagesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const sent = await fetchData('/register_api/sent_messages/');
    this.setState(() => {
      return {
        messages: sent.sent
      }
    })
  }

  refresh = async () => {
    this.fetch_initial_data();
  }

  remove_marked = (marked_messages) => {
    postData('/register_api/sent_messages/delete/', marked_messages, () => { this.forceUpdate(); alert('Usunięto wiadomości'); }, (error) => alert(error));
  }

  render() {
    if (this.state.messages) {
      return (
        <MessagesComponent refresh={this.refresh.bind(this)} remove_marked={this.remove_marked.bind(this)} is_sent={true} messages={this.state.messages} />
      )
    } else {
      return null;
    }
  }
}

export class MessagesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show_modal: false,
      message: null,
      marked_messages: []
    }
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  show_message = (message_id) => {
    let message = find(message_id, this.props.messages);
    if (!this.props.is_sent) {
      this.set_message_read(message);
    }
    this.setState(() => {
      return {
        show_modal: true,
        message: message
      }
    }).then(this.props.refresh())
  }

  set_message_read = (message) => {
    let copied_message = JSON.parse(JSON.stringify(message));
    copied_message.read = true;
    postData('/register_api/received_messages/update/', copied_message, () => { }, (error) => alert(error));
  }

  set_message_unread = (message) => {
    let copied_message = JSON.parse(JSON.stringify(message));
    copied_message.read = false;
    postData('/register_api/received_messages/update/', copied_message, () => { }, (error) => alert(error));
  }

  serve_marked = (target, message) => {
    if (!target.checked) {
      this.setState((state) => {
        return {
          marked_messages: state.marked_messages.filter((value, index, arr) => value == message)
        }
      })
    } else {
      this.setState((state) => {
        state.marked_messages.push(message);
        return {
          marked_messages: state.marked_messages
        }
      })
    }
  }

  remove_marked = () => {
    if (this.props.remove_marked !== undefined) {
      this.props.remove_marked(this.state.marked_messages);
    }
  }

  set_marked_unread = () => {
    this.state.marked_messages.map(msg => {
      this.set_message_unread(msg);
    });
    this.props.refresh();
  }

  render() {
    const receiver_sender_header = (this.props.is_sent) ? "Adresat" : "Nadawca";
    const receiver_sender = (this.props.is_sent) ? "receiver_name" : "sender_name";
    const button = (this.props.is_sent) ? null : <Button onClick={(e) => this.set_marked_unread()} variant="info" style={{ marginLeft: "0.5rem" }} size="sm">Oznacz jako nieprzeczytane</Button>

    const respond_button = (this.props.is_sent) ? null : <Button onClick={(e) => { e.stopPropagation(); }} variant="info" style={{ marginLeft: "0.5rem" }} size="sm">Odpowiedz</Button>
    const title = (this.state.message) ? <div style={{ display: "flex" }}>{this.state.message.title} {respond_button}</div> : "";
    const body = (this.state.message) ? this.state.message.message : "";
    const footer = <Button onClick={(e) => this.set_show_modal(false)}>Zamknij</Button>
    return (
      <div>
        <Modal show_modal={this.state.show_modal} close={(e) => this.set_show_modal(false)} title={title} body={body} footer={footer} />
        <div style={{ display: "flex", marginBottom: "0.3rem" }}>
          <Button onClick={(e) => this.remove_marked()} variant="danger" size="sm">Usuń zaznaczone</Button>
          {button}
        </div>
        <Table striped hover>
          <thead>
            <tr>
              <th>#</th>
              <th>{receiver_sender_header}</th>
              <th>Temat</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.messages &&
              this.props.messages.map((message, index) => {
                const weight = (this.props.is_sent) ? "normal" : ((message.read) ? "normal" : "bold");
                const style = { fontWeight: weight, cursor: "pointer" };
                return (
                  <tr style={style} onClick={(e) => { this.show_message(message.id) }}>
                    <td style={{ display: "flex" }}>{index + 1} <Form.Check onClick={(e) => { e.stopPropagation(); this.serve_marked(e.target, message) }} style={{ marginLeft: "0.3rem" }} type="checkbox" /></td>
                    <td>{message[receiver_sender]}</td>
                    <td>{message.title}</td>
                    <td></td>
                  </tr>
                )
              })
            }
          </tbody>
        </Table>
      </div>
    )
  }
}

export class FormingComponent extends Component {
  render() {
    return (
      <div></div>
    )
  }
}

export class HomeworkComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let newHomeworkComponent = null;
    if (this.props.mode == mapping.TEACHER) {
      newHomeworkComponent = <Tab eventKey="new_homework" title="Nowa praca domowa">
        <NewHomeworkComponent />
      </Tab>
    }
    return (
      <Tabs className="mb-3">
        {newHomeworkComponent}
        <Tab eventKey="past_homework" title="Wszystkie prace domowe">
          <PastHomeworkComponent mode={this.props.mode} student={this.props.student} />
        </Tab>
      </Tabs>
    )
  }
}

class PastHomeworkComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      classes: null,
      courses: null,
      chosen_course: null,
      homework: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_data()
    } else {
      this.fetch_student_data()
    }
  }

  fetch_teacher_data = async () => {
    const all_classes = await TeacherAPI.classes();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const courses = await TeacherAPI.coursesForClass(class_.id);
      proceed_if_has_one(courses, 'courses', async () => {
        const course = courses.courses[0];
        const homework = await TeacherAPI.homeworkForCourse(course.id);
        this.setState(()=>{
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            courses: courses.courses,
            chosen_course: course,
            homework: homework.homework
          }
        });
      })
    })
  }

  fetch_student_data = async () => {
    const semester = await StudentAPI.currentSemester();
    const courses = await StudentAPI.coursesForStudent(this.props.student, semester.id);
    proceed_if_has_one(courses, 'courses', async () => {
      const course = courses.courses[0];
      const homework = await StudentAPI.homeworkForCourse(course.id);
      this.setState(() => {
        return {
          courses: courses.courses,
          chosen_course: course,
          homework: homework.homework
        }
      })
    })
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const courses = await TeacherAPI.coursesForClass(class_.id);
    proceed_if_has_one(courses, 'courses', async () => {
      const course = courses.courses[0];
      const homework = await TeacherAPI.homeworkForCourse(course.id);
      this.setState(() => {
        return {
          chosen_class: class_,
          courses: courses.courses,
          chosen_course: course,
          homework: homework.homework
        }
      })
    });
  }

  set_course = async (course_id) => {
    let course = find(course_id, this.state.courses);
    const homework = await TeacherAPI.homeworkForCourse(course.id);
    this.setState(() => {
      return {
        chosen_course: course,
        homework: homework.homework
      }
    });
  }

  render() {
    let classCombo = null;
    if (this.props.mode == mapping.TEACHER) {
      classCombo = <Col md>
                    <ComboBox 
                      id={"floatingClassGrid"} 
                      label={"Klasa"} 
                      aria_label={"Class select"} 
                      initial_value={this.state.chosen_class?.id} 
                      onChange={this.find_and_set_class?.bind(this)} 
                      things={this.state.classes} 
                      value_prop={"id"} 
                      repr_prop={"str_name"} />
                  </Col>
    }

    if (this.state.homework) {
      return (
        <Container>
          <Row className="mb-3">
            {classCombo}
            <Col md>
              <ComboBox 
                id={"floatingCourseGrid"} 
                label={"Kurs"}
                aria_label={"Course select"} 
                initial_value={this.state.chosen_course.id} 
                onChange={this.set_course?.bind(this)} 
                things={this.state.courses} 
                value_prop={"id"} 
                repr_prop={"name"} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Calendar 
                events={this.state.homework} 
                allowEditing={this.props.mode==mapping.TEACHER}
                allowRemoval={this.props.mode==mapping.TEACHER}
              />
            </Col>
          </Row>
        </Container>

      )
    } else {
      return null;
    }

  }
}

class NewHomeworkComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      courses: null,
      chosen_course: null,
      description: "",
      deadline: new Date(),
      show_modal: false
    }
    console.log(this.state.deadline);
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const all_classes = await TeacherAPI.classes();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const courses = await TeacherAPI.coursesForClass(class_.id);
      proceed_if_has_one(courses, 'courses', () => {
        const course = courses.courses[0];
        this.setState(() => {
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            courses: courses.courses,
            chosen_course: course
          }
        })
      });
    });
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const courses = await TeacherAPI.coursesForClass(class_.id);
    proceed_if_has_one(courses, 'courses', () => {
      const course = courses.courses[0];
      this.setState(() => {
        return {
          chosen_class: class_,
          courses: courses.courses,
          chosen_course: course
        }
      })
    });
  }

  set_course = (course_id) => {
    let course = find(course_id, this.state.courses);
    this.setState(() => {
      return {
        chosen_course: course
      }
    });
  }

  set_description = (description) => {
    this.setState(() => {
      return {
        description: description
      }
    })
  }

  set_deadline = (deadline) => {
    deadline = new Date(deadline);
    this.setState(() => {
      return {
        deadline: deadline
      }
    })
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  give_homework = () => {
    const homework = {
      classId: this.state.chosen_class.id,
      course: this.state.chosen_course.id,
      description: this.state.description,
      deadline: this.state.deadline.toDateString()
    }
    TeacherAPI.addHomework(homework, () => {
      alert(DB_WRITE_SUCCESS_INFO);
      this.setState(() => {
        return {
          show_modal: false,
          deadline: new Date(),
          description: ""
        }
      })
    }, () => {
      alert(DB_WRITE_ERROR_INFO);
      this.setState(() => {
        return {
          show_modal: false,
          deadline: new Date(),
          description: ""
        }
    });
  })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  render() {
    if (this.state.chosen_class && this.state.classes && this.state.chosen_course && this.state.courses) {
      const header = "Nowa praca domowa"
      const body = <div>
        <p>Czy chcesz zadać tą pracę domową?</p>
        <strong>Klasa: </strong>{this.state.chosen_class.str_name}<br />
        <strong>Kurs: </strong>{this.state.chosen_course.name}<br />
        <strong>Opis: </strong>{this.state.description}<br />
        <strong>Termin: </strong>{`${this.state.deadline.getDate()}.${this.state.deadline.getMonth() + 1}.${this.state.deadline.getFullYear()}`}<br />
      </div>
      const footer = <span>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.give_homework()}>Zadaj pracę domową</Button>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.set_show_modal(false)}>Anuluj</Button>
      </span>
      return (
        <Form onSubmit={(event) => this.on_submit(event)}>
          <Modal show_modal={this.state.show_modal} close={() => { this.set_show_modal(false) }} header={header} body={body} footer={footer} />
          <Row className="mb-3">
            <Col md>
              <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
            </Col>
            <Col md>
              <ComboBox id={"floatingCourseGrid"} label={"Kurs"} aria_label={"Course select"} initial_value={this.state.chosen_course.id} onChange={this.set_course.bind(this)} things={this.state.courses} value_prop={"id"} repr_prop={"name"} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Opis</Form.Label>
              <TextEdit value={this.state.description} onChange={this.set_description.bind(this)} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Termin</Form.Label>
              <Form.Control type="date" defaultValue={this.state.deadline} onChange={(e) => this.set_deadline(e.target.value)} />
            </Col>
          </Row>
          <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Zapisz</Button>
        </Form>
      )
    } else {
      return null;
    }
  }
}

class NewTestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      courses: null,
      chosen_course: null,
      description: "",
      deadline: new Date(),
      show_modal: false
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const all_classes = await TeacherAPI.allClasses();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const courses = await TeacherAPI.coursesForClass(class_.id);
      proceed_if_has_one(courses, 'courses', () => {
        const course = courses.courses[0];
        this.setState(() => {
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            courses: courses.courses,
            chosen_course: course
          }
        })
      });
    });
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const courses = await TeacherAPI.coursesForClass(class_.id);
    proceed_if_has_one(courses, 'courses', () => {
      const course = courses.courses[0];
      this.setState(() => {
        return {
          chosen_class: class_,
          courses: courses.courses,
          chosen_course: course
        }
      })
    });
  }

  set_course = (course_id) => {
    let course = find(course_id, this.state.courses);
    this.setState(() => {
      return {
        chosen_course: course
      }
    });
  }

  set_description = (description) => {
    this.setState(() => {
      return {
        description: description
      }
    })
  }

  set_deadline = (deadline) => {
    deadline = new Date(deadline);
    this.setState(() => {
      return {
        deadline: deadline
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  clear_fields = () => {
    this.setState(()=>{
      return {
        show_modal: false,
        description: "",
        deadline: new Date()
      }
    })
  }

  create_test = () => {
    const test = {
      classId: this.state.chosen_class.id,
      course: this.state.chosen_course.id,
      description: this.state.description,
      date: this.state.deadline.toDateString()
    };
    TeacherAPI.addTest(test, () => {
      alert(DB_WRITE_SUCCESS_INFO);
      this.clear_fields();
    }, () => {
      alert(DB_WRITE_ERROR_INFO);
      this.clear_fields();
    })
  }

  render() {
    if (this.state.chosen_class && this.state.classes && this.state.chosen_course && this.state.courses) {
      const header = "Nowy test";
      const body = <div>
        <p>Czy chcesz zapowiedzieć ten sprawdzian?</p>
        <strong>Klasa: </strong>{this.state.chosen_class.str_name}<br />
        <strong>Kurs: </strong>{this.state.chosen_course.name}<br />
        <strong>Opis: </strong>{this.state.description}<br />
        <strong>Termin: </strong>{`${this.state.deadline.getDate()}.${this.state.deadline.getMonth() + 1}.${this.state.deadline.getFullYear()}`}<br />
      </div>
      const footer = <span>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.create_test()}>Zapowiedz sprawdzian</Button>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.set_show_modal(false)}>Anuluj</Button>
      </span>
      return (
        <Form onSubmit={(event) => this.on_submit(event)}>
          <Modal show_modal={this.state.show_modal} close={() => this.set_show_modal(false)} header={header} body={body} footer={footer} />
          <Row className="mb-3">
            <Col md>
              <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
            </Col>
            <Col md>
              <ComboBox id={"floatingCourseGrid"} label={"Kurs"} aria_label={"Course select"} initial_value={this.state.chosen_course.id} onChange={this.set_course.bind(this)} things={this.state.courses} value_prop={"id"} repr_prop={"name"} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Opis</Form.Label>
              <TextEdit value={this.state.description} onChange={this.set_description.bind(this)} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Data</Form.Label>
              <Form.Control type="date" defaultValue={this.state.deadline} onChange={(e) => this.set_deadline(e.target.value)} />
            </Col>
          </Row>
          <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Zapisz</Button>
        </Form>
      )
    } else {
      return null;
    }
  }
}

export class TestsComponent extends Component {
  render() {
    let newTestComponent = null;
    if (this.props.mode == mapping.TEACHER) {
      newTestComponent = <Tab eventKey="new_test" title="Nowy sprawdzian">
        <NewTestComponent />
      </Tab>
    }
    return (
      <Tabs className="mb-3">
        {newTestComponent}
        <Tab eventKey="past_tests" title="Wszystkie sprawdziany">
          <AllTestsComponent mode={this.props.mode} student={this.props.student}/>
        </Tab>
      </Tabs>
    )
  }
}

export class AllTestsComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      classes: null,
      courses: null,
      chosen_course: null,
      homework: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_data()
    } else {
      this.fetch_student_data()
    }
  }

  fetch_teacher_data = async () => {
    const all_classes = await TeacherAPI.classes();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const courses = await TeacherAPI.coursesForClass(class_.id);
      proceed_if_has_one(courses, 'courses', async () => {
        const course = courses.courses[0];
        const tests = await TeacherAPI.testsForCourse(course.id);
        this.setState(()=>{
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            courses: courses.courses,
            chosen_course: course,
            tests: tests.tests
          }
        });
      })
    })
  }

  fetch_student_data = async () => {
    const semester = await StudentAPI.currentSemester();
    const courses = await StudentAPI.coursesForStudent(this.props.student, semester.id);
    proceed_if_has_one(courses, 'courses', async () => {
      const course = courses.courses[0];
      const tests = await StudentAPI.testsForCourse(course.id);
      this.setState(() => {
        return {
          courses: courses.courses,
          chosen_course: course,
          tests: tests.tests
        }
      })
    })
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const courses = await TeacherAPI.coursesForClass(class_.id);
    proceed_if_has_one(courses, 'courses', async () => {
      const course = courses.courses[0];
      const tests = await TeacherAPI.testsForCourse(course.id);
      this.setState(() => {
        return {
          chosen_class: class_,
          courses: courses.courses,
          chosen_course: course,
          tests: tests.tests
        }
      })
    });
  }

  set_course = async (course_id) => {
    let course = find(course_id, this.state.courses);
    const tests = await TeacherAPI.testsForCourse(course.id);
    this.setState(() => {
      return {
        chosen_course: course,
        tests: tests.tests
      }
    });
  }

  render() {
    let classCombo = null;
    if (this.props.mode == mapping.TEACHER) {
      classCombo = <Col md>
                    <ComboBox 
                      id={"floatingClassGrid"} 
                      label={"Klasa"} 
                      aria_label={"Class select"} 
                      initial_value={this.state.chosen_class?.id} 
                      onChange={this.find_and_set_class?.bind(this)} 
                      things={this.state.classes} 
                      value_prop={"id"} 
                      repr_prop={"str_name"} />
                  </Col>
    }

    if (this.state.tests) {
      return (
        <Container>
          <Row className="mb-3">
            {classCombo}
            <Col md>
              <ComboBox 
                id={"floatingCourseGrid"} 
                label={"Kurs"}
                aria_label={"Course select"} 
                initial_value={this.state.chosen_course.id} 
                onChange={this.set_course?.bind(this)} 
                things={this.state.courses} 
                value_prop={"id"} 
                repr_prop={"name"} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Calendar 
                events={this.state.tests} 
                allowEditing={this.props.mode==mapping.TEACHER}
                allowRemoval={this.props.mode==mapping.TEACHER}
              />
            </Col>
          </Row>
        </Container>

      )
    } else {
      return null;
    }

  }
}

export class MessagesMasterComponent extends Component {
  render() {
    return (
      <Tabs className="mb-3">
        <Tab eventKey="got_messages" title="Odebrane">
          <ReceivedMessagesComponent />
        </Tab>
        <Tab eventKey="sent_messages" title="Wysłane">
          <SentMessagesComponent />
        </Tab>
        <Tab eventKey="new_message" title="Nowa wiadomość">
          <NewMessageComponent mode={this.props.mode} />
        </Tab>
      </Tabs>
    )
  }
}

class NewMessageComponent extends Component {

  constructor(props) {
    super(props);
    let modes_loc = null;
    if (this.props.mode == mapping.TEACHER) {
      modes_loc = [
        { id: "1", name: 'students', repr: "Uczniowie" },
        { id: "2", name: 'teachers', repr: "Nauczyciele" },
        { id: "3", name: 'caretakers', repr: "Rodzice" },
        { id: "4", name: 'whole_class', repr: "Cała klasa" }];
    } else {
      modes_loc = [
        { id: "1", name: 'students', repr: "Uczniowie" },
        { id: "2", name: 'teachers', repr: "Nauczyciele" },
        { id: "3", name: 'caretakers', repr: "Rodzice" }];
    }
    this.state = {
      modes: modes_loc,
      mode: modes_loc[0],
      addressee: null,
      show_modal: false,
      message_topic: "",
      message_text: ""
    }
  }

  set_title = (title) => {
    this.setState(() => {
      return {
        message_topic: title
      }
    })
  }

  set_body = (body) => {
    this.setState(() => {
      return {
        message_text: body
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  on_return_from_sending_msg = () => {
    this.setState(() => {
      return {
        show_modal: false,
        message_text: "",
        message_topic: ""
      }
    })
  }

  set_mode = (mode) => {
    let mode_ = find(mode, this.state.modes);
    this.setState(() => {
      return {
        mode: mode_
      }
    })
  }

  set_addressee = (addressee) => {
    this.setState(() => {
      return {
        addressee: addressee
      }
    })
  }

  send_single_message = (addressee, topic, text, is_silent = false) => {
    if (!is_silent) {
      API.sendMessage({
        addressee: addressee,
        title: topic,
        text: text
      }, () => { alert("Pomyślnie wysłano wiadomość"); this.on_return_from_sending_msg(); });
    } else {
      API.sendMessage({
        addressee: addressee,
        title: topic,
        text: text
      }, () => { this.set_show_modal(false); this.on_return_from_sending_msg(); }, (error) => { throw Error(error) });
    }
  }

  send_message = () => {
    if (this.state.mode.id != 4) {
      this.send_single_message(this.state.addressee, this.state.message_topic, this.state.message_text);
    } else {
      console.log('wysylanie calej klasie');
      (async () => {
        const students = await TeacherAPI.students(this.state.addressee.id);
        proceed_if_has_one(students, 'students', () => {
          students.students.map(student => {
            this.send_single_message(student, this.state.message_topic, this.state.message_text, true);
          })
        })
      })().then(() => { alert('Pomyślnie wysłano wszystkie wiadomości.'); this.on_return_from_sending_msg(); }).catch(error => alert(error));
    }
  }


  render() {
    let addressee_widget = null;
    if (this.state.mode.id == 1) {
      addressee_widget = <StudentsAddresseeWidget mode={this.props.mode} set_addressee={this.set_addressee.bind(this)} />
    } else if (this.state.mode.id == 2) {
      addressee_widget = <TeachersAddresseeWidget mode={this.props.mode} set_addressee={this.set_addressee.bind(this)} />
    } else if (this.state.mode.id == 3) {
      addressee_widget = <CaretakersAddresseeWidget mode={this.props.mode} set_addressee={this.set_addressee.bind(this)} />
    } else if (this.state.mode.id == 4) {
      addressee_widget = <WholeClassAddresseeWidget set_addressee={this.set_addressee.bind(this)} />
    }
    let addressee = ""
    if (this.state.addressee) {
      if (this.state.mode.id != 4) {
        addressee = this.state.addressee.name_surname;
      } else {
        addressee = this.state.addressee.str_name;
      }
    }
    const body = <div>
      <p>Czy na pewno chcesz wysłać wiadomość?</p>
      <strong>Adresat: </strong>{addressee}<br />
      <strong>Temat: </strong>{this.state.message_topic}<br />
      <strong>Treść: </strong>{this.state.message_text}<br />
    </div>


    const footer = <span>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.send_message()}>Wyślij</Button>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.set_show_modal(false)}>Anuluj</Button>
    </span>
    return (
      <Form onSubmit={(event) => this.on_submit(event)}>
        <Modal show_modal={this.state.show_modal} close={() => { this.set_show_modal(false) }} title={"Wysyłanie wiadomości"} body={body} footer={footer} />
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingAddresseeTypeGrid"} label={"Rodzaj adresata"} aria_label={"Addressee kind select"} initial_value={this.state.mode.id} onChange={this.set_mode.bind(this)} things={this.state.modes} value_prop={"id"} repr_prop={"repr"} />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md>
            {addressee_widget}
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md>
            <Form.Label>Tytuł</Form.Label>
            <LineEdit value={this.state.message_topic} onChange={this.set_title.bind(this)} />
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md>
            <Form.Label>Wiadomość</Form.Label>
            <TextEdit value={this.state.message_text} onChange={this.set_body.bind(this)} />
          </Col>
        </Row>
        <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Wyślij</Button>
      </Form>
    )
  }
}

export class LessonComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let newLessonComponent = null;
    if (this.props.mode == mapping.TEACHER) {
      newLessonComponent = <Tab eventKey="new_lesson" title="Nowa lekcja"><NewLessonComponent /></Tab>
    }
    return (
      <Tabs className="mb-3">
        {newLessonComponent}
        <Tab eventKey="past_lessons" title="Odbyte lekcje">
          <PastLessonsComponent mode={this.props.mode} student={this.props.student}/>
        </Tab>
      </Tabs>
    )

  }
}

class AllGradesComponent extends Component {
  toast_body;
  toast_header;

  constructor(props) {
    super(props);
    this.state = {
      years: null,
      chosen_year: null,
      semesters: null,
      chosen_semester: null,
      classes: null,
      chosen_class: null,
      courses: null,
      chosen_course: null,
      grades: null,
      show_modal: false,
      show_question_modal: false,
      show_toast: false
    }

    this.toast_body = "";
    this.toast_header = "";
    this.fetch_info();
  }

  fetch_info = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_info();
    } else {
      this.fetch_student_info();
    }
  }

  fetch_teacher_info = async () => {
    const years_json = await TeacherAPI.allYears();
    const current_year_json = await TeacherAPI.currentYear();
    const semesters_json = await TeacherAPI.semestersForYear(current_year_json.id);
    const current_semester_json = await TeacherAPI.currentSemester();
    const classes_json = await TeacherAPI.classes();
    proceed_if_has_one(classes_json, 'classes', async () => {
      const courses_json = await TeacherAPI.coursesForClass(classes_json.classes[0].id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        // fetch all students for a class with their grades from specific subject
        const grades = await TeacherAPI.gradesForCourse(courses_json.courses[0].id);
        this.setState(() => {
          return {
            years: years_json.years,
            chosen_year: current_year_json,
            semesters: semesters_json.semesters,
            chosen_semester: current_semester_json,
            classes: classes_json.classes,
            grades: grades.grades,
            chosen_class: classes_json.classes[0],
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0]
          }
        })
      }, () => alert(COURSES_ERROR));
    }, () => alert(CLASSES_ERROR));
  }

  fetch_student_info = async () => {
    const years_json = await StudentAPI.allYearsForStudent(this.props.student);
    const current_year_json = await StudentAPI.currentYear();
    const semesters_json = await StudentAPI.semestersForYear(current_year_json.id);
    const current_semester_json = await StudentAPI.currentSemester();
    const courses_json = await StudentAPI.coursesForStudent(this.props.student);
    proceed_if_has_one(courses_json, 'courses', async () => {
      // fetch all students for a class with their grades from specific subject
      const grades = await StudentAPI.gradesForCourse(courses_json.courses[0].id, this.props.student);
      this.setState(() => {
        return {
          years: years_json.years,
          chosen_year: current_year_json,
          semesters: semesters_json.semesters,
          chosen_semester: current_semester_json,
          grades: grades.grades,
          courses: courses_json.courses,
          chosen_course: courses_json.courses[0]
        }
      })
    }, () => alert(this.coursesError));
  }

  set_year = async (year_) => {
    if (this.props.mode == mapping.TEACHER) {
      this.set_teacher_year(year_);
    } else {
      this.set_student_year(year_);
    }
  }

  set_teacher_year = async (year_) => {
    let year = find(year_, this.state.years);
    const semesters_json = await TeacherAPI.semestersForYear(year.id);
    proceed_if_has_one(semesters_json, 'semesters', async () => {
      let semester = semesters_json.semesters[0];
      const classes_json = await TeacherAPI.classesForSemester(semester.id);
      proceed_if_has_one(classes_json, 'classes', async () => {
        const courses_json = await TeacherAPI.coursesForClassAndSemester(classes_json.classes[0].id, semester.id);
        proceed_if_has_one(courses_json, 'courses', async () => {
          // fetch all students for a class with their grades from specific subject
          const grades = await TeacherAPI.gradesForCourse(courses_json.courses[0].id);
          this.setState(() => {
            return {
              chosen_year: year,
              semesters: semesters_json.semesters,
              chosen_semester: semester,
              classes: classes_json.classes,
              chosen_class: classes_json.classes[0],
              courses: courses_json.courses,
              chosen_course: courses_json.courses[0],
              grades: grades.grades
            }
          })
        }, () => alert(COURSES_ERROR));
      }, () => alert(CLASSES_ERROR));
    }, () => alert(SEMESTERS_ERROR));
  }

  set_student_year = async (year_) => {
    let year = find(year_, this.state.years);
    const semesters_json = await StudentAPI.semestersForYear(year.id);
    proceed_if_has_one(semesters_json, 'semesters', async () => {
      let semester = semesters_json.semesters[0];
      const courses_json = await StudentAPI.coursesForStudent(this.props.student, semester.id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        // fetch all students for a class with their grades from specific subject
        const grades = await StudentAPI.gradesForCourse(courses_json.courses[0].id, this.props.student);
        this.setState(() => {
          return {
            chosen_year: year,
            semesters: semesters_json.semesters,
            chosen_semester: semester,
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0],
            grades: grades.grades
          }
        })
      }, () => alert(COURSES_ERROR));
    }, () => alert(SEMESTERS_ERROR));
  }

  set_semester = async (semester_) => {
    if (this.props.mode == mapping.TEACHER) {
      this.set_teacher_semester(semester_);
    } else {
      this.set_student_semester(semester_);
    }
  }

  set_teacher_semester = async (semester_) => {
    let semester = find(semester_, this.state.semesters);
    const classes_json = await TeacherAPI.classesForSemester(semester.id);
    proceed_if_has_one(classes_json, 'classes', async () => {
      const courses_json = await TeacherAPI.coursesForClassAndSemester(classes_json.classes[0].id, semester.id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        // fetch all students for a class with their grades from specific course
        const grades = await TeacherAPI.gradesForCourse(courses_json.courses[0].id);
        this.setState(() => {
          return {
            chosen_semester: semester,
            classes: classes_json.classes,
            chosen_class: classes_json.classes[0],
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0],
            grades: grades.grades
          }
        })
      }, () => alert(COURSES_ERROR));
    }, () => alert(CLASSES_ERROR));
  }

  set_student_semester = async (semester_) => {
    let semester = find(semester_, this.state.semesters);
    const courses_json = await StudentAPI.coursesForStudent(this.props.student, semester.id);
    proceed_if_has_one(courses_json, 'courses', async () => {
      // fetch all students for a class with their grades from specific course
      const grades = await StudentAPI.gradesForCourse(courses_json.courses[0].id, this.props.student);
      this.setState(() => {
        return {
          chosen_semester: semester,
          courses: courses_json.courses,
          chosen_course: courses_json.courses[0],
          grades: grades.grades
        }
      })
    }, () => alert("Brak kursów w wybranym semestrze."));
  }

  set_class = async (class_) => {
    // find correct class because class_ is only id
    let classLoc = find(class_, this.state.classes);
    const courses_json = await TeacherAPI.coursesForClass(classLoc.id);
    proceed_if_has_one(courses_json, 'courses', async () => {
      let course = courses_json.courses[0];
      // fetch all students for a class with their grades from specific subject
      const grades = await TeacherAPI.gradesForCourse(courses_json.courses[0].id);
      this.setState(() => {
        return {
          chosen_class: classLoc,
          courses: courses_json.courses,
          chosen_course: course,
          grades: grades.grades
        }
      });
    }, () => alert("No 'courses' in courses. Internal Error."));
  }

  set_course = async (course_) => {
    // find correct course because course_ is only id
    let course = find(course_, this.state.courses);
    // fetch all students for a class with their grades from specific subject
    let grades = null;
    if (this.props.student) {
      grades = await API.gradesForCourse(course.id, this.props.student)
    } else {
      grades = await API.gradesForCourse(course.id);
    }
    this.setState(() => {
      return {
        chosen_course: course,
        grades: grades.grades
      }
    })
  }

  edit_grade = (grade_id) => {
    let grade = find(grade_id, this.state.grades);
    this.setState(() => {
      return {
        show_modal: true,
        grade_to_edit: grade
      }
    });
  }

  remove_grade = (grade_id) => {
    let grade = find(grade_id, this.state.grades);
    this.setState(() => {
      return {
        show_question_modal: true,
        grade_to_edit: grade
      }
    })
  }

  cancel_deleting = () => {
    this.setState(() => {
      return {
        show_question_modal: false,
        grade_to_edit: null
      }
    })
  }

  cancel_editing = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: false,
        grade_to_edit: null
      }
    });
  }

  post_edit = () => {
    this.cancel_editing();
    this.set_class(this.state.chosen_class.id);
  }

  show_toast = (boolVal) => {
    if (!boolVal) {

    }
    this.setState(() => {
      return {
        show_toast: boolVal
      }
    })
  }

  post_delete = () => {
    TeacherAPI.deleteGrade({ id: this.state.grade_to_edit.id });
    this.cancel_deleting();
    this.set_class(this.state.chosen_class.id);
  }

  render() {
    let text = `Edycja oceny ${this.state.lesson_to_edit && this.state.lesson_to_edit.topic}`;
    let body = <Container>
      <Row className="mb-3">
        <Col md>
          <Form.Label>Nowy temat</Form.Label>
          <Form.Control onChange={(e) => this.set_new_topic(e.target.value)} />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md>
          <Form.Label>Nowa data</Form.Label>
          <Form.Control type="date" defaultValue={this.state.lesson_to_edit && this.state.lesson_to_edit.date} onChange={(e) => this.new_date = e.target.value} />
        </Col>
      </Row>
    </Container>
    let footer = <span>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.post_edit()}>Zapisz</Button>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.cancel_editing()}>Anuluj</Button>
    </span>

    let classCombo = null;
    if (this.state.chosen_class && this.state.classes) {
      classCombo =           <Col md>
      <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
    </Col>
    }

    return (
      this.state.years && this.state.semesters && this.state.chosen_course &&
      <div>
        <Toast show_toast={this.state.show_toast} header={this.toast_header} body={this.toast_body} onClose={() => this.show_toast(false)} />
        <Modal show_modal={this.state.show_question_modal} title={`Usuwanie lekcji ${this.state.lesson_to_edit && this.state.lesson_to_edit.topic}`} body={"Czy jesteś pewien, że chcesz usunąć tą lekcję?"} footer={<span>
          <Button style={{ margin: "0.2rem" }} onClick={() => this.post_delete()}>Usuń</Button>
          <Button style={{ margin: "0.2rem" }} onClick={() => this.cancel_deleting()}>Anuluj</Button>
        </span>} close={() => { this.setState(() => { return { show_question_modal: false } }) }} />
        <Modal show_modal={this.state.show_modal} title={text} body={body} footer={footer} close={this.cancel_editing.bind(this)} />
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingClassGrid"} label={"Rok"} aria_label={"Year select"} initial_value={this.state.chosen_year.id} onChange={this.set_year.bind(this)} things={this.state.years} value_prop={"id"} repr_prop={"name"} />
          </Col>
          <Col md>
            <ComboBox id={"floatingCourseGrid"} label={"Semestr"} aria_label={"Semester select"} initial_value={this.state.chosen_semester.id} onChange={this.set_semester.bind(this)} things={this.state.semesters} value_prop={"id"} repr_prop={"representation"} />
          </Col>
        </Row>
        <Row className="mb-3">
          {classCombo}
          <Col md>
            <ComboBox id={"floatingCourseGrid"} label={"Kurs"} aria_label={"Course select"} initial_value={this.state.chosen_course.id} onChange={this.set_course.bind(this)} things={this.state.courses} value_prop={"id"} repr_prop={"name"} />
          </Col>
        </Row>
        <Row>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Uczeń</th>
                <th>Oceny</th>
                <th>Średnia</th>
                <th>Przewidywana ocena</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.grades &&
                this.state.grades.map((grade, index) => {
                  const course = grade.course;
                  const student = grade.student;
                  const class_ = grade.course.class;
                  return (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{student.name_surname}</td>
                      <td><div style={{ display: "flex" }}>{
                        grade.grades.map((grade_) => {
                          const allowEdit = (this.props.mode == mapping.TEACHER) ? true : false;
                          return (
                            <SingleGradeView allow_edit={allowEdit} grade={grade_} student={student} course={course} class_={class_} post_edit={this.post_edit.bind(this)} />
                          )
                        })
                      }</div>
                      </td>
                      <td>{grade.average}</td>
                      <td>{grade.expected_grade}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </Table>
        </Row>
        <Button variant="primary" size="sm">Wygeneruj plik</Button>
      </div>
    )
  }
}

class PastLessonsComponent extends Component {
  new_topic;
  new_date;
  notes;
  toast_body;
  toast_header;

  constructor(props) {
    super(props);
    this.state = {
      years: null,
      chosen_year: null,
      semesters: null,
      chosen_semester: null,
      classes: null,
      chosen_class: null,
      courses: null,
      chosen_course: null,
      lessons: null,
      show_modal: false,
      show_question_modal: false,
      show_toast: false,
      lesson_to_edit: null
    }

    this.toast_body = "";
    this.toast_header = "";
    this.fetch_info();
  }

  fetch_info = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_info();
    } else {
      this.fetch_student_info();
    }
  }

  fetch_teacher_info = async () => {
    const years_json = await TeacherAPI.allYears();
    const current_year_json = await TeacherAPI.currentYear();
    const semesters_json = await TeacherAPI.semestersForYear(current_year_json.id);
    const current_semester_json = await TeacherAPI.currentSemester();
    const classes_json = await TeacherAPI.classes();
    proceed_if_has_one(classes_json, 'classes', async () => {
      const courses_json = await TeacherAPI.coursesForClass(classes_json.classes[0].id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        const lessons_json = await TeacherAPI.lessonsForCourse(courses_json.courses[0].id);
        this.setState(() => {
          return {
            years: years_json.years,
            chosen_year: current_year_json,
            semesters: semesters_json.semesters,
            chosen_semester: current_semester_json,
            classes: classes_json.classes,
            chosen_class: classes_json.classes[0],
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0],
            lessons: lessons_json.lessons
          }
        })
      }, () => alert("No 'courses' in courses_json. Internal Error."));
    }, () => alert("No 'classes' in classes_json. Internal Error."));
  }

  fetch_student_info = async () => {
    const years_json = await StudentAPI.allYearsForStudent(this.props.student);
    const current_year_json = await StudentAPI.currentYear();
    const current_semester_json = StudentAPI.currentSemester();
    const semesters_json = await StudentAPI.semestersForYear(current_year_json.id);
    const courses_json = await StudentAPI.coursesForStudent(this.props.student);
    proceed_if_has_one(courses_json, 'courses', async () => {
      const lessons_json = await StudentAPI.lessonsForCourse(courses_json.courses[0].id);
      this.setState(() => {
        return {
          years: years_json.years,
          chosen_year: current_year_json,
          semesters: semesters_json.semesters,
          chosen_semester: current_semester_json,
          courses: courses_json.courses,
          chosen_course: courses_json.courses[0],
          lessons: lessons_json.lessons
        }
      })
    }, () => {
      this.setState(() => {
        return {
          years: years_json.years,
          chosen_year: current_year_json,
          semesters: semesters_json.semesters,
          chosen_semester: current_semester_json,
          courses: courses_json.courses,
          chosen_course: null,
          lessons: null
        }
      })
    })
  }

  set_year = async (year_) => {
    if (this.props.mode == mapping.TEACHER) {
      this.set_teacher_year(year_);
    } else {
      this.set_student_year(year_);
    }
  }

  set_teacher_year = async (year_) => {
    let year = find(year_, this.state.years);
    const semesters_json = await TeacherAPI.semestersForYear(year.id);
    proceed_if_has_one(semesters_json, 'semesters', async () => {
      let semester = semesters_json.semesters[0];
      const classes_json = await TeacherAPI.classesForSemester(semester.id);
      proceed_if_has_one(classes_json, 'classes', async () => {
        const courses_json = await TeacherAPI.coursesForClassAndSemester(classes_json.classes[0].id, semester.id);
        proceed_if_has_one(courses_json, 'courses', async () => {
          const lessons_json = await TeacherAPI.lessonsForCourse(courses_json.courses[0].id);
          this.setState(() => {
            return {
              chosen_year: year,
              semesters: semesters_json.semesters,
              chosen_semester: semester,
              classes: classes_json.classes,
              chosen_class: classes_json.classes[0],
              courses: courses_json.courses,
              chosen_course: courses_json.courses[0],
              lessons: lessons_json.lessons
            }
          })
        }, () => alert("No 'courses' in courses_json. Internal Error."));
      }, () => alert("No 'classes' in classes. Internal Error!"));
    }, () => alert("No 'semesters' in semesters. Internal Error!"));
  }

  set_student_year = async (year_) => {
    let year = find(year_, this.state.years);
    const semesters_json = await StudentAPI.semestersForYear(year.id);
    proceed_if_has_one(semesters_json, 'semesters', async () => {
      let semester = semesters_json.semesters[0];
      const courses_json = await StudentAPI.coursesForStudent(this.props.student, semester.id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        const lessons_json = await StudentAPI.lessonsForCourse(courses_json.courses[0].id);
        this.setState(() => {
          return {
            chosen_year: year,
            semesters: semesters_json.semesters,
            chosen_semester: semester,
            classes: classes_json.classes,
            chosen_class: classes_json.classes[0],
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0],
            lessons: lessons_json.lessons
          }
        })
      }, () => alert("No 'courses' in courses_json. Internal Error."));
    }, () => alert("No 'semesters' in semesters. Internal Error!"));
  }

  set_semester = async (semester_) => {
    if (this.props.mode == mapping.TEACHER) {
      this.set_teacher_semester(semester_);
    } else {
      this.set_student_semester(semester_);
    }
  }

  set_teacher_semester = async (semester_) => {
    let semester = find(semester_, this.state.semesters);
    const classes_json = await TeacherAPI.classesForSemester(semester.id);
    proceed_if_has_one(classes_json, 'classes', async () => {
      const courses_json = await TeacherAPI.coursesForClassAndSemester(classes_json.classes[0].id, semester.id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        const lessons_json = await TeacherAPI.lessonsForCourse(courses_json.courses[0].id);
        this.setState(() => {
          return {
            chosen_semester: semester,
            classes: classes_json.classes,
            chosen_class: classes_json.classes[0],
            courses: courses_json.courses,
            chosen_course: courses_json.courses[0],
            lessons: lessons_json.lessons
          }
        })
      }, () => this.setState(() => {
        return {
          classes: classes_json.classes,
          chosen_class: classes_json.classes[0],
          chosen_semester: semester,
          courses: courses_json.courses,
          chosen_course: null,
          lessons: null
        }
      }));
    }, () => this.setState(() => {
      return {
        chosen_semester: semester,
        courses: courses_json.courses,
        chosen_course: null,
        lessons: null
      }
    }));
  }

  set_student_semester = async (semester_) => {
    let semester = find(semester_, this.state.semesters);
    const courses_json = await StudentAPI.coursesForStudent(this.props.student, semester.id);
    proceed_if_has_one(courses_json, 'courses', async () => {
      const lessons_json = await StudentAPI.lessonsForCourse(courses_json.courses[0].id);
      this.setState(() => {
        return {
          chosen_semester: semester,
          courses: courses_json.courses,
          chosen_course: courses_json.courses[0],
          lessons: lessons_json.lessons
        }
      })
    }, () => {
      this.setState(() => {
        return {
          chosen_semester: semester,
          courses: null,
          chosen_course: null,
          lessons: null
        }
      })
    });
  }

  set_class = async (class_) => {
    // find correct class because class_ is only id
    let classLoc = find(class_, this.state.classes);
    const courses_json = await TeacherAPI.coursesForClass(classLoc.id);
    proceed_if_has_one(courses_json, 'courses', async () => {
      let course = courses_json.courses[0];
      const lessons_json = await TeacherAPI.lessonsForCourse(course.id);
      this.setState(() => {
        return {
          chosen_class: classLoc,
          courses: courses_json.courses,
          chosen_course: course,
          lessons: lessons_json.lessons
        }
      });
    }, () => this.setState(() => {
      return {
        chosen_class: classLoc,
        courses: null,
        chosen_course: null,
        lessons: null
      }
    }));
  }

  set_course = async (course_) => {
    // find correct course because course_ is only id
    let course = find(course_, this.state.courses);
    let lessons_json = null;
    if (this.props.mode == mapping.TEACHER)
      lessons_json = await TeacherAPI.lessonsForCourse(course.id);
    else
      lessons_json = await StudentAPI.lessonsForCourse(course.id);
    this.setState(() => {
      return {
        chosen_course: course,
        lessons: lessons_json.lessons
      }
    })
  }

  edit_lesson = (lesson_id) => {
    let lesson = find(lesson_id, this.state.lessons);
    this.new_topic = lesson.topic;
    this.notes = lesson.notes;
    this.new_date = lesson.date;
    this.setState(() => {
      return {
        show_modal: true,
        lesson_to_edit: lesson
      }
    });
  }

  remove_lesson = (lesson_id) => {
    let lesson = find(lesson_id, this.state.lessons);
    this.setState(() => {
      return {
        show_question_modal: true,
        lesson_to_edit: lesson
      }
    })
  }

  cancel_deleting = () => {
    this.setState(() => {
      return {
        show_question_modal: false,
        lesson_to_edit: null
      }
    })
  }

  cancel_editing = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: false,
        lesson_to_edit: null
      }
    });
  }

  set_new_topic = (topic) => {
    this.new_topic = topic;
    console.log(this.new_topic);
  }

  post_edit = () => {
    let old_topic = this.state.lesson_to_edit.topic;
    let old_date = this.state.lesson_to_edit.date;
    let lesson = JSON.parse(JSON.stringify(this.state.lesson_to_edit));
    lesson.topic = this.new_topic;
    lesson.notes = this.notes;
    lesson.date = this.new_date;
    TeacherAPI.editLesson(lesson, (r) => {
      this.toast_header = "Poprawna edycja";
      this.toast_body = <div>
        <Table striped bordered hover size="sm">
          <thead>
            <th>Przed edycją</th>
            <th>Po edycji</th>
          </thead>
          <tbody>
            <tr>
              <td>Temat: {old_topic}<br />Data: {old_date}</td>
              <td>Temat: {lesson.topic}<br />Data: {lesson.date}</td>
            </tr>
          </tbody>
        </Table>
      </div>;
      this.show_toast(true);
    }, (error) => alert(error));

    this.cancel_editing();
    this.set_class(this.state.chosen_class.id);
  }

  show_toast = (boolVal) => {
    if (!boolVal) {
      this.toast_header = "";
      this.toast_body = "";
    }
    this.setState(() => {
      return {
        show_toast: boolVal
      }
    })
  }

  set_new_notes = (notes) => {
    this.notes = notes;
  }

  post_delete = () => {
    TeacherAPI.deleteLesson({ id: this.state.lesson_to_edit.id });
    this.cancel_deleting();
    this.set_class(this.state.chosen_class.id);
  }

  render() {
    let text = `Edycja lekcji o temacie ${this.state.lesson_to_edit && this.state.lesson_to_edit.topic}`;
    let body = <Container>
      <Row className="mb-3">
        <Col md>
          <Form.Label>Temat</Form.Label>
          <Form.Control defaultValue={this.new_topic} onChange={(e) => this.set_new_topic(e.target.value)} />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md>
          <Form.Label>Opis</Form.Label>
          <TextEdit defaultValue={this.notes} onChange={(e) => this.set_new_notes(e.target.value)} />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col md>
          <Form.Label>Nowa data</Form.Label>
          <Form.Control type="date" defaultValue={this.state.lesson_to_edit && this.state.lesson_to_edit.date} onChange={(e) => this.new_date = e.target.value} />
        </Col>
      </Row>
    </Container>
    let footer = <span>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.post_edit()}>Zapisz</Button>
      <Button style={{ margin: "0.2rem" }} onClick={() => this.cancel_editing()}>Anuluj</Button>
    </span>

    let classCombo = null;
    if (this.props.mode == mapping.TEACHER && this.state.chosen_class && this.state.classes) {
      classCombo = <Col md>
        <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
      </Col>
    }

    let tableHeaders = null;
    if (this.props.mode == mapping.TEACHER) {
      tableHeaders = <tr>
        <th>#</th>
        <th>Temat</th>
        <th>Data</th>
        <th></th>
        <th></th>
      </tr>
    } else {
      tableHeaders = <tr>
        <th>#</th>
        <th>Temat</th>
        <th>Data</th>
      </tr>
    }

    let courseCombo = null;
    if (this.state.courses && this.state.chosen_course) {
      courseCombo =           <Col md>
      <ComboBox id={"floatingCourseGrid"} label={"Kurs"} aria_label={"Course select"} initial_value={this.state.chosen_course.id} onChange={this.set_course.bind(this)} things={this.state.courses} value_prop={"id"} repr_prop={"name"} />
    </Col>
    }

    return (
      this.state.years && this.state.semesters &&
      <div>
        <Toast show_toast={this.state.show_toast} header={this.toast_header} body={this.toast_body} onClose={() => this.show_toast(false)} />
        <Modal show_modal={this.state.show_question_modal} title={`Usuwanie lekcji ${this.state.lesson_to_edit && this.state.lesson_to_edit.topic}`} body={"Czy jesteś pewien, że chcesz usunąć tą lekcję?"} footer={<span>
          <Button style={{ margin: "0.2rem" }} onClick={() => this.post_delete()}>Usuń</Button>
          <Button style={{ margin: "0.2rem" }} onClick={() => this.cancel_deleting()}>Anuluj</Button>
        </span>} close={() => { this.setState(() => { return { show_question_modal: false } }) }} />
        <Modal show_modal={this.state.show_modal} title={text} body={body} footer={footer} close={this.cancel_editing.bind(this)} />
        <Row className="mb-3">
          <Col md>
            <ComboBox id={"floatingClassGrid"} label={"Rok"} aria_label={"Year select"} initial_value={this.state.chosen_year.id} onChange={this.set_year.bind(this)} things={this.state.years} value_prop={"id"} repr_prop={"name"} />
          </Col>
          <Col md>
            <ComboBox id={"floatingCourseGrid"} label={"Semestr"} aria_label={"Semester select"} initial_value={this.state.chosen_semester.id} onChange={this.set_semester.bind(this)} things={this.state.semesters} value_prop={"id"} repr_prop={"representation"} />
          </Col>
        </Row>
        <Row className="mb-3">
          {classCombo}
          {courseCombo}
        </Row>
        <Row>
          <Table striped bordered hover>
            <thead>
              {tableHeaders}
            </thead>
            <tbody>
              {
                this.state.lessons &&
                this.state.lessons.map((lesson, index) => {
                  let edit_button = null;
                  if (this.props.mode == mapping.TEACHER) {
                    edit_button = <td><Button variant="success" size="sm" onClick={(e) => this.edit_lesson(lesson.id)}>Edytuj</Button></td>
                  }
                  let remove_button = null;
                  if (this.props.mode == mapping.TEACHER) {
                    remove_button = <td><Button variant="danger" size="sm" onClick={(e) => this.remove_lesson(lesson.id)}>Usuń</Button></td>
                  }
                  return (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{lesson.topic}</td>
                      <td>{lesson.date}</td>
                      {edit_button}
                      {remove_button}
                    </tr>
                  )
                })
              }
            </tbody>
          </Table>
        </Row>
      </div>
    )
  }
}

export class GradeComponent extends Component {
  render() {
    let gradeEditView = null;
    if (this.props.mode == mapping.TEACHER) {
      gradeEditView = <Tab eventKey="new_grade" title="Nowa ocena">
        <GradeEditView />
      </Tab>
    }
    return (
      <Tabs className="mb-3">
        {gradeEditView}
        <Tab eventKey="given_grades" title="Wystawione oceny">
          <AllGradesComponent mode={this.props.mode} student={this.props.student} />
        </Tab>
      </Tabs>
    )
  }
}

class NewGradeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: null,
      value: null,
      types: null,
      type: null,
      classes: null,
      chosen_class: null,
      courses_with_class: null,
      chosen_course: null,
      students: null,
      chosen_student: null,
      description: "",
      show_modal: false,
      show_toast: false
    };

    this.fetch_info();
  }

  fetch_info = async () => {
    const classes_json = await TeacherAPI.classes();
    const courses_json = await TeacherAPI.coursesForClass(classes_json.classes[0].id);
    const students_json = await TeacherAPI.students(classes_json.classes[0].id);
    const grade_values_types_json = await TeacherAPI.gradeValuesTypes();

    this.setState(() => {
      return {
        values: grade_values_types_json.values,
        types: grade_values_types_json.types,
        value: grade_values_types_json.values[0].value,
        type: grade_values_types_json.types[0].id,
        students: students_json.students,
        chosen_student: students_json.students[0],
        classes: classes_json.classes,
        chosen_class: classes_json.classes[0],
        courses_with_class: courses_json.courses,
        chosen_course: courses_json.courses[0]
      }
    });
  }

  set_students = (students_) => {
    this.setState(() => {
      return {
        students: students_,
        chosen_student: students_[0].id
      }
    })
  }

  set_student = (student_) => {
    let student = find(student_, this.state.students);
    this.setState(() => {
      return {
        chosen_student: student
      }
    })
  }


  set_grade_value = (value) => {
    this.setState(() => {
      return {
        value: value
      }
    })
  }

  set_grade_type = (type) => {
    this.setState(() => {
      return {
        type: type
      }
    })
  }

  set_classes = (classes_) => {
    this.setState(() => {
      return {
        classes: classes_
      }
    });
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  set_class = async (class_) => {
    const courses_json = await TeacherAPI.coursesForClass(class_.id);
    const students_json = await TeacherAPI.students(class_.id);


    this.setState(() => {
      return {
        chosen_class: class_,
        courses_with_class: courses_json.courses,
        chosen_course: courses_json.courses[0],
        students: students_json.students,
        chosen_student: students_json.students[0].id
      }
    });
  }

  change_class = (class_id) => {
    for (let class_ of this.state.classes) {
      if (class_.id == class_id) {
        this.set_class(class_);
        break;
      }
    }
  }

  set_courses = (courses_) => {
    this.setState(() => {
      return {
        courses_with_class: courses_
      }
    })
  }

  change_course = (course_id) => {
    for (let course_ of this.state.courses_with_class) {
      if (course_.id == course_id) {
        this.set_course(course_);
        break;
      }
    }
  }

  set_course = (course_) => {
    this.setState(() => {
      return {
        chosen_course: course_
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  change_grade = (grade) => {
    this.setState(() => {
      return {
        value: grade
      }
    })
  }

  change_type = (type_) => {
    console.log(type_);
    this.setState(() => {
      return {
        type: type_
      }
    })
  }

  set_description = (description_) => {
    this.setState(() => {
      return {
        description: description_
      }
    })
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    });
  }

  proceed_submit = (boolVal) => {
    if (boolVal) {
      TeacherAPI.addGrade({
        student: this.state.chosen_student,
        course: this.state.chosen_course,
        grade: this.state.values[this.state.value - 1],
        type: this.state.types[this.state.type - 1],
        description: this.state.description
      }, () => { this.show_toast(true) })
    }
  }

  show_toast = (boolVal) => {
    this.setState(() => {
      return {
        show_toast: boolVal
      }
    })
  }

  render() {
    return (
      this.state.classes && this.state.chosen_class && this.state.courses_with_class && this.state.chosen_course && this.state.students && this.state.values && this.state.types && this.state.value && this.state.type &&
      <div>
        <Toast show_toast={this.state.show_toast} header={"Dodawanie oceny"} body={"Dodawanie oceny zakończone pomyślnie."} onClose={() => this.show_toast(false)} />
        <ReactModal
          show={this.state.show_modal}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <ReactModal.Header closeButton onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>
            <ReactModal.Title id="contained-modal-title-vcenter">
              Wystawianie oceny
            </ReactModal.Title>
          </ReactModal.Header>
          <ReactModal.Body>
            <h4>Podsumowanie</h4>
            <div>
              <p>Klasa: {this.state.chosen_class.str_name}</p>
              <p>Uczeń: {this.state.chosen_student.name} {this.state.chosen_student.surname}</p>
              <p>Przedmiot: {this.state.chosen_course.subject.name}</p>
              <p>Ocena: {this.state.values[this.state.value - 1].name}</p>
              <p>Rodzaj oceny: {this.state.types[this.state.type - 1].name}</p>
              <p>Opis: {this.state.description}</p>
            </div>
          </ReactModal.Body>
          <ReactModal.Footer>
            <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(true); }}>Zapisz</Button>
            <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>Anuluj</Button>
          </ReactModal.Footer>
        </ReactModal>
        <Form onSubmit={(event) => this.on_submit(event)}>
          <Row className="mb-3">
            <Col md>
              <FloatingLabel controlId="floatingClassGrid" label="Klasa">
                <Form.Select aria-label="Class select" value={this.state.chosen_class.id} onChange={(e) => this.change_class(e.target.value)}>
                  {
                    this.state.classes.map(c => {
                      return (
                        <option value={c.id}>{c.str_name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
            <Col md>
              <FloatingLabel controlId="floatingCourseGrid" label="Kurs">
                <Form.Select aria-label="Course select" value={this.state.chosen_course.id} onChange={(e) => this.change_course(e.target.value)}>
                  {
                    this.state.courses_with_class &&
                    this.state.courses_with_class.map(c => {
                      return (
                        <option value={c.id}>{c.subject.name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <FloatingLabel controlId="floatingCourseGrid" label="Uczeń">
                <Form.Select aria-label="Student select" value={this.state.chosen_student.id} onChange={(e) => this.set_student(e.target.value)}>
                  {
                    this.state.students &&
                    this.state.students.map(student => {
                      return (
                        <option value={student.id}>{student.name} {student.surname}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <FloatingLabel controlId="floatingGradeValueGrid" label="Ocena">
                <Form.Select aria-label="Grade select" value={this.state.value} onChange={(e) => this.change_grade(e.target.value)}>
                  {
                    this.state.values &&
                    this.state.values.map(value => {
                      return (
                        <option value={value.value}>{value.name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
            <Col md>
              <FloatingLabel controlId="floatingGradeTypeGrid" label="Rodzaj oceny">
                <Form.Select aria-label="GradeType select" value={this.state.type} onChange={(e) => this.change_type(e.target.value)}>
                  {
                    this.state.types &&
                    this.state.types.map(type => {
                      return (
                        <option value={type.id}>{type.name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Opis oceny</Form.Label>
              <LineEdit onChange={this.set_description.bind(this)} />
            </Col>
          </Row>
          <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Wystaw ocenę</Button>
        </Form>
      </div>
    )
  }



}

export class RemarkComponent extends Component {
  render() {
    let newRemarkComponent = null;
    if (this.props.mode == mapping.TEACHER) {
      newRemarkComponent = <Tab eventKey="new_remark" title="Nowa uwaga">
        <NewRemarkComponent />
      </Tab>
    }
    return (
      <Tabs className="mb-3">
        {newRemarkComponent}
        <Tab eventKey="given_remarks" title="Wystawione uwagi">
          <AllRemarksComponent mode={this.props.mode} student={this.props.student}/>
        </Tab>
      </Tabs>
    )
  }
}

class NewRemarkComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      students: null,
      show_modal: false,
      remark_title: "",
      remark_description: ""
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    const all_classes = await TeacherAPI.allClasses();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const students = await TeacherAPI.students(class_.id);
      proceed_if_has_one(students, 'students', () => {
        const student = students.students[0];
        this.setState(() => {
          return {
            classes: all_classes.classes,
            chosen_class: class_,
            students: students.students,
            chosen_student: student
          }
        })
      });
    });
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const students = await TeacherAPI.students(class_.id);
    proceed_if_has_one(students, 'students', () => {
      const student = students.students[0];
      this.setState(() => {
        return {
          chosen_class: class_,
          students: students.students,
          chosen_student: student
        }
      })
    });
  }

  set_student = (student_id) => {
    let student = find(student_id, this.state.students);
    this.chosen_student = student
  }

  set_title = (title) => {
    this.setState(() => {
      return {
        remark_title: title
      }
    })
  }

  set_text = (text) => {
    this.setState(() => {
      return {
        remark_description: text
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  on_return_from_giving_remark = () => {
    this.setState(() => {
      return {
        show_modal: false,
        remark_title: "",
        remark_description: ""
      }
    })
  }

  give_remark = () => {
    TeacherAPI.addRemark({
      student: this.state.chosen_student,
      title: this.state.remark_title,
      description: this.state.remark_description
    }, () => { alert('Pomyślnie wystawiono uwagę') }, (error) => { alert(error) });
  }

  render() {
    if (this.state.classes && this.state.chosen_class && this.state.students && this.state.chosen_student) {
      const body = <div>
        <p>Czy chcesz wystawić następującą uwagę?</p>
        <strong>Uczeń: </strong>{this.state.chosen_student.name_surname}<br />
        <strong>Temat: </strong>{this.state.remark_title}<br />
        <strong>Treść: </strong>{this.state.remark_description}<br />
      </div>

      const footer = <span>
        <Button style={{ margin: "0.2rem" }} onClick={() => { this.give_remark(); this.on_return_from_giving_remark(); }}>Wystaw uwagę</Button>
        <Button style={{ margin: "0.2rem" }} onClick={() => this.set_show_modal(false)}>Anuluj</Button>
      </span>
      return (
        <Form onSubmit={(event) => this.on_submit(event)}>
          <Modal show_modal={this.state.show_modal} close={() => { this.set_show_modal(false) }} title={"Wystawianie uwagi"} body={body} footer={footer} />
          <Row className="mb-3">
            <Col md>
              <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
            </Col>
            <Col md>
              <ComboBox id={"floatingStudentsGrid"} label={"Uczeń"} aria_label={"Student select"} initial_value={this.state.chosen_student.id} onChange={this.set_student.bind(this)} things={this.state.students} value_prop={"id"} repr_prop={"name_surname"} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Tytuł uwagi</Form.Label>
              <LineEdit value={this.state.remark_title} onChange={this.set_title.bind(this)} />
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md>
              <Form.Label>Treść uwagi</Form.Label>
              <TextEdit value={this.state.remark_description} onChange={this.set_text.bind(this)} />
            </Col>
          </Row>
          <Button type="submit" onClick={(e) => this.set_show_modal(true)}>Wystaw uwagę</Button>
        </Form>
      )
    } else {
      return null;
    }
  }
}

class AllRemarksComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: null,
      chosen_class: null,
      remarks: null
    }
    this.fetch_initial_data();
  }

  fetch_initial_data = async () => {
    if (this.props.mode == mapping.TEACHER) {
      this.fetch_teacher_data();
    } else {
      this.fetch_student_data();
    }
  }

  fetch_teacher_data = async () => {
    const all_classes = await TeacherAPI.allClasses();
    proceed_if_has_one(all_classes, 'classes', async () => {
      const class_ = all_classes.classes[0];
      const remarks = await TeacherAPI.remarks(class_.id);
      this.setState(() => {
        return {
          classes: all_classes.classes,
          chosen_class: class_,
          remarks: remarks.remarks
        }
      })
    });
  }

  fetch_student_data = async () => {
    const remarks = await StudentAPI.remarksForStudent(this.props.student);
    this.setState(() => {
      return {
        remarks: remarks.remarks
      }
    })
  }

  find_and_set_class = (class_id) => {
    let class_ = find(class_id, this.state.classes);
    this.set_class(class_);
  }

  set_class = async (class_) => {
    const remarks = await TeacherAPI.remarks(class_.id);
    this.setState(() => {
      return {
        chosen_class: class_,
        remarks: remarks.remarks
      }
    })
  }

  edit_remark = (remark_id) => {

  }

  remove_remark = (remark_id) => {

  }

  render() {
    let headers = null;
    let editButton = null;
    let removeButton = null;
    let classCombo = null;
    if (this.props.mode == mapping.TEACHER) {
      headers =                   <tr>
      <th>#</th>
      <th>Uczeń</th>
      <th>Tytuł</th>
      <th>Treść</th>
      <th>Data</th>
      <th></th>
      <th></th>
    </tr>
      editButton = <td><Button variant="success" size="sm" onClick={(e) => this.edit_remark(remark.id)}>Edytuj</Button></td>
      removeButton = <td><Button variant="danger" size="sm" onClick={(e) => this.remove_remark(remark.id)}>Usuń</Button></td>
      if (this.state.chosen_class && this.state.classes) {
        classCombo = <Row className="mb-3">
                        <Col md>
                          <ComboBox id={"floatingClassGrid"} label={"Klasa"} aria_label={"Class select"} initial_value={this.state.chosen_class.id} onChange={this.find_and_set_class.bind(this)} things={this.state.classes} value_prop={"id"} repr_prop={"str_name"} />
                        </Col>
                      </Row>
      }
    } else {
      headers = <tr>
      <th>#</th>
      <th>Uczeń</th>
      <th>Tytuł</th>
      <th>Treść</th>
      <th>Data</th>
    </tr>
    }
    if (this.state.remarks) {
      return (
        <div>
          {classCombo}
          <Row className="mb-3">
            <Col md>
              <Table striped bordered hover>
                <thead>
                  {headers}
                </thead>
                <tbody>
                  {
                    this.state.remarks.map((remark, index) => {
                      return (
                        <tr>
                          <td>{index + 1}</td>
                          <td>{remark.student.name_surname}</td>
                          <td>{remark.title}</td>
                          <td>{remark.description}</td>
                          <td>{remark.date}</td>
                          {editButton}
                          {removeButton}
                        </tr>
                      )
                    })
                  }
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
      )
    } else {
      return null;
    }
  }
}

class NewLessonComponent extends Component {
  notes;
  constructor(props) {
    super(props);
    this.state = {
      chosen_class: null,
      classes: null,
      courses_with_class: null,
      chosen_course: null,
      manual_input: true,
      show_modal: false,
      show_success_modal: false,
      topics: null,
      chosen_topic: null,
      students: null,
      student_presence: null
    }
    this.notes = '';
    this.fetch_info();
  }

  fetch_info = async () => {
    const classes_json = await TeacherAPI.classes();
    proceed_if_has_one(classes_json, 'classes', async () => {
      const courses_json = await TeacherAPI.coursesForClass(classes_json.classes[0].id);
      const students_json = await TeacherAPI.students(classes_json.classes[0].id);
      proceed_if_has_one(courses_json, 'courses', async () => {
        const topics_json = await TeacherAPI.topics(courses_json.courses[0].subject.id, classes_json.classes[0].level);
        proceed_if_has_one(students_json, 'students', async () => {
          let presence = {};
          for (let student of students_json.students) {
            presence[student.id] = true;
          }
          this.setState(() => {
            return {
              classes: classes_json.classes,
              chosen_class: (classes_json.classes && classes_json.classes.length > 0) ? classes_json.classes[0] : null,
              courses_with_class: courses_json.courses,
              chosen_course: (courses_json.courses && courses_json.courses.length > 0) ? courses_json.courses[0] : null,
              topics: topics_json.topics,
              chosen_topic: this.get_initial_chosen_topic(topics_json.topics),
              students: students_json.students,
              student_presence: presence
            }
          })
        });
      });
    });
  }

  set_students = (students_) => {
    this.setState(() => {
      let presence = {};
      for (let student of students_) {
        presence[student.id] = true;
      }
      return {
        students: students_,
        student_presence: presence
      }
    })
  }

  set_topics = (topics) => {
    let chosen_topic = this.get_initial_chosen_topic(topics);
    this.setState(() => {
      return {
        topics: topics,
        chosen_topic: chosen_topic
      }
    });
  }

  get_initial_chosen_topic = (topics, invert = false) => {
    if (!invert) {
      if (!this.state.manual_input && topics && topics.length > 0)
        return topics[0].topic;
      else
        return "";
    } else {
      if (this.state.manual_input && topics && topics.length > 0)
        return topics[0].topic;
      else
        return "";
    }
  }

  set_topic = (topic) => {
    this.setState(() => {
      return {
        chosen_topic: topic
      }
    })
  }

  set_classes = (classes_) => {
    this.setState(() => {
      return {
        classes: classes_
      }
    });
  }

  set_class = async (class_) => {
    console.log(class_);
    const courses_json = await TeacherAPI.coursesForClass(class_.id);
    proceed_if_has_one(courses_json, 'courses', async () => {
      const students_json = await TeacherAPI.students(class_.id);
      const topics_json = await TeacherAPI.topics(courses_json.courses[0].subject.id, class_.level);
      proceed_if_has_one(students_json, 'students', async () => {
        let presence = {};
        for (let student of students_json.students) {
          presence[student.id] = true;
        }
        this.setState(() => {
          return {
            chosen_class: class_,
            courses_with_class: courses_json.courses,
            chosen_course: courses_json.courses[0],
            students: students_json.students,
            student_presence: presence,
            topics: topics_json.topics,
            chosen_topic: this.get_initial_chosen_topic(topics_json.topics)
          }
        });
      }, () => alert('Error!'))
    }, () => alert('Error!'));
  }

  change_class = (class_id) => {
    for (let class_ of this.state.classes) {
      if (class_.id == class_id) {
        this.set_class(class_);
        break;
      }
    }
  }

  set_courses = (courses_) => {
    this.setState(() => {
      return {
        courses_with_class: courses_
      }
    })
  }

  change_course = (course_id) => {
    for (let course_ of this.state.courses_with_class) {
      if (course_.id == course_id) {
        this.set_course(course_);
        break;
      }
    }
  }

  set_course = async (course_) => {
    const topics_json = await TeacherAPI.topics(course_.subject.id, this.state.chosen_class.level);
    let chosen_topic = this.get_initial_chosen_topic(topics_json.topics);
    this.setState(() => {
      return {
        chosen_course: course_,
        topics: topics_json.topics,
        chosen_topic: chosen_topic
      }
    });
  }


  set_manual_input = (boolVal) => {
    this.setState((state) => {
      return {
        manual_input: boolVal,
        chosen_topic: this.get_initial_chosen_topic(state.topics, true)
      }
    });
  }

  set_show_modal = (boolVal) => {
    this.setState(() => {
      return {
        show_modal: boolVal
      }
    })
  }

  on_submit = (event) => {
    event.preventDefault();
    this.set_show_modal(true);
  }

  proceed_submit = (cont) => {
    if (cont) {
      (async () => {
        let obj = {
          class: this.state.chosen_class,
          course: this.state.chosen_course,
          topic: this.state.chosen_topic,
          notes: this.notes,
          presence: this.state.student_presence
        };

        TeacherAPI.newLesson(obj, async (response) => {
          const content = await response.json();
          if ("class" in content) {
            this.setState(() => {
              return {
                show_success_modal: true
              }
            })
          } else {
            alert(`Błąd serwera. Odpowiedź ${content}`);
          }
        });
      })();
    }
  }

  reset_success_modal = () => {
    this.setState(() => {
      return {
        show_success_modal: false
      }
    })
  }

  change_presence = (id, boolVal) => {
    let loc_student_presence = this.state.student_presence;
    loc_student_presence[id] = boolVal;
    this.setState(() => {
      return {
        student_presence: loc_student_presence
      }
    })
  }

  set_notes = (notes) => {
    this.notes = notes;
  }

  render() {
    return (
      this.state.classes && this.state.chosen_class && this.state.courses_with_class && this.state.chosen_course &&
      <div>
        <ReactModal
          show={this.state.show_success_modal}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <ReactModal.Header closeButton onClick={() => { this.reset_success_modal(); }}>
            <ReactModal.Title id="contained-modal-title-vcenter">
              Zapisano lekcję
            </ReactModal.Title>
          </ReactModal.Header>
          <ReactModal.Body>
            <p>Lekcja została zapisana poprawnie</p>
          </ReactModal.Body>
          <ReactModal.Footer>
            <Button onClick={() => { this.reset_success_modal() }}>OK</Button>
          </ReactModal.Footer>
        </ReactModal>
        <ReactModal
          show={this.state.show_modal}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <ReactModal.Header closeButton onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>
            <ReactModal.Title id="contained-modal-title-vcenter">
              Zapisywanie lekcji
            </ReactModal.Title>
          </ReactModal.Header>
          <ReactModal.Body>
            <h4>Podsumowanie</h4>
            <div>
              <p>Klasa: {this.state.chosen_class.str_name}</p>
              <p>Temat: {this.state.chosen_topic}</p>
              <p>Notatki: {this.notes}</p>
              <p>Obecność:</p>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Obecność</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.students.map((student, index) => {
                      let pres = (this.state.student_presence[student.id]) ? "obecny" : "nieobecny";
                      return (
                        <tr>
                          <td>{index + 1}</td>
                          <td>{student.name}</td>
                          <td>{student.surname}</td>
                          <td>{pres}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </Table>
            </div>
          </ReactModal.Body>
          <ReactModal.Footer>
            <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(true); }}>Zapisz</Button>
            <Button onClick={() => { this.set_show_modal(false); this.proceed_submit(false); }}>Anuluj</Button>
          </ReactModal.Footer>
        </ReactModal>
        <Form onSubmit={(event) => this.on_submit(event)}>
          <Row className="mb-3">
            <Col md>
              <FloatingLabel controlId="floatingClassGrid" label="Klasa">
                <Form.Select aria-label="Class select" value={this.state.chosen_class.id} onChange={(e) => this.change_class(e.target.value)}>
                  {
                    this.state.classes.map(c => {
                      return (
                        <option value={c.id}>{c.str_name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
            <Col md>
              <FloatingLabel controlId="floatingCourseGrid" label="Kurs">
                <Form.Select aria-label="Course select" value={this.state.chosen_course.id} onChange={(e) => this.change_course(e.target.value)}>
                  {
                    this.state.courses_with_class &&
                    this.state.courses_with_class.map(c => {
                      return (
                        <option value={c.id}>{c.subject.name}</option>
                      )
                    })
                  }
                </Form.Select>
              </FloatingLabel>
            </Col>
          </Row>
          <Row className="mb-3">
            <LessonInfoComponent students={this.state.students} student_presence={this.state.student_presence} change_presence={this.change_presence.bind(this)} set_topic={this.set_topic.bind(this)} topics={this.state.topics} chosen_topic={this.state.chosen_topic} manual_input={this.state.manual_input} set_manual_input={this.set_manual_input.bind(this)} on_notes_change={this.set_notes.bind(this)} />
          </Row>
          <Button type="submit">Stwórz lekcję i zapisz obecność</Button>
        </Form>

      </div>
    );
  }
}

class LessonInfoComponent extends Component {
  constructor(props) {
    super(props);
  }

  change_presence = (e) => {
    this.props.change_presence(e.target.id, e.target.checked);
  }

  render() {
    let topic_widget = (this.props.manual_input) ?
      <Form.Control onChange={(e) => this.props.set_topic(e.target.value)} /> :
      <Form.Select aria-label="topic" value={this.props.chosen_topic} onChange={(e) => this.props.set_topic(e.target.value)}>
        {
          this.props.topics.map(topic => {
            return (
              <option value={topic.topic}>{topic.topic}</option>
            )
          })
        }
      </Form.Select>

    return (
      <Accordion defaultActiveKey="0">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Temat</Accordion.Header>
          <Accordion.Body>
            <div>
              <Form.Group className="mb-3" controlId="formTopic">
                <Form.Label>Temat</Form.Label>
                {topic_widget}
              </Form.Group>
              <Form.Group as={Col} controlId="manual_title_box">
                <Form.Check defaultChecked={this.props.manual_input} type="checkbox" label="Wpisz temat ręcznie" onChange={(e) => { this.props.set_manual_input(e.target.checked) }} />
              </Form.Group>
            </div>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>Notatki</Accordion.Header>
          <Accordion.Body>
            <div>
              <Form.Group className="mb-3" controlId="formTopic">
                <Form.Label>Notatki</Form.Label>
                <TextEdit onChange={this.props.on_notes_change.bind(this)} />
              </Form.Group>
            </div>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>Obecność</Accordion.Header>
          <Accordion.Body>
            <div>
              <Form.Group as={Col} controlId="presence_boxes">
                <Form.Label>Sprawdź obecność</Form.Label>
                {
                  this.props.students &&
                  this.props.students.map(student => {
                    return (
                      <Form.Check id={student.id} checked={this.props.student_presence[student.id]} type="checkbox" label={student.name + " " + student.surname} onChange={(e) => this.change_presence(e)} />
                    )
                  })
                }
              </Form.Group>
            </div>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    )
  }
}

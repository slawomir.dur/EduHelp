import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import {
    LessonComponent,
    GradeComponent,
    RemarkComponent,
    HomeworkComponent,
    TestsComponent,
    MessagesMasterComponent
} from './Components.jsx';

export class MainComponent extends Component {
    mode = null;
    
    components = {
        lesson: {component: LessonComponent, title: "Lekcje"},
        grades: {component: GradeComponent, title: "Oceny"},
        remarks: {component: RemarkComponent, title: "Uwagi"},
        homework: {component: HomeworkComponent, title: "Prace domowe"},
        tests: {component: TestsComponent, title: "Sprawdziany"},
        messages: {component: MessagesMasterComponent, title: "Wiadomości"}
    }



    constructor(props, mode){
        super(props);
        this.mode = mode;
    }

    render() {
        return (
            <Tabs className="mb-3">
                {
                    Object.keys(this.components).map(value => {
                        const Component = this.components[value]['component'];
                        return (
                            <Tab eventKey={value} title={this.components[value]['title']}>
                                <Component mode={this.mode}/>
                            </Tab>
                        )
                    })
                }
            </Tabs>
        )
    }
}
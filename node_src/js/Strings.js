export const COURSES_ERROR = 'Brak kursów'
export const CLASSES_ERROR = 'Brak klas'
export const SEMESTERS_ERROR = 'Brak semestrów'

export const DB_WRITE_SUCCESS_INFO = 'Poprawnie zapisano.'
export const DB_WRITE_ERROR_INFO = 'Błąd zapisu.'
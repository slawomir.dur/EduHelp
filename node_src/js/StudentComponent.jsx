import React from 'react'
import {Tabs, Tab} from 'react-bootstrap';
import { StudentAPI } from './APICalls.js';
import { mapping } from './ComponentMapping.js'
import { MainComponent } from './MainComponent.jsx'

export class StudentComponent extends MainComponent {
    constructor(props) {
        super(props, mapping.STUDENT);
        this.state = {
            student: null
        }
        this.fetch_initial_data();
    }

    fetch_initial_data = async () => {
        const id = await StudentAPI.ownID();
        this.setState(() => {
            return {
                student: id.id
            }
        })
    }

    render() {
        if (this.state.student) {
            return (
                <Tabs className="mb-3">
                    {
                        Object.keys(this.components).map(value => {
                            const Component = this.components[value]['component'];
                            return (
                                <Tab eventKey={value} title={this.components[value]['title']}>
                                    <Component mode={this.mode} student={this.state.student}/>
                                </Tab>
                            )
                        })
                    }
                </Tabs>
            )
        } else {
            return null;
        }
    }
}

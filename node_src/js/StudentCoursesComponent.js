class StudentCoursesComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: -1,
            years: null,
            year: null,
            semesters: null,
            semester: null,
            grades: null,
        }
    }

    componentDidMount() {
        fetch('/register_api/years/').then(r=>r.json()).then(json=>this.set_years(json.years))
        fetch('/register_api/current_year/').then(r=>r.json()).then(json => this.set_year(json.id)).then(
            value => {
                if (this.state.year)
                {
                    fetch(`/register_api/semesters/${this.state.year}`).then(
                        r=>r.json()
                    ).then(json => this.set_semesters(json.semesters))
                }
            }
        )
        fetch('/register_api/current_semester/').then(r=>r.json()).then(json => this.set_semester(json.id)).then(
            value => {
                if (this.state.semester) {
                    this.update_grades()
                }
            }
        )
    }

    set_years(yearsLoc){
        this.setState(state=>{
            return {
                years: yearsLoc
            }
        })
    }
    
    set_year(yearLoc){
        this.setState(state=>{
            return {
                year: yearLoc
            }
        })
    }

    set_semester(semesterLoc){
        this.setState(state=>{
            return {
                semester: semesterLoc
            }
        })
    }

    set_semesters(semestersLoc){
        this.setState(state=>{
            return {
                semesters: semestersLoc
            }
        })
    }

    update_grades(){
        fetch(`/register_api/grades/${this.state.semester}`).then(r=>r.json()).then(json => {
            this.setState(state=>{
                return {
                    grades: json
                }
            })
         })
    }

    async fetchYears() {
        return fetch('/register_api/years/').then(response=>response.json()).then(years=>{
            this.set_years(years);
        })
    }



    async fetchSemesters(year) {
        return fetch(`/register_api/semesters/${year}`).then(response=>response.json()).then(semestersLoc=>{
            this.setState(state=>{
                return {
                    semesters: semestersLoc.semesters
                }
            })
        })
    }

    change_year(yearLoc){
        this.setState(state=>{
            return {
                year: yearLoc
            }
        });
        this.fetchSemesters(yearLoc).then(
            value => {
                if (this.state.semesters.length > 0) {
                    this.change_semester(this.state.semesters[0].id);
                }
            }
        ).then(
            value => {
                this.update_grades();
            }
        )
    }

    change_semester(semester){
        this.setState(state=>{
            return {
                semester: semester
            }
        });
        this.update_grades();
    }

    prepare_url(url) {
        let lastIdx = url.lastIndexOf("/", url.length-2);
        return url.substring(0, lastIdx) + "/";
    }

    render() {
        return (
            <div>
                <YearChoiceWidget years={this.state.years} current_year={this.state.year} change_year={this.change_year.bind(this)} semesters={this.state.semesters} semester={this.state.semester} change_semester={this.change_semester.bind(this)}/>
                <GradeTableWidget grades={this.state.grades} lesson_url={this.prepare_url(this.props.lesson_url)} semester={this.state.semester}
                     
                                  gen_pdf_grades_url={this.prepare_url(this.props.gen_pdf_grades_url)}
                                  gen_csv_grades_url={this.prepare_url(this.props.gen_csv_grades_url)}
                                  gen_json_grades_url={this.prepare_url(this.props.gen_json_grades_url)}
                                  gen_xlsx_grades_url={this.prepare_url(this.props.gen_xlsx_grades_url)}
                                  gen_xml_grades_url={this.prepare_url(this.props.gen_xml_grades_url)}
                                  
                                  gen_pdf_stats_url={this.prepare_url(this.props.gen_pdf_stats_url)}
                                  gen_csv_stats_url={this.prepare_url(this.props.gen_csv_stats_url)}
                                  gen_json_stats_url={this.prepare_url(this.props.gen_json_stats_url)}
                                  gen_xlsx_stats_url={this.prepare_url(this.props.gen_xlsx_stats_url)}
                                  gen_xml_stats_url={this.prepare_url(this.props.gen_xml_stats_url)}/>
            </div>
        );
    }
}

class YearChoiceWidget extends React.Component {
    constructor(props, context){
        super(props, context);
        this.change_year=this.change_year.bind(this);
        this.change_semester=this.change_semester.bind(this);
    }

    change_year(event){
        this.props.change_year(event.target.value);
    }

    change_semester(event) {
        this.props.change_semester(event.target.value);
    }

    render() {
        if (this.props.years && this.props.semesters) {
            console.log(this.props);
            return (
                <form>
                    <select onChange={this.change_year}>
                        {
                            this.props.years.map(year=>
                                <option value={year["id"]} >{year["name"]}</option>
                            )
                        }
                    </select>
                    <select onChange={this.change_semester}>
                        {
                            this.props.semesters.map(semester=>
                                <option value={semester["id"]} >{((semester["is_winter"])?"Zimowy":"Letni")}</option>
                            )
                        }
                    </select>
                </form>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

class GradeTableWidget extends React.Component {
    render() {
        console.log(this.props)
        if (this.props.grades) {
            return (
                <div>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">L.p.</th>
                                <th scope="col">Przedmiot</th>
                                <th scope="col">Nauczyciel prowadzący</th>
                                <th scope="col">Oceny</th>
                                <th scope="col">Średnia ocen</th>
                                <th scope="col">Przewidywana ocena</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                                {   
                                    Object.keys(this.props.grades).map((key,index) => {
                                        let grades = this.props.grades[key]["grades"]
                                        return (
                                            <tr>
                                                <td>{index+1}</td>
                                                <td>{key}</td>
                                                <td>{this.props.grades[key]["teacher"]}</td>
                                                <td>{
                                                        grades.map(grade=>{
                                                            return (
                                                                <div className="edu-tooltip" style={{backgroundColor: `${grade.color}`, padding: "0.2rem 0.4rem", margin: "4px"}}>{grade.value}
                                                                    <span className="edu-tooltiptext" style={{border: `${grade.darkerColor} 1px dotted`, backgroundColor: `${grade.lighterColor}`}}>
                                                                        Wartość: <span style={{fontWeight:"bold"}}>{grade.value}</span><br />
                                                                        Rodzaj: <span style={{fontWeight:"bold"}}>{grade.name}</span><br />
                                                                        Waga: <span style={{fontWeight:"bold"}}>{grade.value}</span>
                                                                    </span>
                                                                </div>
                                                            )
                                                        })
                                                    }</td>
                                                <td>{this.props.grades[key]["average"]}</td>
                                                <td>{this.props.grades[key]["expected_grade"]}</td>
                                                <td><a href={`${this.props.lesson_url}${this.props.grades[key]["course_id"]}`} class="btn btn-primary">Zobacz lekcje</a></td>
                                            </tr>
                                        )
                                    })
                                }
                        </tbody>
                    </table>
                    <div class="edu-tooltip">
                        <a href={`${this.props.gen_pdf_grades_url}${this.props.semester}`} className="btn btn-secondary">Generuj kartę ocen</a>
                        <span class="edu-tooltiptext" top="medium">
                            Dostępne formaty:<br/>
                                <a href={`${this.props.gen_json_grades_url}${this.props.semester}`} className="edu-btn" color="crimson">JSON</a>
                                <a href={`${this.props.gen_xml_grades_url}${this.props.semester}`} className="edu-btn" color="green">XML</a>
                                <a href={`${this.props.gen_csv_grades_url}${this.props.semester}`} className="edu-btn" color="brown">CSV</a>
                                <a href={`${this.props.gen_xlsx_grades_url}${this.props.semester}`} className="edu-btn" color="blue">XLSX</a>
                                <a href={`${this.props.gen_pdf_grades_url}${this.props.semester}`} className="edu-btn" color="violet">PDF</a>
                        </span>
                    </div>
                    <div class="edu-tooltip">
                        <a href={`${this.props.gen_pdf_stats_url}${this.props.semester}`} className="btn btn-secondary">Generuj statystyki obecności</a>
                        <span class="edu-tooltiptext" top="medium">
                            Dostępne formaty:<br/>
                                <a href={`${this.props.gen_json_stats_url}${this.props.semester}`} className="edu-btn" color="crimson">JSON</a>
                                <a href={`${this.props.gen_xml_stats_url}${this.props.semester}`} className="edu-btn" color="green">XML</a>
                                <a href={`${this.props.gen_csv_stats_url}${this.props.semester}`} className="edu-btn" color="brown">CSV</a>
                                <a href={`${this.props.gen_xlsx_stats_url}${this.props.semester}`} className="edu-btn" color="blue">XLSX</a>
                                <a href={`${this.props.gen_pdf_stats_url}${this.props.semester}`} className="edu-btn" color="violet">PDF</a>
                        </span>
                    </div>
                </div>
            )
        }  else {
            return (
                <div></div>
            )
        }
    }
}

var root = document.getElementById('react_compo');
ReactDOM.render(
    <StudentCoursesComponent {...(root.dataset)} />,
    root
);

var weekDayMap = {
  0: "Niedziela",
  1: "Poniedziałek",
  2: "Wtorek",
  3: "Środa",
  4: "Czwartek",
  5: "Piątek",
  6: "Sobota"
}

var monthMap = {
  0: "Styczeń",
  1: "Luty",
  2: "Marzec",
  3: "Kwiecień",
  4: "Maj",
  5: "Czerwiec",
  6: "Lipiec",
  7: "Sierpień",
  8: "Wrzesień",
  9: "Październik",
  10: "Listopad",
  11: "Grudzień"
}

class App extends React.Component {
  constructor(props){
    super(props);
    let today=new Date();
    this.state={
      day: today.getDay(),
      month: today.getMonth(),
      year: today.getFullYear(),
      homework: null
    };
  }

  componentDidMount() {
    this.fetch_and_set_homework();
  }

  fetch_and_set_homework(){
    fetch(`/register_api/homework/${this.state.month+1}/${this.state.year}/`).then(
      r=>r.json()
    ).then(
      json=>this.set_homework(json.homework)
    )
  }

  set_homework(homework){
    this.setState(() => {
      return {
        homework: homework
      };
    })
  }
  
  nextMonth(){
    this.setState(state=>{
      let date = new Date(state.year, state.month, state.day);
      let anotherDate = new Date(date.setMonth(date.getMonth()+1));
      return {
        day: anotherDate.getDay(),
        month: anotherDate.getMonth(),
        year: anotherDate.getFullYear(),
      }
    }, () => {
      this.fetch_and_set_homework();
    });
  }
  
  previousMonth(){
    this.setState(state=>{
      let date = new Date(state.year, state.month, state.day);
      let anotherDate = new Date(date.setMonth(date.getMonth()-1));
      return {
        day: anotherDate.getDay(),
        month: anotherDate.getMonth(),
        year: anotherDate.getFullYear(),
      }
    }, () => {
        this.fetch_and_set_homework();
    });
  }
  
  render() {
    return (
      <div>
        <ScopeChangePanel month={this.state.month} year={this.state.year} previousMonth={this.previousMonth.bind(this)} nextMonth={this.nextMonth.bind(this)}/>
        <HomeworkComponent homework={this.state.homework}/>
      </div>
      );
  }
}

class ScopeChangePanel extends React.Component {
  constructor(props){
    super(props);
    this.previousMonth=this.previousMonth.bind(this);
    this.nextMonth=this.nextMonth.bind(this);
  }
  
  previousMonth(){
    this.props.previousMonth();
  }
  
  nextMonth(){
    this.props.nextMonth();
  }
  
  render() {
    return (
      <div className="changePanel">
        <ul className="changePanelUl">
          <li className="changePanelLi" onClick={this.previousMonth}>&lt;</li>
          <li className="changePanelMonthName"><div className="divwidth">{monthMap[this.props.month]}</div></li>
          <li className="changePanelLi" onClick={this.nextMonth}>&gt;</li>
          <li className="changePanelMonthName"><div className="divwidth">{this.props.year}</div></li>
        </ul>
      </div>
    );
  }
}

class HomeworkComponent extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <table className="table table-hover">
              <thead>
                  <tr>
                      <th scope="col">L.p.</th>
                      <th scope="col">Przedmiot</th>
                      <th scope="col">Opis</th>
                      <th scope="col">Data</th>
                  </tr>
              </thead>
              <tbody>
                  { this.props.homework &&
                    this.props.homework.map((homework,index)=>{
                      return (
                        <tr>
                          <td>{index+1}</td>
                          <td>{homework.course.subject}</td>
                          <td>{homework.description}</td>
                          <td>{homework.deadline}</td>
                        </tr>
                      );
                    })
                  }
              </tbody>
          </table>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('react_compo'));
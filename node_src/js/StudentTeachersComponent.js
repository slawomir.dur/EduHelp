class StudentTeachersComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: -1,
            years: null,
            year: null,
            semesters: null,
            semester: null,
            teachers: null,
        }
    }

    componentDidMount() {
        fetch('/register_api/years/').then(r=>r.json()).then(json=>this.set_years(json.years))
        fetch('/register_api/current_year/').then(r=>r.json()).then(json => this.set_year(json.id)).then(
            value => {
                if (this.state.year)
                {
                    fetch(`/register_api/semesters/${this.state.year}`).then(
                        r=>r.json()
                    ).then(json => this.set_semesters(json.semesters))
                }
            }
        )
        fetch('/register_api/current_semester/').then(r=>r.json()).then(json => this.set_semester(json.id)).then(
            value => {
                if (this.state.semester) {
                    this.update_teachers()
                }
            }
        )
    }

    set_years(yearsLoc){
        this.setState(state=>{
            return {
                years: yearsLoc
            }
        })
    }
    
    set_year(yearLoc){
        this.setState(state=>{
            return {
                year: yearLoc
            }
        })
    }

    set_semester(semesterLoc){
        this.setState(state=>{
            return {
                semester: semesterLoc
            }
        })
    }

    set_semesters(semestersLoc){
        this.setState(state=>{
            return {
                semesters: semestersLoc
            }
        })
    }

    update_teachers(){
        fetch(`/register_api/teachers/${this.state.semester}`).then(r=>r.json()).then(json => {
            this.setState(state=>{
                return {
                    teachers: json
                }
            })
         })
    }

    async fetchYears() {
        return fetch('/register_api/years/').then(response=>response.json()).then(years=>{
            this.set_years(years);
        })
    }



    async fetchSemesters(year) {
        return fetch(`/register_api/semesters/${year}`).then(response=>response.json()).then(semestersLoc=>{
            this.setState(state=>{
                return {
                    semesters: semestersLoc.semesters
                }
            })
        })
    }

    change_year(yearLoc){
        this.setState(state=>{
            return {
                year: yearLoc
            }
        });
        this.fetchSemesters(yearLoc).then(
            value => {
                if (this.state.semesters.length > 0) {
                    this.change_semester(this.state.semesters[0].id);
                }
            }
        ).then(
            value => {
                this.update_teachers();
            }
        )
    }

    change_semester(semester){
        this.setState(state=>{
            return {
                semester: semester
            }
        });
        this.update_teachers();
    }

    prepare_url(url) {
        let lastIdx = url.lastIndexOf("/", url.length-2);
        return url.substring(0, lastIdx) + "/";
    }

    render() {
        return (
            <div>
                <YearChoiceWidget years={this.state.years} current_year={this.state.year} change_year={this.change_year.bind(this)} semesters={this.state.semesters} semester={this.state.semester} change_semester={this.change_semester.bind(this)}/>
                <TeacherTableWidget teachers={this.state.teachers} semester={this.state.semester} message_url={this.prepare_url(this.props.message_url)} form_teacher_img={this.props.form_teacher_img} message_img={this.props.message_img}/>
            </div>
        );
    }
}

class YearChoiceWidget extends React.Component {
    constructor(props, context){
        super(props, context);
        this.change_year=this.change_year.bind(this);
        this.change_semester=this.change_semester.bind(this);
    }

    change_year(event){
        this.props.change_year(event.target.value);
    }

    change_semester(event) {
        this.props.change_semester(event.target.value);
    }

    render() {
        if (this.props.years && this.props.semesters) {
            return (
                <form>
                    <select onChange={this.change_year}>
                        {
                            this.props.years.map(year=>
                                <option value={year["id"]} >{year["name"]}</option>
                            )
                        }
                    </select>
                    <select onChange={this.change_semester}>
                        {
                            this.props.semesters.map(semester=>
                                <option value={semester["id"]} >{((semester["is_winter"])?"Zimowy":"Letni")}</option>
                            )
                        }
                    </select>
                </form>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

class TeacherTableWidget extends React.Component {
    render() {
        if (this.props.teachers) {
            return (
                <div>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">L.p.</th>
                                <th scope="col">Imię</th>
                                <th scope="col">Nazwisko</th>
                                <th scope="col">Prowadzony przedmiot</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                                {   
                                    Object.keys(this.props.teachers.teachers).map((key,index) => {
                                        let teacher_info = this.props.teachers.teachers[key];
                                        console.log(teacher_info);
                                        let is_form = teacher_info["is_form"];
                                        let name = teacher_info["name"];
                                        let surname = teacher_info["surname"];
                                        let courses = teacher_info["courses"];
                                        let user_id = teacher_info["user_id"];
                                        let is_form_expr = null;
                                        if (is_form) {
                                            is_form_expr = <div className="edu-tooltip"><img src={`${this.props.form_teacher_img}`} alt="form teacher icon"/><span className="edu-tooltiptext" top="close">Wychowawca</span></div>;
                                        } else {
                                            is_form_expr = <div></div>
                                        }
                                        return (
                                            <tr>
                                                <td>{index+1}{is_form_expr}</td>
                                                <td>{name}</td>
                                                <td>{surname}</td>
                                                <td>{
                                                        courses.map(course=>{
                                                            return (
                                                                <p>{course.subject}</p>
                                                            )
                                                        })
                                                    }
                                                </td>
                                                <td><a href={`${this.props.message_url}${user_id}`}><img src={`${this.props.message_img}`}/></a></td>
                                            </tr>
                                        )
                                    })
                                }
                        </tbody>
                    </table>
                </div>
            )
        }  else {
            return (
                <div><p>Nie masz żadnych nauczycieli...</p></div>
            )
        }
    }
}

var root = document.getElementById('react_compo');
ReactDOM.render(
    <StudentTeachersComponent {...(root.dataset)} />,
    root
);
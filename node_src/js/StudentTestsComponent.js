
var weekDayMap = {
    0: "Niedziela",
    1: "Poniedziałek",
    2: "Wtorek",
    3: "Środa",
    4: "Czwartek",
    5: "Piątek",
    6: "Sobota"
  }
  
  var monthMap = {
    0: "Styczeń",
    1: "Luty",
    2: "Marzec",
    3: "Kwiecień",
    4: "Maj",
    5: "Czerwiec",
    6: "Lipiec",
    7: "Sierpień",
    8: "Wrzesień",
    9: "Październik",
    10: "Listopad",
    11: "Grudzień"
  }
  
  class App extends React.Component {
    constructor(props){
      super(props);
      let today=new Date();
      this.state={
        day: today.getDay(),
        month: today.getMonth(),
        year: today.getFullYear(),
        tests: null
      };
    }

    componentDidMount() {
        this.fetch_and_set_tests();
    }

    fetch_and_set_tests(){
        fetch(`/register_api/tests/${this.state.month+1}/${this.state.year}/`).then(
          r=>r.json()
        ).then(
          json=>this.set_tests(json.tests)
        )
      }
    
      set_tests(tests){
        this.setState(() => {
          return {
              tests: tests
          };
        })
      }
    
    nextMonth(){
      this.setState(state=>{
        let date = new Date(state.year, state.month, state.day);
        let anotherDate = new Date(date.setMonth(date.getMonth()+1));
        return {
          day: anotherDate.getDay(),
          month: anotherDate.getMonth(),
          year: anotherDate.getFullYear(),
        }
    }, () => {this.fetch_and_set_tests()});
    }
    
    previousMonth(){
      this.setState(state=>{
        let date = new Date(state.year, state.month, state.day);
        let anotherDate = new Date(date.setMonth(date.getMonth()-1));
        return {
          day: anotherDate.getDay(),
          month: anotherDate.getMonth(),
          year: anotherDate.getFullYear(),
        }
      }, () => {this.fetch_and_set_tests()});
    }
    
    render() {
      return (
        <div>
            <ScopeChangePanel month={this.state.month} year={this.state.year} previousMonth={this.previousMonth.bind(this)} nextMonth={this.nextMonth.bind(this)}/>
            <TestComponent tests={this.state.tests}/>
        </div>
        );
    }
  }
  
  class ScopeChangePanel extends React.Component {
    constructor(props){
      super(props);
      this.previousMonth=this.previousMonth.bind(this);
      this.nextMonth=this.nextMonth.bind(this);
    }
    
    previousMonth(){
      this.props.previousMonth();
    }
    
    nextMonth(){
      this.props.nextMonth();
    }
    
    render() {
      return (
        <div className="changePanel">
          <ul className="changePanelUl">
            <li className="changePanelLi" onClick={this.previousMonth}>&lt;</li>
            <li className="changePanelMonthName"><div className="divwidth">{monthMap[this.props.month]}</div></li>
            <li className="changePanelLi" onClick={this.nextMonth}>&gt;</li>
            <li className="changePanelMonthName"><div className="divwidth">{this.props.year}</div></li>
          </ul>
        </div>
      );
    }
  }

  class TestComponent extends React.Component {
      constructor(props){
          super(props);
      }

      render() {
          return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">L.p.</th>
                        <th scope="col">Przedmiot</th>
                        <th scope="col">Opis</th>
                        <th scope="col">Data</th>
                    </tr>
                </thead>
                <tbody>
                    {   this.props.tests &&
                        this.props.tests.map((test, index) => {
                            return (
                                <tr>
                                    <td>{index+1}</td>
                                    <td>{test.course.subject.name}</td>
                                    <td>{test.description}</td>
                                    <td>{test.date}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
          );
      }
  }
  
  ReactDOM.render(<App />, document.getElementById('react_compo'));
import React from 'react';
import { Tab } from 'react-bootstrap';
import {
    FormingComponent
} from './Components.jsx';
import { mapping } from './ComponentMapping.js';
import { MainComponent } from './MainComponent.jsx';

export class TeacherComponent extends MainComponent {
    constructor(props) {
        super(props, mapping.TEACHER);
        //this.components = {forming: {component: FormingComponent, title: "Wychowawstwo"}, ...this.components};
    }
}

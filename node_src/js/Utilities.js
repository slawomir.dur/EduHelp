
export function proceed_if_has_one(elem, elem2, func, els = () => { }) {
    if (elem2 in elem && elem[elem2] && elem[elem2].length > 0) {
        func();
    } else {
        els();
    }
}

export function find(what, where) {
    let i = 0;
    for (; i < where.length; ++i) {
        if (where[i].id == what) {
            break;
        }
    }
    return where[i];
}
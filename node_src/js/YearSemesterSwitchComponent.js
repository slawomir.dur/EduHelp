class YearSemesterSwitchComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            years: null,
            year: null,
            semesters: null,
            semester: null
        }
    }

    componentDidMount() {
        fetch('/register_api/years/').then(r=>r.json()).then(json=>this.set_years(json.years))
        fetch('/register_api/current_year/').then(r=>r.json()).then(json => this.set_year(json.id)).then(
            value => {
                if (this.state.year)
                {
                    fetch(`/register_api/semesters/${this.state.year}`).then(
                        r=>r.json()
                    ).then(json => this.set_semesters(json.semesters))
                }
            }
        )
        fetch('/register_api/current_semester/').then(r=>r.json()).then(json => this.set_semester(json.id));
    }

    render() {
        return (
            <form>
                <select onChange={this.change_year}>
                    {
                        this.state.years.map(year=>
                            <option value={year["id"]} >{year["name"]}</option>
                        )
                    }
                </select>
                <select onChange={this.change_semester}>
                    {
                        this.state.semesters.map(semester=>
                            <option value={semester["id"]} >{((semester["is_winter"])?"Zimowy":"Letni")}</option>
                        )
                    }
                </select>
            </form>
        )
    }
}
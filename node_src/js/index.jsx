import React from 'react';
import ReactDOM from 'react-dom';
import { TeacherComponent } from './TeacherComponent.jsx'
import { StudentComponent } from './StudentComponent.jsx'
import { CaretakerComponent } from './CaretakerComponent.jsx'
import { mapping } from './ComponentMapping.js'

const teacherComponent = document.getElementById(mapping.TEACHER);
if (teacherComponent) {
    ReactDOM.render(<TeacherComponent />, teacherComponent);
}

const studentComponent = document.getElementById(mapping.STUDENT);
if (studentComponent) {
    ReactDOM.render(<StudentComponent />, studentComponent);
}

const caretakerComponent = document.getElementById(mapping.CARETAKER);
if (caretakerComponent) {
    ReactDOM.render(<CaretakerComponent />, caretakerComponent);
}

from django.contrib import admin
from django.contrib.admin.views.main import ERROR_FLAG
from django.core.exceptions import ObjectDoesNotExist
from .models import Caretaker, SentMessage, ReceivedMessage, Teacher, Class, Student, \
 SubjectType, Subject, Course, Lesson, CoursePresence, Grade, GradeType, \
 GradeValue, PlannedHomework, PlannedTest, StudentRemark, Semester, Year, LessonTopic

# Register your models here.


@admin.register(Caretaker)
class CaretakerAdmin(admin.ModelAdmin):
    list_display=('name', 'surname', 'mobile', 'mail')
    search_fields=('name', 'surname', 'mobile', 'mail')

@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display=('name', 'surname', 'mobile', 'mail')
    search_fields=('name', 'surname', 'mobile', 'mail')

@admin.register(Year)
class YearAdmin(admin.ModelAdmin):
    list_display=('name', 'start', 'end')
    search_fields=('name',)

@admin.register(Semester)
class SemesterAdmin(admin.ModelAdmin):
    list_display=('year', 'start', 'end', 'is_winter')
    list_filter=('year',)
    search_fields=('year',)

@admin.register(Class)
class ClassAdmin(admin.ModelAdmin):
    list_display=('creation_year', 'name', 'level', 'form_teacher')
    list_filter=('creation_year', 'level', 'form_teacher')
    actions=['promote_classes', 'demote_classes']

    @admin.action(description='Przenieś wybrane klasy na następny rok')
    def promote_classes(self, request, queryset):
        for class_ in queryset:
            class_.promote()
            class_.save()

    @admin.action(description='Obniż rok wybranym klasom')
    def demote_classes(self, request, queryset):
        for class_ in queryset:
            class_.demote()
            class_.save()

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display=('name', 'surname', 'classId', 'caretaker')
    list_filter=('classId', 'caretaker')
    search_fields=('name', 'surname', 'classId', 'caretaker')

@admin.register(SubjectType)
class SubjectTypeAdmin(admin.ModelAdmin):
    list_display=('name',)

@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display=('name', 'type')

@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display=('subject', 'teacher', 'classId', 'semester')
    list_filter=('subject', 'teacher', 'classId', 'semester')
    search_fields=('subject', 'teacher', 'classId')

@admin.register(LessonTopic)
class LessonTopicAdmin(admin.ModelAdmin):
    list_display=('subject', 'level', 'topic')
    list_filter=('subject', 'level', 'topic')
    search_fields=('subject', 'level', 'topic')

@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display=('course', 'topic', 'date')
    list_filter=('course', 'date')
    search_fields=('topic',)

@admin.register(CoursePresence)
class CoursePresenceAdmin(admin.ModelAdmin):
    list_display=('student', 'lesson', 'present')
    list_filter=('student', 'lesson', 'present')
    search_fields=('student', 'lesson')

@admin.register(GradeValue)
class GradeValueAdmin(admin.ModelAdmin):
    list_display=('name', 'value')

@admin.register(GradeType)
class GradeTypeAdmin(admin.ModelAdmin):
    list_display=('name', 'value', 'color')

@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):
    list_display=('value', 'type', 'student', 'course', 'description')
    list_filter=('value', 'type', 'student', 'course')
    search_fields=('value', 'type', 'student', 'course')

@admin.register(PlannedHomework)
class PlannedHomeworkAdmin(admin.ModelAdmin):
    list_display=('classId', 'course', 'deadline', 'description')
    list_filter=('classId', 'course', 'deadline')

@admin.register(PlannedTest)
class PlannedTestAdmin(admin.ModelAdmin):
    list_display=('classId', 'course', 'date', 'description')
    list_filter=('classId', 'course', 'date')

@admin.register(StudentRemark)
class StudentRemarkAdmin(admin.ModelAdmin):
    list_display=('title', 'student', 'teacher', 'description')
    list_filter=('teacher', 'student')

@admin.register(SentMessage)
class SentMessageAdmin(admin.ModelAdmin):
    list_display=('sender', 'receiver', 'title', 'message')
    list_filter=('sender', 'receiver')
    search_fields=('sender', 'receiver', 'title', 'message')

@admin.register(ReceivedMessage)
class ReceivedMessageAdmin(admin.ModelAdmin):
    list_display=('sender', 'receiver', 'title', 'message')
    list_filter=('sender', 'receiver')
    search_fields=('sender', 'receiver', 'title', 'message')
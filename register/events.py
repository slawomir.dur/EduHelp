from django.dispatch import receiver
from .models import Grade, SentMessage, StudentRemark, PlannedTest, PlannedHomework, Student, send_message
from django.db.models.signals import post_save
from django.core.mail import send_mail

from . import models

@receiver(post_save, sender=Grade)
def on_grade_save(sender, instance, **kwargs):
    """
    Sends mail to caretaker about grade
    """
    student = instance.student
    caretaker = student.caretaker
    if caretaker.notify_on_grade:
        message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że Państwa dziecko właśnie otrzymało ocenę {instance} z przedmiotu {instance.course}.\nZ poważaniem,\nZespół EduHelp'
        send_mail(
            subject=f'Twoje dziecko właśnie otrzymało ocenę {instance} z przedmiotu {instance.course}',
            message=message,
            from_email='eduhelp@gmail.com',
            recipient_list=[caretaker.mail],
            fail_silently=False
        )

@receiver(post_save, sender=StudentRemark)
def on_remark_save(sender, instance, **kwargs):
    """
    Sends mail to caretaker about remark
    """
    student = instance.student
    caretaker = student.caretaker
    if caretaker.notify_on_remark:
        message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że Państwa dziecko właśnie otrzymało uwagę o tytule {instance.title} od nauczyciela {instance.teacher}.\nTreść uwagi jest następująca:\n\n{instance.description}\n\nZ poważaniem,\nZespół EduHelp'
        send_mail(
            subject=f'Twoje dziecko właśnie otrzymało uwagę {instance.title}',
            message=message,
            from_email='eduhelp@gmail.com',
            recipient_list=[caretaker.mail],
            fail_silently=False
        )

@receiver(post_save, sender=PlannedTest)
def on_test_save(sender, instance, **kwargs):
    """
    Sends mail to caretaker about planned test
    """
    students = Student.objects.filter(classId=instance.classId)
    for student in students:
        caretaker = student.caretaker
        if caretaker.notify_on_test:
            message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że właśnie zapowiedziano test Państwa dziecku z przedmiotu {instance.course}\nData testu: {instance.date}\nOpis testu: {instance.description}\nZ poważaniem,\nZespół EduHelp'
            send_mail(
                subject=f'Twojemu dziecku zapowiedziano test z przedmiotu {instance.course} na {instance.date}',
                message=message,
                from_email='eduhelp@gmail.com',
                recipient_list=[caretaker.mail],
                fail_silently=False
            )

@receiver(post_save, sender=PlannedHomework)
def on_homework_save(sender, instance, **kwargs):
    """
    Sends mail to caretaker about planned homework
    """
    students = Student.objects.filter(classId=instance.classId)
    for student in students:
        caretaker = student.caretaker
        if caretaker.notify_on_homework:
            message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że Państwa dziecko właśnie otrzymało pracę domową z przedmiotu {instance.course}.\nTermin oddania: {instance.deadline}\nOpis: {instance.description}\nZ poważaniem,\nZespół EduHelp'
            send_mail(
                subject=f'Twoje dziecko właśnie otrzymało pracę domową z przedmiotu {instance.course}',
                message=message,
                from_email='eduhelp@gmail.com',
                recipient_list=[caretaker.mail],
                fail_silently=False
            )
@receiver(post_save, sender=SentMessage)
def on_message_send(sender, instance: SentMessage, **kwargs):
    """
    Reacts on messages sent between student and teacher
    """
    if instance.sender.groups.filter(name="Students") and instance.receiver.groups.filter(name="Teachers"):
        student = instance.sender.student
        teacher = instance.receiver.teacher
        caretaker = student.caretaker
        subject = f'Twoje dziecko wysłało wiadomość do nauczyciela {teacher}'
        message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że Państwa dziecko właśnie wysłało wiadomość do nauczyciela {teacher}.\nTemat: {instance.title}\nTreść: {instance.message}\nZ poważaniem,\nZespół EduHelp'
        send_mail(
            subject=subject,
            message=message,
            from_email='eduhelp@gmail.com',
            recipient_list=[caretaker.mail],
            fail_silently=False
        )

        send_message(models.User.objects.get(pk=1), caretaker.user, subject, message)

    elif instance.sender.groups.filter(name="Teachers") and instance.receiver.groups.filter(name="Students"):
        teacher = instance.sender.teacher
        student = instance.receiver.student
        caretaker = student.caretaker
        subject = f'Twoje dziecko otrzymało wiadomość od nauczyciela {teacher}'
        message = f'Szanowna Pani / Szanowny Panie {caretaker.name} {caretaker.surname},\nInformujemy, że Państwa dziecko właśnie otrzymało wiadomość od nauczyciela {teacher}.\nTemat: {instance.title}\nTreść: {instance.message}\nZ poważaniem,\nZespół EduHelp'
        send_mail(
            subject=subject,
            message=message,
            from_email='eduhelp@gmail.com',
            recipient_list=[caretaker.mail],
            fail_silently=False
        )

        send_message(models.User.objects.get(pk=1), caretaker.user, subject, message)
    
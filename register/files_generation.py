from reportlab.pdfgen import canvas
from .models import Semester, Student, Course, Grade
from datetime import datetime
import io
import json
import xlsxwriter

from xml.etree import ElementTree

MARGIN_HORIZONTAL = 0.05
MARGIN_VERTICAL = 0.05
TITLE_FONT = ('Courier', 16)
TITLE_LINE_WIDTH = 0.3
GRADE_FONT = ('Courier', 12)
FOOTER_FONT = ('Courier', 6)

def bottom_bound(canvas: canvas.Canvas) -> int:
    height = canvas._pagesize[1]
    return height * MARGIN_VERTICAL

def top_bound(canvas: canvas.Canvas) -> int:
    return canvas._pagesize[1] - bottom_bound(canvas)

def left_bound(canvas: canvas.Canvas) -> int:
    width = canvas._pagesize[0]
    return width * MARGIN_HORIZONTAL

def right_bound(canvas: canvas.Canvas) -> int:
    return canvas._pagesize[0] - left_bound(canvas)

def today() -> str:
    return str(datetime.now())

class DocumentGenerator:
    @staticmethod
    def gen_course_dict(student: Student, semester: Semester = None):
        courses = student.get_courses(semester)
        course_dict = {}
        for course in courses:
            course_dict[course] = student.get_grades_for_course(course)
        return course_dict

    @staticmethod
    def generate_grades_pdf(student: Student, semester: Semester = None) -> io.BytesIO:
        # obtain grades
        course_dict = DocumentGenerator.gen_course_dict(student, semester)

        # create pdf
        file = io.BytesIO()
        # =========================================================

        def draw_header(canvas: canvas.Canvas, vert_offset):
            c.drawString(left_bound(c), top_bound(c) - TITLE_FONT[1], f'Oceny {student}')
            date = f"Data wydruku {today()}"
            date_width = c.stringWidth(date, TITLE_FONT[0], TITLE_FONT[1])
            c.drawString(right_bound(c) - date_width, top_bound(c) - TITLE_FONT[1], date)
            c.line(left_bound(c), top_bound(c) - vert_offset, right_bound(c), top_bound(c) - 25)

        def draw_footer(canvas: canvas.Canvas):
            footer_text = 'Wygenerowano przez system EduHelp'
            footer_text_width = c.stringWidth(footer_text, FOOTER_FONT[0], FOOTER_FONT[1])
            c.setFont(FOOTER_FONT[0], FOOTER_FONT[1])
            c.drawString(right_bound(c) - footer_text_width, bottom_bound(c) - bottom_bound(c)/2, footer_text)

        c = canvas.Canvas(file)
        c.setFont(TITLE_FONT[0], TITLE_FONT[1])
        width, height = c._pagesize
        c.setLineWidth(TITLE_LINE_WIDTH)

        vertical_offset = 25
        draw_header(c, 25)
        vertical_offset += 40
        MAX_KEY_WIDTH = max([c.stringWidth(str(key), GRADE_FONT[0], GRADE_FONT[1]) for key in course_dict.keys()]) + 35
        c.setFont(GRADE_FONT[0], GRADE_FONT[1])
        for key, grades in course_dict.items():
            c.drawString(left_bound(c), top_bound(c) - vertical_offset, str(key))
            horizontal_offset = MAX_KEY_WIDTH
            for grade in grades:
                c.drawString(left_bound(c) + horizontal_offset, top_bound(c) - vertical_offset, str(grade.value.value))
                horizontal_offset += c.stringWidth(str(grade.value.value), GRADE_FONT[0], GRADE_FONT[1]) + 5
                if left_bound(c) + horizontal_offset > right_bound(c):
                    horizontal_offset = MAX_KEY_WIDTH
                    vertical_offset += 15
            vertical_offset += 25
            
            # case for page end
            if top_bound(c) - vertical_offset < bottom_bound(c):
                draw_footer(c)
                c.showPage()
                c.setFont(TITLE_FONT[0], TITLE_FONT[1])
                draw_header(c, 25)
                vertical_offset = 65
                c.setFont(GRADE_FONT[0], GRADE_FONT[1])
        draw_footer(c)
        c.showPage()
        c.save()
        # ========================================================
        file.seek(0)
        return file

    @staticmethod
    def generate_grades_xml(student: Student, semester: Semester = None):
        course_dict = DocumentGenerator.gen_course_dict(student, semester)
        root = ElementTree.Element('root')
        ElementTree.SubElement(root, 'student').text = f'{student.name} {student.surname}'
        doc = ElementTree.SubElement(root, 'grades', date=str(datetime.now()))
        for course, grades in course_dict.items():
            course_tree = ElementTree.SubElement(doc, str(course))
            for grade in grades:
                ElementTree.SubElement(course_tree, 'ocena', value=str(grade.value.value), weight=str(grade.type.value), name=str(grade.value.name), type=str(grade.type.name))
        tree = ElementTree.ElementTree(root)
        file = io.BytesIO()
        tree.write(file)
        file.seek(0)
        return file

    @staticmethod
    def generate_grades_json(student: Student, semester: Semester = None):
        course_dict = DocumentGenerator.gen_course_dict(student, semester)
        file = io.BytesIO()
        grades_main = {}
        grades_main['student'] = f'{student.name} {student.surname}'
        grades_internal = {}
        for course, grades in course_dict.items():
            course_grades = {}
            counter = 1
            for grade in grades:
                grade_dict = {}
                grade_dict['value'] = grade.value.value
                grade_dict['weight'] = grade.type.value
                grade_dict['name'] = grade.value.name
                grade_dict['type'] = grade.type.name
                course_grades[f'grade_{str(counter)}'] = grade_dict
                counter += 1
            grades_internal[str(course)] = course_grades
        grades_main['grades'] = grades_internal
        json_str = json.dumps(grades_main)
        file.write(json_str.encode('utf-8')) 
        file.seek(0)
        return file

    @staticmethod
    def generate_grades_csv(student: Student, semester: Semester = None):
        course_dict = DocumentGenerator.gen_course_dict(student, semester)
        headers = 'grade,value,weight,name,type,course'
        csv_contents = headers
        for course, grades in course_dict.items():
            for grade in grades:
                csv_contents += f'\n{str(grade)},{str(grade.value.value)},{str(grade.type.value)},{str(grade.value.name)},{str(grade.type.name)},{str(course)}'
        file = io.BytesIO()
        file.write(csv_contents.encode('utf-8'))
        file.seek(0)
        return file

    @staticmethod
    def generate_grades_xlsx(student: Student, semester: Semester = None):
        course_dict = DocumentGenerator.gen_course_dict(student, semester)
        file = io.BytesIO()
        xlsx = xlsxwriter.Workbook(file)
        sheet = xlsx.add_worksheet()
        headers = ['grade','value','weight','name','type','course']
        for i in range(len(headers)):
            sheet.write(0, i, headers[i])
        counter = 1
        for course, grades in course_dict.items():
            for grade in grades:
                sheet.write(counter, 0, str(grade))
                sheet.write(counter, 1, str(grade.value.value))
                sheet.write(counter, 2, str(grade.type.value))
                sheet.write(counter, 3, str(grade.value.name))
                sheet.write(counter, 4, str(grade.type.name))
                sheet.write(counter, 5, str(course))
                counter += 1
        xlsx.close()
        file.seek(0)
        return file

    @staticmethod
    def generate_statistics_pdf(student: Student, semester: Semester):
        file = io.BytesIO()
        return file

    @staticmethod
    def generate_statistics_xml(student: Student, semester: Semester):
        stats = student.get_statistics()
        file = io.BytesIO()
        root = ElementTree.Element('root')
        ElementTree.SubElement(root, 'student').text = f'{student.name} {student.surname}'
        doc = ElementTree.SubElement(root, 'statistics', date=str(datetime.now()))
        for course, statistics in stats.items():
            course_branch = ElementTree.SubElement(doc, course)
            for key, value in statistics.items():
                ElementTree.SubElement(course_branch, key, value=str(value))
        tree = ElementTree.ElementTree(root)
        tree.write(file)
        file.seek(0)
        return file

    @staticmethod
    def generate_statistics_json(student: Student, semester: Semester):
        file = io.BytesIO()
        stats_parent = {}
        stats_parent['student'] = f'{student.name} {student.surname}'
        stats = student.get_statistics()
        stats_parent['statistics'] = stats
        json_str = json.dumps(stats_parent)
        file.write(json_str.encode('utf-8'))
        file.seek(0)
        return file

    @staticmethod
    def generate_statistics_csv(student: Student, semester: Semester):
        file = io.BytesIO()
        stats = student.get_statistics()
        content = []
        for key in stats.keys():
            content.append(key)
        if len(content) > 0:
            inner_dict = stats[content[0]]
        content = str.join(',', content) + '\n'
        for inner_key in inner_dict.keys():
            line = []
            for key in stats.keys():
                line.append(str(stats[key][inner_key]))
            line = str.join(',', line)
            line += '\n'
            content += line
        return file

    @staticmethod
    def generate_statistics_xlsx(student: Student, semester: Semester):
        file = io.BytesIO()
        stats = student.get_statistics()
        xlsx = xlsxwriter.Workbook(file)
        sheet = xlsx.add_worksheet()
        headers = []
        for key in stats.keys():
            headers.append(key)
        for i in range(len(headers)):
            sheet.write(0, i, headers[i])
        if len(headers) > 0:
            inner_dict = stats[headers[0]]
        row = 1
        for inner_key in inner_dict.keys():
            col = 0
            for key in stats.keys():
                sheet.write(row, col, stats[key][inner_key])
                col += 1
            row += 1
        xlsx.close()
        file.seek(0)
        return file
        
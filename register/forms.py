from django.forms import ModelForm, Form, BooleanField
from django.forms.widgets import HiddenInput
from django.shortcuts import get_list_or_404, get_object_or_404
from . import models

class MessageFromToForm(ModelForm):
    class Meta:
        model = models.SentMessage
        fields = ['title', 'message']

class LessonCreateForm(ModelForm):
    class Meta:
        model = models.Lesson
        fields = ['topic']

class LessonPresenceForm(Form):
    def __init__(self, class_id, *args, **kwargs):
        super(LessonPresenceForm, self).__init__(*args, **kwargs)
        students = get_list_or_404(models.Student, classId__pk=class_id)
        self.label_dict = {}
        for student in students:
            self.fields[f'{student.user.first_name} {student.user.last_name}'] = BooleanField()

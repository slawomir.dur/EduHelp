from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields import related
from django.db.models.query import QuerySet
from django.utils import timezone
from colorfield.fields import ColorField
import datetime
import random


# Create your models here.
class Caretaker(models.Model):
    mobile = models.CharField(max_length=13)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    notify_on_grade = models.BooleanField(default=False)
    notify_on_remark = models.BooleanField(default=False)
    notify_on_test = models.BooleanField(default=False)
    notify_on_homework = models.BooleanField(default=False)

    def to_dict(self):
        return {
            "id": self.id,
            "user_id": self.user.id,
            "name": self.name,
            "surname": self.surname,
            "mail": self.mail,
            "mobile": self.mobile,
            "name_surname": self.name + ' ' + self.surname,
            "notify_on_grade": self.notify_on_grade,
            "notify_on_remark": self.notify_on_remark,
            "notify_on_test": self.notify_on_test,
            "notify_on_homework": self.notify_on_homework
        }

    @property
    def name(self):
        return self.user.first_name

    @property
    def surname(self):
        return self.user.last_name

    @property
    def mail(self):
        return self.user.email

    @property
    def email(self):
        return self.user.email

    def get_all_for_class(class_):
        students = Student.objects.filter(classId=class_)
        caretakers = []
        for student in students:
            caretakers.append(student.caretaker)
        return caretakers

    def __str__(self):
        return f'{self.name} {self.surname} ({str(self.user)})'


class Teacher(models.Model):
    mobile = models.CharField(max_length=13)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "name": self.name,
            "surname": self.surname,
            "mail": self.mail,
            "mobile": self.mobile,
            "id": self.id,
            "user_id": self.user.id,
            "name_surname": self.name + ' ' + self.surname
        }

    @property
    def name(self):
        return self.user.first_name

    @property
    def surname(self):
        return self.user.last_name

    @property
    def mail(self):
        return self.user.email

    @property
    def email(self):
        return self.user.email

    def __str__(self):
        return self.name + " " + self.surname

    def is_form_teacher(self, class_id=None):
        if class_id is None:
            classes = Class.objects.filter(form_teacher=self)
            return len(classes) != 0
        else:
            return class_id.form_teacher == self

    def get_courses(self, class_, semester=None):
        if semester is None:
            semester = Semester.get_current_semester()
        return Course.objects.filter(classId=class_, semester=semester, teacher=self)

    def get_classes(self, semester=None, all_classes=False):
        if semester is None:
            semester = Semester.get_current_semester()
        if all_classes:
            courses = Course.objects.filter(semester=semester)
        else:
            courses = Course.objects.filter(semester=semester, teacher=self)
        classes = []
        for course in courses:
            classes.append(course.classId)
        return list(set(classes))

    def get_all_semesters(self):
        courses = Course.objects.filter(teacher=self)
        semesters = []
        for course in courses:
            if course.semester not in semesters:
                semesters.append(course.semester)
        return semesters

    def get_all_years(self):
        semesters = self.get_all_semesters()
        years = []
        for sem in semesters:
            if sem.year not in years:
                years.append(sem.year)
        return years


class Year(models.Model):
    start = models.DateField()
    end = models.DateField(null=True)
    name = models.CharField(max_length=100)

    def to_dict(self):
        return {
            "start": str(self.start),
            "end": str(self.end),
            "name": self.name,
            "id": self.id
        }

    def __str__(self):
        return self.name

    @staticmethod
    def get_current_year():
        today = datetime.date.today()
        return Year.objects.get(start__lt=today, end__gt=today)


class Semester(models.Model):
    start = models.DateField()
    end = models.DateField()
    is_winter = models.BooleanField()
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='semesters')

    def to_dict(self):
        return {
            "start": str(self.start),
            "end": str(self.end),
            "is_winter": self.is_winter,
            "year": self.year.to_dict(),
            "id": self.id,
            "representation": ("zimowy " if self.is_winter else "letni ") + f'({str(self.start)} -> {str(self.end)})'
        }

    @staticmethod
    def get_current_semester():
        year = Year.get_current_year()
        semesters = Semester.objects.filter(year=year)
        today = datetime.date.today()
        return semesters.get(start__lte=today, end__gte=today)

    @staticmethod
    def get_semesters_for_year(year):
        year = Year.objects.get(pk=year)
        return Semester.objects.filter(year=year)

    def __str__(self):
        return str(self.year) + ' ' + ('WINTER' if self.is_winter else 'SUMMER')


class Class(models.Model):
    creation_year = models.DateField(auto_now_add=True)
    name = models.CharField(max_length=5)
    level = models.IntegerField()
    form_teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="their_class")

    def to_dict(self):
        return {
            "creation_year": str(self.creation_year),
            "name": self.name,
            "level": self.level,
            "form_teacher": self.form_teacher.to_dict(),
            "id": self.id,
            "str_name": str(self)
        }

    @property
    def get_students(self) -> QuerySet:
        st = Student.objects.filter(classId=self)
        return st

    def promote(self):
        self.level += 1

    def demote(self):
        self.level -= 1

    def __str__(self) -> str:
        return str(self.level) + self.name


class Student(models.Model):
    classId = models.ForeignKey(Class, on_delete=models.CASCADE, related_name="students")
    caretaker = models.ForeignKey(Caretaker, on_delete=models.CASCADE, related_name="students")
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "surname": self.surname,
            "name_surname": self.name + " " + self.surname,
            "mail": self.mail,
            "user_id": self.user.id,
            "caretaker": self.caretaker.to_dict(),
            "class": self.classId.to_dict()
        }

    @property
    def name(self):
        return self.user.first_name

    @property
    def surname(self):
        return self.user.last_name

    @property
    def mail(self):
        return self.user.email

    @property
    def email(self):
        return self.user.email

    def __str__(self):
        return self.name + " " + self.surname

    def get_remarks(self) -> QuerySet:
        return StudentRemark.objects.filter(student=self)

    def get_courses(self, semester=None) -> QuerySet:
        if semester is None:
            return Course.objects.filter(classId=self.classId, semester_id=Semester.get_current_semester().id)
        else:
            return Course.objects.filter(classId=self.classId, semester__id=semester.id)

    def get_grades_for_course(self, course) -> QuerySet:
        return Grade.objects.filter(course=course, student=self)

    def get_teachers(self, semester: Semester):
        courses = self.get_courses(semester=semester)
        teachers = []
        for course in courses:
            teachers.append(course.teacher)
        return list(set(teachers))

    def get_avg_for_course(self, course) -> float:
        grades = self.get_grades_for_course(course)
        if len(grades) == 0:
            return 0.0
        weight_sum = 0
        value_sum = 0
        for grade in grades:
            weight_sum += grade.type.value
            value_sum += grade.value.value * grade.type.value
        return float("{:.2f}".format(value_sum / weight_sum))

    def expected_grade(self, course) -> int:
        return int(round(self.get_avg_for_course(course)))

    def get_statistics(self, course=None, semester: Semester = None):
        if course is None:
            stats = {}
            courses = self.get_courses(semester)
            for course in courses:
                stats[course.subject.name] = self.get_statistics(course)
            return stats
        else:
            stats = {}
            # presence stats
            lessons = Lesson.objects.filter(course=course)
            present = 0
            try:
                for lesson in lessons:
                    if CoursePresence.objects.get(student=self, lesson=lesson).present:
                        present += 1
            except CoursePresence.DoesNotExist:
                pass
            try:
                stats['presence'] = (present / len(lessons)) * 100
            except ZeroDivisionError:
                stats['presence'] = None

            # this student's average
            stats['average'] = self.get_avg_for_course(course)

            # this student's expected grade
            stats['expected_grade'] = self.expected_grade(course)

            # class average
            students = Student.objects.filter(classId=course.classId)
            avg = 0
            for student in students:
                avg += student.get_avg_for_course(course)
            avg /= len(students)
            stats['class_average'] = avg
            return stats

    def get_all_semesters(self):
        courses = Course.objects.filter(classId=self.classId)
        semesters = []
        for course in courses:
            semesters.append(course.semester)
        return list(set(semesters))

    def get_all_years(self):
        semesters = self.get_all_semesters()
        years = []
        for semester in semesters:
            years.append(semester.year)
        return list(set(years))


class SubjectType(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            "name": self.name,
            "id": self.id
        }


class Subject(models.Model):
    name = models.CharField(max_length=20)
    type = models.ForeignKey(SubjectType, on_delete=models.CASCADE, related_name="subjects")

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "type": self.type.to_dict()
        }


class Course(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name="courses")
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name="courses")
    classId = models.ForeignKey(Class, on_delete=models.CASCADE, related_name="courses")
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, related_name='courses', null=True)

    def __str__(self):
        return f'{str(self.subject)}({self.classId}|{str(self.semester)})'

    def to_dict(self):
        return {
            "subject": self.subject.to_dict(),
            "teacher": self.teacher.to_dict(),
            "class": self.classId.to_dict(),
            "semester": self.semester.to_dict(),
            "id": self.id,
            "name": self.subject.name
        }


class Lesson(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='lessons')
    notes = models.TextField(default='', null=True, blank=True)
    topic = models.CharField(max_length=200)
    date = models.DateField(auto_now_add=True)

    def get_class(self):
        return self.course.classId

    def __str__(self) -> str:
        return f'{self.course}|{self.course.classId}|{self.topic}|{self.date}'

    def to_dict(self):
        return {
            "id": self.id,
            "course": self.course.to_dict(),
            "topic": self.topic,
            "date": str(self.date),
            "notes": self.notes
        }


class LessonTopic(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='lesson_topics')
    level = models.IntegerField()
    topic = models.CharField(max_length=200)

    @staticmethod
    def get_topics_for_subject_level(subject, level):
        return LessonTopic.objects.filter(subject=subject, level=level)

    def to_dict(self):
        return {
            "id": self.id,
            "subject": self.subject.to_dict(),
            "level": self.level,
            "topic": self.topic
        }


class CoursePresence(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, null=True)
    present = models.BooleanField()

    def to_dict(self):
        return {
            "id": self.id,
            "student": self.student.to_dict(),
            "lesson": self.lesson.to_dict(),
            "present": self.present
        }


class GradeValue(models.Model):
    name = models.CharField(max_length=20)
    value = models.IntegerField()

    def __str__(self):
        return self.name + ' (' + str(self.value) + ')'

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "value": self.value
        }


class GradeType(models.Model):
    name = models.CharField(max_length=20)
    value = models.FloatField()
    color = ColorField(default='#000000')

    def __str__(self):
        return self.name + ' (' + str(self.value) + ')'

    def to_dict(self):
        return {
            "id": self.id,
            "value": self.value,
            "color": str(self.color),
            "name": self.name
        }


class Grade(models.Model):
    value = models.ForeignKey(GradeValue, on_delete=models.CASCADE)
    type = models.ForeignKey(GradeType, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name="student")
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="course")
    description = models.TextField(default="", null=True, blank=True)

    def __str__(self):
        return str(self.value)

    def to_dict(self):
        return {
            'id': self.id,
            'value': self.value.to_dict(),
            'type': self.type.to_dict(),
            'student': self.type.to_dict(),
            'course': self.type.to_dict(),
            'description': self.description
        }


class PlannedHomework(models.Model):
    classId = models.ForeignKey(Class, on_delete=models.CASCADE, related_name='homework')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='homework')
    description = models.TextField()
    deadline = models.DateField()

    @staticmethod
    def get_for_month_year(month, year, class_):
        return PlannedHomework.objects.filter(deadline__year=year, deadline__month=month, classId=class_)

    def to_dict(self):
        gen = lambda: random.randint(0, 255)
        r = lambda: format(gen(), 'x')
        return {
            'id': self.id,
            "classId": self.classId.to_dict(),
            "course": self.course.to_dict(),
            "description": self.description,
            "deadline": str(self.deadline),
            "date": self.deadline.strftime('%Y-%m-%d'),
            "title": 'Praca domowa',
            "subtitle": self.course.subject.name,
            "color": f"#{r()}{r()}{r()}"
        }


class PlannedTest(models.Model):
    classId = models.ForeignKey(Class, on_delete=models.CASCADE, related_name='tests')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='tests')
    description = models.TextField()
    date = models.DateField()

    @staticmethod
    def get_for_month_year(month, year, class_):
        return PlannedTest.objects.filter(date__year=year, date__month=month, classId=class_)

    def to_dict(self):
        return {
            'id': self.id,
            "classId": self.classId.to_dict(),
            "course": self.course.to_dict(),
            "description": self.description,
            "date": self.date.strftime('%Y-%m-%d'),
            "title": 'Sprawdzian',
            "subtitle": self.course.subject.name
        }


class StudentRemark(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='remarks')
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    date = models.DateField(auto_now_add=True)

    @staticmethod
    def get_all_of_class(class_: Class):
        students = Student.objects.filter(classId=class_)
        remarks = []
        for student in students:
            student_remarks = StudentRemark.objects.filter(student=student)
            remarks.extend(student_remarks)
        return remarks

    def to_dict(self):
        return {
            "id": self.id,
            "student": self.student.to_dict(),
            "teacher": self.teacher.to_dict(),
            "title": self.title,
            "description": self.description,
            "date": str(self.date)
        }


class SentMessage(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_messages')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    message = models.TextField()
    read = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.sender}->{self.receiver} ({self.title})'

    def to_dict(self):
        return {
            "id": self.id,
            "sender": self.sender.id,
            "sender_name": self.sender.first_name + ' ' + self.sender.last_name,
            "receiver": self.receiver.id,
            "receiver_name": self.receiver.first_name + ' ' + self.receiver.last_name,
            "title": self.title,
            "message": self.message,
            "read": self.read,
            "date": self.date
        }


class ReceivedMessage(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_messages')
    title = models.CharField(max_length=200)
    message = models.TextField()
    read = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.sender}->{self.receiver} ({self.title})'

    def to_dict(self):
        return {
            "id": self.id,
            "sender": self.sender.id,
            "sender_name": self.sender.first_name + ' ' + self.sender.last_name,
            "receiver": self.receiver.id,
            "receiver_name": self.receiver.first_name + ' ' + self.receiver.last_name,
            "title": self.title,
            "message": self.message,
            "read": self.read,
            "date": self.date
        }


def send_message(sender, receiver, title, message):
    try:
        sent = SentMessage()
        sent.sender = sender
        sent.receiver = receiver
        sent.title = title
        sent.message = message
        sent.save()

        received = ReceivedMessage()
        received.sender = sender
        received.receiver = receiver
        received.title = title
        received.message = message
        received.save()
        return True
    except Exception:
        return False

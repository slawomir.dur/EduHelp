from abc import ABC

from rest_framework import serializers
from .models import Caretaker, SentMessage, Teacher, Class, Student, SubjectType, Subject, Course, CoursePresence, GradeValue, GradeType, Grade, PlannedHomework, PlannedTest, StudentRemark

class CaretakerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Caretaker
        fields = ['id', 'name', 'surname', 'mobile', 'mail', 'user', 'notify_on_grade', 'notify_on_test', 'notify_on_homework']


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ['id', 'name', 'surname', 'mobile', 'mail', 'user']


class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = ['id', 'sub', 'form_teacher']


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['id', 'name', 'surname', 'classId', 'caretaker', 'user']


class SubjectTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectType
        fields = ['id', 'name']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'name', 'type']


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'subject', 'teacher', 'classId']

class CoursePresenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoursePresence
        fields = ['id', 'student', 'date', 'course', 'present']

class GradeValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = GradeValue
        fields = ['id', 'name', 'value']


class GradeTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GradeType
        fields = ['id', 'name', 'value']


class GradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = ['id', 'value', 'type', 'student', 'course']


class PlannedHomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlannedHomework
        fields = ['id', 'classId', 'course', 'description', 'deadline']


class PlannedTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlannedTest
        fields = ['id', 'classId', 'course', 'description', 'date']


class StudentRemarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentRemark
        fields = ['id', 'student', 'teacher', 'title', 'description']

class SentMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = SentMessage
        fields = ['id', 'sender', 'receiver', 'title', 'message', 'read', 'date']
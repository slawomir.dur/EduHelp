from django import template
from django.core.checks.messages import ERROR
from .. import models
import math


register = template.Library()


@register.filter
def name(user):
    try:
        return models.Student.objects.get(user=user).name
    except Exception as e:
        return str(e)

@register.filter
def surname(user):
    try:
        return models.Student.objects.get(user=user).surname
    except Exception as e:
        return str(e)


@register.filter
def grades(user, course):
    try:
        student = models.Student.objects.get(user=user)
        return student.get_grades_for_course(course)
    except Exception as e:
        return str(e)


@register.filter
def avg_grade(user, course):
    try:
        student = models.Student.objects.get(user=user)
        return student.get_avg_for_course(course)
    except Exception as e:
        return str(e)


@register.filter
def expected_grade(user, course):
    try:
        student = models.Student.objects.get(user=user)
        return student.expected_grade(course)
    except Exception as e:
        return str(e)

@register.filter
def course_id(course) -> int:
    return course.id

@register.filter
def was_present(user, lesson):
    try:
        student = models.Student.objects.get(user=user)
        presence = models.CoursePresence.objects.get(student=student, lesson=lesson)
        return presence.present
    except:
        return False
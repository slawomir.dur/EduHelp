from django import template
from .. import models

register = template.Library()

@register.filter
def get_courses_with_class(teacher: models.Teacher, class_: models.Class):
    return models.Course.objects.filter(teacher=teacher, classId=class_)

@register.filter
def get_courses_with_class_from_semester(two_arg_pack: tuple, semester_id: int):
    teacher, class_ = two_arg_pack
    semester = models.Semester.objects.get(pk=semester_id)
    return models.Course.objects.filter(teacher=teacher, classId=class_, semester=semester)

@register.filter
def is_form_teacher(teacher: models.Teacher, class_: models.Class = None):
    return teacher.is_form_teacher(class_)

@register.filter
def get_formed_classes(teacher: models.Teacher):
    return models.Class.objects.filter(form_teacher=teacher)
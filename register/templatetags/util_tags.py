from django import template
from django.forms.widgets import HiddenInput
from .. import models
import re

register = template.Library()

@register.filter
def size(item):
    return len(item)

@register.filter
def get_from_index(list, idx: int):
    return list[idx]

@register.filter
def is_in_group(user, group) -> bool:
    return user.groups.filter(name=group).exists()

@register.filter
def get_value(dict: dict, key):
    return dict[key]

@register.filter
def get_user_id(entity):
    return entity.user.id

@register.filter
def is_not_hidden_widget(widget):
    return not widget.subwidgets[0].data['is_hidden']

@register.filter
def shade_color(color: str, shade: float):
    def check_color(color: str):
        if len(color) != 7:
            return False
        return re.match('#([A-F]|[0-9]){6}', color) is not None
    def shade_element(element: str, shade: float):
        val = int(int(element, 16) * shade)
        val = 255 if val > 255 else val
        return format(val,'x')
    if not check_color(color):
        raise RuntimeError("Invalid color")
    first_el = shade_element(color[1:3], shade)
    second_el = shade_element(color[3:5], shade)
    third_el = shade_element(color[5:], shade)
    return f"#{first_el}{second_el}{third_el}"

@register.filter
def add(value, incrementor):
    return value+incrementor

@register.filter
def sub(value, decrementor):
    return value-decrementor

@register.filter
def pack_two_args(arg1, arg2):
    return arg1, arg2
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import IndexView, MessageSentView, MessageView, SentMessageDetail, SentMessageList, StudentClassListView, StudentCourseListView, StudentHomeworkListView, StudentLessonListView, StudentSiteView, StudentTeacherListView, StudentTestsListView, StudentRemarkListView, TeacherCourseList, TeacherCoursePresenceView, TeacherGradeList, TeacherLessonPresenceFormView, TeacherNewLessonView, TeacherSiteView, CaretakerSiteView, \
CaretakerList, CaretakerDetail, TeacherList, TeacherDetail, ClassList, ClassDetail, StudentList, StudentDetail, SubjectTypeList, SubjectTypeDetail, SubjectList, SubjectDetail, CourseList, CourseDetail, GradeValueList, GradeValueDetail, GradeTypeList, GradeTypeDetail, GradeList, GradeDetail, PlannedHomeworkList, PlannedHomeworkDetail, PlannedTestList, PlannedTestDetail, StudentRemarkList, StudentRemarkDetail, \
    CaretakerCreate, TeacherCreate, ClassCreate, StudentCreate, SubjectTypeCreate, SubjectCreate, CourseCreate, CoursePresenceCreate, GradeValueCreate, GradeTypeCreate, GradeCreate, PlannedHomeworkCreate, PlannedTestCreate, StudentRemarkCreate, \
        CaretakerUpdate, TeacherStudentLessonListView, TeacherStudentRemarkListView, TeacherUpdate, ClassUpdate, StudentUpdate, SubjectTypeUpdate, SubjectUpdate, CourseUpdate, CoursePresenceUpdate, GradeValueUpdate, GradeTypeUpdate, GradeUpdate, PlannedHomeworkUpdate, PlannedTestUpdate, StudentRemarkUpdate, \
            CaretakerDelete, TeacherDelete, ClassDelete, get_remarks_for_student, courses_for_student, all_years_for_student, own_id, students_for_caretaker, tests_for_course, homework_for_course, teacher_new_test, teacher_new_homework, get_remarks, add_grade, all_caretakers_for_class, all_grades, StudentDelete, SubjectTypeDelete, SubjectDelete, CourseDelete, CoursePresenceDelete, GradeValueDelete, GradeTypeDelete, GradeDelete, PlannedHomeworkDelete, PlannedTestDelete, StudentRemarkDelete, all_grades_course, all_semesters, all_teachers_for_student, all_teachers, all_years, create_remark, current_semester, current_user, current_year, delete_grade, delete_lesson, edit_lesson, generate_grades_csv, generate_grades_json, generate_grades_xlsx, generate_grades_xml, generate_statistics_csv, generate_statistics_json, generate_statistics_pdf, generate_statistics_xml, generate_statistics_xlsx, get_received_messages, get_remarks_for_class, get_sent_messages, grade_values_types, grades, generate_grades_pdf, TeacherClassList, TeacherLessonList, TeacherCreateLesson, TeacherCourseGradesView, homework, lessons_for_course, semesters_for_year, send_message, teacher_all_classes, teacher_classes, teacher_courses, teacher_new_lesson, teacher_students, teacher_subjects, tests, update_received_message, courses_for_semester  

app_name='register'

urlpatterns = [
    # regular views for application
    path('', IndexView.as_view(), name='index'),
    path('student_site/', StudentSiteView.as_view(), name='student_site'),
    path('student_site/class/', StudentClassListView.as_view(), name='student_class_list'),
    
    path('send_message/<int:receiver_id>/', MessageView.as_view(), name='send_message'),
    path('message_sent/', MessageSentView.as_view(), name='message_sent'),

    path('student_site/teachers/', StudentTeacherListView.as_view(), name='student_teacher_list'),
    path('student_site/courses/', StudentCourseListView.as_view(), name='student_course_list'),
    
    path('gen_grades_pdf/<int:semester_id>/', generate_grades_pdf, name='generate_grades_pdf'),
    path('gen_grades_xml/<int:semester_id>/', generate_grades_xml, name='generate_grades_xml'),
    path('gen_grades_json/<int:semester_id>/', generate_grades_json, name='generate_grades_json'),
    path('gen_grades_csv/<int:semester_id>/', generate_grades_csv, name='generate_grades_csv'),
    path('gen_grades_xlsx/<int:semester_id>/', generate_grades_xlsx, name='generate_grades_xlsx'),
    
    path('gen_stats_pdf/<int:semester_id>/', generate_statistics_pdf, name='generate_statistics_pdf'),
    path('gen_stats_xml/<int:semester_id>/', generate_statistics_xml, name='generate_statistics_xml'),
    path('gen_stats_json/<int:semester_id>/', generate_statistics_json, name='generate_statistics_json'),
    path('gen_stats_csv/<int:semester_id>/', generate_statistics_csv, name='generate_statistics_csv'),
    path('gen_stats_xlsx/<int:semester_id>/', generate_statistics_xlsx, name='generate_statistics_xlsx'),

    path('gen_grades_pdf/<int:user>/', generate_grades_pdf, name='generate_grades_pdf'),
    path('gen_grades_xml/<int:user>/', generate_grades_xml, name='generate_grades_xml'),
    path('gen_grades_json/<int:user>/', generate_grades_json, name='generate_grades_json'),
    path('gen_grades_csv/<int:user>/', generate_grades_csv, name='generate_grades_csv'),
    path('gen_grades_xlsx/<int:user>/', generate_grades_xlsx, name='generate_grades_xlsx'),
    
    path('gen_stats_pdf/<int:user>/', generate_statistics_pdf, name='generate_statistics_pdf'),
    path('gen_stats_xml/<int:user>/', generate_statistics_xml, name='generate_statistics_xml'),
    path('gen_stats_json/<int:user>/', generate_statistics_json, name='generate_statistics_json'),
    path('gen_stats_csv/<int:user>/', generate_statistics_csv, name='generate_statistics_csv'),
    path('gen_stats_xlsx/<int:user>/', generate_statistics_xlsx, name='generate_statistics_xlsx'),

    path('student_site/homework/', StudentHomeworkListView.as_view(), name='student_homework_list'),
    path('student_site/tests/', StudentTestsListView.as_view(), name='student_test_list'),
    path('student_site/remarks/', StudentRemarkListView.as_view(), name='student_remark_list'),
    path('student_site/lessons/<int:course_id>/', StudentLessonListView.as_view(), name='student_lesson_list'),

    path('teacher_site/', TeacherSiteView.as_view(), name='teacher_site'),
    path('teacher_site/classes/', TeacherClassList.as_view(), name='teacher_class_list'),
    path('teacher_site/courses/', TeacherCourseList.as_view(), name='teacher_course_list'),
    path('teacher_site/lessons/<int:course_id>/', TeacherLessonList.as_view(), name='teacher_lesson_list'),
    path('teacher_site/lessons/<int:course_id>/create/', TeacherCreateLesson.as_view(), name='teacher_lesson_create'),
    path('teacher_site/grades/<int:student_id>/', TeacherGradeList.as_view(), name="teacher_student_grade_list"),
    path('teacher_site/lessons_and_presence/<int:course_id>/<int:student_id>/', TeacherStudentLessonListView.as_view(), name='teacher_student_lesson_list'),
    path('teacher_site/remarks/<int:student_id>/', TeacherStudentRemarkListView.as_view(), name='teacher_student_remark_list'),
    path('teacher_site/presence/<int:lesson_id>/', TeacherCoursePresenceView.as_view(), name='teacher_course_presence'),
    path('teacher_site/course/grades/<int:course_id>/', TeacherCourseGradesView.as_view(), name='teacher_course_grades_list'),
    path('teacher_site/lesson/presence/<int:lesson_id>/', TeacherLessonPresenceFormView.as_view(), name='teacher_lesson_presence_set'),
    path('teacher_site/new_lesson/', TeacherNewLessonView.as_view(), name='teacher_new_lesson'),

    path('caretaker_site/', CaretakerSiteView.as_view(), name='caretaker_site'),

    # create db urls
    path('register/create/caretaker/', CaretakerCreate.as_view(), name='create_caretaker'),
    path('register/create/teacher/', TeacherCreate.as_view(), name='create_teacher'),
    path('register/create/class/', ClassCreate.as_view(), name='create_class'),
    path('register/create/student/', StudentCreate.as_view(), name='create_student'),
    path('register/create/subject_type/', SubjectTypeCreate.as_view(), name='create_subject_type'),
    path('register/create/subject/', SubjectCreate.as_view(), name='create_subject'),
    path('register/create/course/', CourseCreate.as_view(), name='create_course'),
    path('register/create/course_presence/', CoursePresenceCreate.as_view(), name='create_course_presence'),
    path('register/create/grade_value/', GradeValueCreate.as_view(), name='create_grade_value'),
    path('register/create/grade_type/', GradeTypeCreate.as_view(), name='create_grade_type'),
    path('register/create/grade/', GradeCreate.as_view(), name='create_grade'),
    path('register/create/planned_homework/', PlannedHomeworkCreate.as_view(), name='create_planned_homework'),
    path('register/create/planned_test/', PlannedTestCreate.as_view(), name='create_planned_test'),
    path('register/create/student_remark/', StudentRemarkCreate.as_view(), name='create_student_remark'),


    # update db urls
    path('register/update/caretaker/<int:pk>', CaretakerUpdate.as_view()),
    path('register/update/teacher/<int:pk>', TeacherUpdate.as_view()),
    path('register/update/class/<int:pk>', ClassUpdate.as_view()),
    path('register/update/student/<int:pk>', StudentUpdate.as_view()),
    path('register/update/subject_type/<int:pk>', SubjectTypeUpdate.as_view()),
    path('register/update/subject/<int:pk>', SubjectUpdate.as_view()),
    path('register/update/course/<int:pk>', CourseUpdate.as_view()),
    path('register/update/course_presence/<int:pk>', CoursePresenceUpdate.as_view()),
    path('register/update/grade_value/<int:pk>', GradeValueUpdate.as_view()),
    path('register/update/grade_type/<int:pk>', GradeTypeUpdate.as_view()),
    path('register/update/grade/<int:pk>', GradeUpdate.as_view()),
    path('register/update/planned_homework/<int:pk>', PlannedHomeworkUpdate.as_view()),
    path('register/update/planned_test/<int:pk>', PlannedTestUpdate.as_view()),
    path('register/update/student_remark/<int:pk>', StudentRemarkUpdate.as_view()),

    # delete db urls
    path('register/delete/caretaker/<int:pk>', CaretakerDelete.as_view()),
    path('register/delete/teacher/<int:pk>', TeacherDelete.as_view()),
    path('register/delete/class/<int:pk>', ClassDelete.as_view()),
    path('register/delete/student/<int:pk>', StudentDelete.as_view()),
    path('register/delete/subject_type/<int:pk>', SubjectTypeDelete.as_view()),
    path('register/delete/subject/<int:pk>', SubjectDelete.as_view()),
    path('register/delete/course/<int:pk>', CourseDelete.as_view()),
    path('register/delete/course_presence/<int:pk>', CoursePresenceDelete.as_view()),
    path('register/delete/grade_value/<int:pk>', GradeValueDelete.as_view()),
    path('register/delete/grade_type/<int:pk>', GradeTypeDelete.as_view()),
    path('register/delete/grade/<int:pk>', GradeDelete.as_view()),
    path('register/delete/planned_homework/<int:pk>', PlannedHomeworkDelete.as_view()),
    path('register/delete/planned_test/<int:pk>', PlannedTestDelete.as_view()),
    path('register/delete/student_remark/<int:pk>', StudentRemarkDelete.as_view()),

    # list views for REST api
    path('register_api/current_user/', current_user, name='api/current_user'),
    path('register_api/current_year/', current_year, name='api/current_year'),
    path('register_api/current_semester/', current_semester, name='api/current_semester'),
    path('register_api/years/', all_years, name='api/all_years'),
    path('register_api/years/<int:student_id>/', all_years_for_student, name='api/all_years_for_student'),
    path('register_api/semesters/', all_semesters, name='api/all_semesters'),
    path('register_api/semesters/<int:year>/', semesters_for_year, name='api/semesters_for_year'),
    path('register_api/courses/', courses_for_semester, name='api/courses_for_semester'),
    path('register_api/courses/<int:semester_id>/', courses_for_semester, name='api/courses_for_semester'),
    path('register_api/grades/', all_grades, name='api/all_grades_from_current_semester'),
    path('register_api/grades/<int:semester_id>/', all_grades, name='api/all_grades_from_semester'),
    path('register_api/grades/<student_id>/<subject>/', grades, name='api/student_grades'),
    path('register_api/teachers/<int:semester_id>/', all_teachers_for_student, name='api/student_teachers'),
    path('register_api/homework/<int:month>/<int:year>/', homework, name='api/student_homework'),
    path('register_api/tests/<int:month>/<int:year>/', tests, name='api/student_tests'),
    path('register_api/homework/<int:course_id>/', homework_for_course, name='api/homework_for_course'),
    path('register_api/tests/<int:course_id>', tests_for_course, name='api/tests_for_course'),
    path('register_api/own_id/', own_id, name='api/own_id'),
    path('register_api/courses_for_student/<int:student_id>/', courses_for_student, name='api/courses_for_student'),
    path('register_api/courses_for_student/<int:student_id>/<int:semester_id>/', courses_for_student, name='api/courses_for_student_with_semester'),

    path('register_api/caretaker/students/', students_for_caretaker, name='api/students_for_caretaker'),

    path('register_api/teacher/all_classes/', teacher_all_classes, name='api/teacher_all_classes'),
    path('register_api/teacher/classes/', teacher_classes, name='api/teacher_classes'),
    path('register_api/teacher/classes/<int:semester_id>/', teacher_classes, name='api/teacher_classes_with_sem'),
    path('register_api/teacher/courses/<int:classId>/', teacher_courses, name='api/teacher_courses'),
    path('register_api/teacher/courses/<int:classId>/<int:semester_id>/', teacher_courses, name='api/teacher_courses_with_semester'),
    path('register_api/teacher/topics/<int:subject_id>/<int:level>/', teacher_subjects, name='api/teacher_topics'),
    path('register_api/teacher/students/<int:class_id>/', teacher_students, name='api/teacher_students'),
    path('register_api/teacher/new_lesson/', teacher_new_lesson, name='api/teacher_new_lesson'),
    path('register_api/teacher/new_homework/', teacher_new_homework, name='api/teacher_new_homework'),
    path('register_api/teacher/new_test/', teacher_new_test, name='api/teacher_new_test'),
    path('register_api/grade_values_types/', grade_values_types, name='api/grade_values_types'),
    path('register_api/lessons/<int:course_id>/', lessons_for_course, name='api/lessons_for_course'),

    path('register_api/lesson/edit/', edit_lesson, name='api/edit_lesson'),
    path('register_api/lesson/delete/', delete_lesson, name='api/delete_lesson'),
    path('register_api/grade/add/', add_grade, name='api/add_grade'),
    path('register_api/grade/delete/', delete_grade, name='api/delete_grade'),
    path('register_api/grades_for_course/<int:course_id>/<int:student_id>/', all_grades_course, name='api/grades_for_course'),
    path('register_api/grades_for_course/<int:course_id>/', all_grades_course, name='api/grades_for_course'),

    path('register_api/all_teachers/', all_teachers, name='api/all_teachers'),
    path('register_api/caretakers/<int:class_id>/', all_caretakers_for_class, name='api/caretakers_for_class'),

    path('register_api/send_message/', send_message, name='api/send_message'),
    path('register_api/received_messages/', get_received_messages, name='api/received_messages'),
    path('register_api/sent_messages/', get_sent_messages, name='api/sent_messages'),
    path('register_api/received_messages/update/', update_received_message, name='api/update_received_message'),

    path('register_api/remarks/', get_remarks, name='api/remarks_for_student'),
    path('register_api/remarks/<int:class_id>/', get_remarks_for_class, name='api/remarks_for_class'),
    path('register_api/remarks_for_student/<int:student_id>/', get_remarks_for_student, name='api/remarks_for_student_id'),
    path('register_api/remark/create/', create_remark, name='api/create_remark'),

    path('register_api/caretakers/', CaretakerList.as_view()),
    path('register_api/teachers/', TeacherList.as_view()),
    path('register_api/classes/', ClassList.as_view()),
    path('register_api/students/', StudentList.as_view()),
    path('register_api/subject_types/', SubjectTypeList.as_view()),
    path('register_api/subjects/', SubjectList.as_view()),
    #path('register_api/courses/', CourseList.as_view()),
    path('register_api/grade_values/', GradeValueList.as_view()),
    path('register_api/grade_types/', GradeTypeList.as_view()),
    path('register_api/grades/', GradeList.as_view()),
    path('register_api/planned_homeworks/', PlannedHomeworkList.as_view()),
    path('register_api/planned_tests/', PlannedTestList.as_view()),
    path('register_api/student_remarks/', StudentRemarkList.as_view()),

    # detail views for REST api
    path('register_api/caretakers/<int:pk>/', CaretakerDetail.as_view()),
    path('register_api/teachers/<int:pk>/', TeacherDetail.as_view()),
    path('register_api/classes/<int:pk>/', ClassDetail.as_view()),
    path('register_api/students/<int:pk>/', StudentDetail.as_view()),
    path('register_api/subject_types/<int:pk>/', SubjectTypeDetail.as_view()),
    path('register_api/subjects/<int:pk>/', SubjectDetail.as_view()),
    path('register_api/courses/<int:pk>/', CourseDetail.as_view()),
    path('register_api/grade_values/<int:pk>/', GradeValueDetail.as_view()),
    path('register_api/grade_types/<int:pk>/', GradeTypeDetail.as_view()),
    path('register_api/grades/<int:pk>/', GradeDetail.as_view()),
    path('register_api/planned_homeworks/<int:pk>/', PlannedHomeworkDetail.as_view()),
    path('register_api/planned_tests/<int:pk>/', PlannedTestDetail.as_view()),
    path('register_api/student_remarks/<int:pk>/', StudentRemarkDetail.as_view()),
    path('api/sent_messages/', SentMessageList.as_view()),
    path('api/sent_message/<int:pk>/', SentMessageDetail.as_view())
]

from datetime import datetime
import re
from django import shortcuts
from django.contrib.auth import base_user
from .templatetags.util_tags import is_in_group, shade_color
from django.forms.widgets import HiddenInput
from .forms import LessonCreateForm, MessageFromToForm, LessonPresenceForm
from re import S
from .files_generation import DocumentGenerator
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404, render
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, edit, ListView
from django.http.response import FileResponse, Http404, HttpResponseBadRequest

from braces.views import GroupRequiredMixin

from rest_framework import generics
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.decorators import api_view

from xml.etree import ElementTree

from .models import LessonTopic, SentMessage, ReceivedMessage, Semester, User, Caretaker, Lesson, Teacher, Class, \
    Student, SubjectType, Subject, Course, CoursePresence, GradeValue, GradeType, Grade, PlannedHomework, PlannedTest, \
    StudentRemark, Year, send_message as send_msg
from .serializers import CaretakerSerializer, SentMessageSerializer, TeacherSerializer, ClassSerializer, StudentSerializer, \
    SubjectTypeSerializer, SubjectSerializer, CourseSerializer, GradeValueSerializer, GradeTypeSerializer, \
    GradeSerializer, PlannedHomeworkSerializer, PlannedTestSerializer, StudentRemarkSerializer
from .templatetags import auth_tags


def is_member(user, group):
    return user.groups.filter(name=group).exists()


# Create your views here.
class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self):
        context = {}
        try:
            context['student'] = Student.objects.get(user=self.request.user)
        except:
            pass
        try:
            context['teacher'] = Teacher.objects.get(user=self.request.user)
        except:
            pass
        try:
            context['caretaker'] = Caretaker.objects.get(user=self.request.user)
        except:
            pass
        return context


class MessageSentView(LoginRequiredMixin, TemplateView):
    template_name = 'sent_message.html'


class StudentSiteView(GroupRequiredMixin, TemplateView):
    group_required = u'Students'
    template_name = 'student_site.html'


class StudentClassListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = Student
    context_object_name = 'class_list'
    template_name = 'student_class_list.html'

    def get_queryset(self):
        user = self.request.user
        return Student.objects.filter(classId__exact=user.student.classId).order_by('user__last_name')


class StudentTeacherListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = Teacher
    context_object_name = 'teacher_list'
    template_name = 'student_teacher_list.html'

    def get_queryset(self):
        print(self.request.user.student.classId)
        courses = Course.objects.filter(classId__exact=self.request.user.student.classId)
        print(courses)
        teachers = []
        for course in courses:
            teachers.append(course.teacher)
        print(teachers)
        teachers = list(set(teachers))
        return teachers

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['class'] = self.request.user.student.classId
        return context


class StudentCourseListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = Course
    context_object_name = 'course_list'
    template_name = 'student_course_list.html'

    def get_queryset(self):
        return Course.objects.filter(classId__exact=self.request.user.student.classId)


def generate_grades_pdf(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_grades_pdf(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_grades.pdf')


def generate_grades_xml(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_grades_xml(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_grades.xml')


def generate_grades_json(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_grades_json(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_grades.json')


def generate_grades_csv(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_grades_csv(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_grades.csv')


def generate_grades_xlsx(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_grades_xlsx(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_grades.xlsx')


def generate_statistics_pdf(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_statistics_pdf(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_statistics.pdf')


def generate_statistics_xml(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_statistics_xml(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_statistics.xml')


def generate_statistics_json(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_statistics_json(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_statistics.json')


def generate_statistics_csv(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_statistics_csv(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_statistics.csv')


def generate_statistics_xlsx(request, semester_id: int, user=None):
    if user is None:
        user = request.user
    else:
        user = get_object_or_404(User, pk=user)
    if not is_in_group(user, 'Students'):
        return HttpResponseBadRequest()
    semester = get_object_or_404(Semester, pk=semester_id)
    student = get_object_or_404(Student, user=user)
    file = DocumentGenerator.generate_statistics_xlsx(student, semester)
    return FileResponse(file, as_attachment=True, filename=f'{user.first_name}_{user.last_name}_statistics.xlsx')


class MessageView(LoginRequiredMixin, edit.FormView):
    template_name = 'send_message.html'
    form_class = MessageFromToForm
    success_url = '/message_sent/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["receiver"] = get_object_or_404(User, pk=self.kwargs['receiver_id'])
        print(context)
        return context

    def form_valid(self, form):
        msg = form.save(commit=False)
        msg.sender = self.request.user
        msg.receiver = get_object_or_404(User, pk=self.kwargs['receiver_id'])
        msg.save()
        return super().form_valid(form)


class StudentLessonListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = Lesson
    context_object_name = 'lesson_list'
    template_name = 'student_lesson_list.html'

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        return course.lessons.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        context['course'] = course
        context['statistics'] = self.request.user.student.get_statistics(course)
        return context


class StudentHomeworkListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = PlannedHomework
    context_object_name = 'homework_list'
    template_name = 'student_homework_list.html'

    def get_queryset(self):
        return PlannedHomework.objects.filter(classId=self.request.user.student.classId).order_by('-deadline')


class StudentTestsListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = PlannedTest
    context_object_name = 'test_list'
    template_name = 'student_test_list.html'

    def get_queryset(self):
        return PlannedTest.objects.filter(classId=self.request.user.student.classId).order_by('-date')


class StudentRemarkListView(GroupRequiredMixin, ListView):
    group_required = u'Students'
    model = StudentRemark
    context_object_name = 'remark_list'
    template_name = 'student_remark_list.html'
    paginate_by = 20

    def get_queryset(self):
        return StudentRemark.objects.filter(student=self.request.user.student)


class TeacherSiteView(GroupRequiredMixin, TemplateView):
    group_required = u'Teachers'
    model = Teacher
    context_object_name = 'teacher'
    template_name = 'teacher_site.html'

    def get_queryset(self):
        return Teacher.objects.get(user=self.request.user.teacher)


class TeacherClassList(GroupRequiredMixin, View):
    group_required = u'Teachers'

    def get(self, request):
        return render(request, 'teacher_class_list.html', {"chosen": 0})

    def post(self, request):
        return render(request, 'teacher_class_list.html', {"chosen": int(request.POST['class_select']) - 1})


class TeacherCreateLesson(GroupRequiredMixin, View):
    group_required = u'Teachers'

    def get(self, request, course_id):
        context = {}
        context['course'] = get_object_or_404(Course, pk=course_id)
        context['form'] = LessonCreateForm()
        return render(request, 'teacher_create_lesson.html', context)

    def post(self, request, course_id):
        form = LessonCreateForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            lesson = Lesson()
            lesson.course = get_object_or_404(Course, pk=course_id)
            lesson.topic = cd['topic']
            lesson.save()
            return render(request, 'teacher_created_lesson.html', {'lesson': lesson})
        return HttpResponseBadRequest()


class TeacherNewLessonView(GroupRequiredMixin, View):
    group_required = u'Teachers'

    def get(self, request):
        return render(request, 'teacher_new_lesson.html', {})


class TeacherLessonPresenceFormView(GroupRequiredMixin, View):
    group_required = u'Teachers'

    def get(self, request, lesson_id):
        context = {}
        lesson = get_object_or_404(Lesson, pk=lesson_id)
        context['heading'] = f'Obecność na lekcji {lesson}'
        context['form'] = LessonPresenceForm(class_id=lesson.course.classId.id)
        return render(request, 'register/general_form.html', context)

    def post(self, request, lesson_id):
        form = LessonPresenceForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            for key, value in cd.items:
                name_list = key.split()
                user = get_object_or_404(User, first_name=name_list[0], last_name=name_list[1])
                presence = CoursePresence()
                presence.lesson = get_object_or_404(Lesson, pk=lesson_id)
                presence.student = user.student
                presence.present = value
                presence.save()
            return render(request, 'teacher_lesson_presence_set_success.html', {})
        return HttpResponseBadRequest()


class TeacherCourseList(GroupRequiredMixin, ListView):
    group_required = u'Teachers'
    model = Course
    context_object_name = 'course_list'
    template_name = 'teacher_course_list.html'

    def get_queryset(self):
        return Course.objects.filter(teacher=self.request.user.teacher)


class TeacherLessonList(GroupRequiredMixin, ListView):
    group_required = u'Teachers'
    model = Lesson
    context_object_name = 'lesson_list'
    template_name = 'teacher_lesson_list.html'

    def get_queryset(self):
        course = Course.objects.get(pk=self.kwargs['course_id'])
        return course.lessons.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        context['lesson_list'] = Lesson.objects.filter(course=course)
        context['course'] = course
        return context


class TeacherGradeList(GroupRequiredMixin, ListView):
    group_required = u'Teachers'
    model = Course
    context_object_name = 'course_list'
    template_name = 'teacher_student_grade_list.html'

    def get_queryset(self):
        return Course.objects.filter(classId__exact=get_object_or_404(Student, pk=self.kwargs['student_id']).classId)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["student"] = get_object_or_404(Student, pk=self.kwargs['student_id'])
        return context


class TeacherStudentLessonListView(GroupRequiredMixin, ListView):
    group_required = u'Teachers'
    model = Lesson
    context_object_name = 'lesson_list'
    template_name = 'teacher_student_lesson_list.html'

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        return course.lessons.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        context['course'] = course
        student = get_object_or_404(Student, pk=self.kwargs['student_id'])
        context['statistics'] = student.get_statistics(course)
        context['student'] = student
        return context


class TeacherStudentRemarkListView(GroupRequiredMixin, ListView):
    group_required = u'Teachers'
    model = StudentRemark
    context_object_name = 'remark_list'
    template_name = 'teacher_student_remark_list.html'
    paginate_by = 20

    def get_queryset(self):
        student = get_object_or_404(Student, pk=self.kwargs['student_id'])
        return StudentRemark.objects.filter(student=student)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["student"] = get_object_or_404(Student, pk=self.kwargs['student_id'])
        return context


class TeacherCoursePresenceView(GroupRequiredMixin, TemplateView):
    group_required = u'Teachers'
    template_name = 'teacher_course_presence.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["lesson"] = get_object_or_404(Lesson, pk=kwargs['lesson_id'])
        context["students"] = Student.objects.filter(classId=context['lesson'].course.classId)
        return context


class TeacherCourseGradesView(GroupRequiredMixin, TemplateView):
    group_required = u'Teachers'
    template_name = 'teacher_course_grades_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['course'] = get_object_or_404(Course, pk=kwargs['course_id'])
        context['student_list'] = Student.objects.filter(classId=context['course'].classId)
        return context


class CaretakerSiteView(GroupRequiredMixin, TemplateView):
    group_required = u'Caretakers'
    template_name = 'caretaker_site.html'


class CaretakerCreate(edit.CreateView):
    model = Caretaker
    fields = ['name', 'surname', 'mobile', 'mail', 'user', 'notify_on_grade', 'notify_on_remark', 'notify_on_test',
              'notify_on_homework']


class TeacherCreate(edit.CreateView):
    model = Teacher
    fields = ['name', 'surname', 'mobile', 'mail', 'user']


class ClassCreate(edit.CreateView):
    model = Class
    fields = ['name', 'level', 'form_teacher']


class StudentCreate(edit.CreateView):
    model = Student
    fields = ['name', 'surname', 'classId', 'caretaker', 'user']


class SubjectTypeCreate(edit.CreateView):
    model = SubjectType
    fields = ['name']


class SubjectCreate(edit.CreateView):
    model = Subject
    fields = ['name', 'type']


class CourseCreate(edit.CreateView):
    model = Course
    fields = ['subject', 'teacher', 'classId']


class CoursePresenceCreate(edit.CreateView):
    model = CoursePresence
    fields = ['student', 'course', 'present']


class GradeValueCreate(edit.CreateView):
    model = GradeValue
    fields = ['name', 'value']


class GradeTypeCreate(edit.CreateView):
    model = GradeType
    fields = ['name', 'value']


class GradeCreate(edit.CreateView):
    model = Grade
    fields = ['value', 'type', 'student', 'course']


class PlannedHomeworkCreate(edit.CreateView):
    model = PlannedHomework
    fields = ['classId', 'course', 'description', 'deadline']


class PlannedTestCreate(edit.CreateView):
    model = PlannedTest
    fields = ['classId', 'course', 'description', 'date']


class StudentRemarkCreate(edit.CreateView):
    model = StudentRemark
    fields = ['student', 'teacher', 'title', 'description']


# Update views

class CaretakerUpdate(edit.UpdateView):
    model = Caretaker
    fields = ['name', 'surname', 'mobile', 'mail', 'user', 'notify_on_grade', 'notify_on_remark', 'notify_on_test',
              'notify_on_homework']
    template_name_suffix = '_update_form'


class TeacherUpdate(edit.UpdateView):
    model = Teacher
    fields = ['name', 'surname', 'mobile', 'mail', 'user']
    template_name_suffix = '_update_form'


class ClassUpdate(edit.UpdateView):
    model = Class
    fields = ['sub', 'form_teacher']
    template_name_suffix = '_update_form'


class StudentUpdate(edit.UpdateView):
    model = Student
    fields = ['name', 'surname', 'classId', 'caretaker', 'user']
    template_name_suffix = '_update_form'


class SubjectTypeUpdate(edit.UpdateView):
    model = SubjectType
    fields = ['name']
    template_name_suffix = '_update_form'


class SubjectUpdate(edit.UpdateView):
    model = Subject
    fields = ['name', 'type']
    template_name_suffix = '_update_form'


class CourseUpdate(edit.UpdateView):
    model = Course
    fields = ['subject', 'teacher', 'classId']
    template_name_suffix = '_update_form'


class CoursePresenceUpdate(edit.UpdateView):
    model = CoursePresence
    fields = ['student', 'course', 'present']
    template_name_suffix = '_update_form'


class GradeValueUpdate(edit.UpdateView):
    model = GradeValue
    fields = ['name', 'value']
    template_name_suffix = '_update_form'


class GradeTypeUpdate(edit.UpdateView):
    model = GradeType
    fields = ['name', 'value']
    template_name_suffix = '_update_form'


class GradeUpdate(edit.UpdateView):
    model = Grade
    fields = ['value', 'type', 'student', 'course']
    template_name_suffix = '_update_form'


class PlannedHomeworkUpdate(edit.UpdateView):
    model = PlannedHomework
    fields = ['classId', 'course', 'description', 'deadline']
    template_name_suffix = '_update_form'


class PlannedTestUpdate(edit.UpdateView):
    model = PlannedTest
    fields = ['classId', 'course', 'description', 'date']
    template_name_suffix = '_update_form'


class StudentRemarkUpdate(edit.UpdateView):
    model = StudentRemark
    fields = ['student', 'teacher', 'title', 'description']
    template_name_suffix = '_update_form'


# delete views
class CaretakerDelete(edit.DeleteView):
    model = Caretaker
    fields = ['name', 'surname', 'mobile', 'mail', 'user', 'notify_on_grade', 'notify_on_remark', 'notify_on_test',
              'notify_on_homework']
    template_name_suffix = '_update_form'


class TeacherDelete(edit.DeleteView):
    model = Teacher
    fields = ['name', 'surname', 'mobile', 'mail', 'user']
    template_name_suffix = '_delete_form'


class ClassDelete(edit.DeleteView):
    model = Class
    fields = ['sub', 'form_teacher']
    template_name_suffix = '_delete_form'


class StudentDelete(edit.DeleteView):
    model = Student
    fields = ['name', 'surname', 'classId', 'caretaker', 'user']
    template_name_suffix = '_delete_form'


class SubjectTypeDelete(edit.DeleteView):
    model = SubjectType
    fields = ['name']
    template_name_suffix = '_delete_form'


class SubjectDelete(edit.DeleteView):
    model = Subject
    fields = ['name', 'type']
    template_name_suffix = '_delete_form'


class CourseDelete(edit.DeleteView):
    model = Course
    fields = ['subject', 'teacher', 'classId']
    template_name_suffix = '_delete_form'


class CoursePresenceDelete(edit.DeleteView):
    model = CoursePresence
    fields = ['student', 'course', 'present']
    template_name_suffix = '_delete_form'


class GradeValueDelete(edit.DeleteView):
    model = GradeValue
    fields = ['name', 'value']
    template_name_suffix = '_delete_form'


class GradeTypeDelete(edit.DeleteView):
    model = GradeType
    fields = ['name', 'value']
    template_name_suffix = '_delete_form'


class GradeDelete(edit.DeleteView):
    model = Grade
    fields = ['value', 'type', 'student', 'course']
    template_name_suffix = '_delete_form'


class PlannedHomeworkDelete(edit.DeleteView):
    model = PlannedHomework
    fields = ['classId', 'course', 'description', 'deadline']
    template_name_suffix = '_delete_form'


class PlannedTestDelete(edit.DeleteView):
    model = PlannedTest
    fields = ['classId', 'course', 'description', 'date']
    template_name_suffix = '_delete_form'


class StudentRemarkDelete(edit.DeleteView):
    model = StudentRemark
    fields = ['student', 'teacher', 'title', 'description']
    template_name_suffix = '_delete_form'


# REST views
@api_view(['GET'])
def current_user(request: Request):
    user = request.user
    if user.is_authenticated:
        return Response({
            'authenticated': True,
            'username': user.username,
            'user_id': user.id,
            'student_id': user.student.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'groups': list(user.groups.values_list('name', flat=True)),
            'email': user.email,
            'date_joined': user.date_joined
        })
    else:
        return Response({
            'authenticated': False
        })


@api_view(['GET'])
def current_year(request: Request):
    if request.user.is_authenticated:
        semester = Semester.get_current_semester()
        if semester is not None:
            year = semester.year
            return Response(year.to_dict())
    return Response({})


@api_view(['GET'])
def current_semester(request: Request):
    if request.user.is_authenticated:
        semester = Semester.get_current_semester()
        if semester is not None:
            return Response(
                semester.to_dict()
            )
    return Response({})


@api_view(['GET'])
def all_years(request: Request):
    if request.user.is_authenticated:
        years = None
        if is_member(request.user, 'Students'):
            years = request.user.student.get_all_years()
        elif is_member(request.user, 'Teachers'):
            years = request.user.teacher.get_all_years()
        elif is_member(request.user, 'Caretakers'):
            raise NotImplemented()
        d = {}
        d['years'] = []
        for year in years:
            d['years'].append(year.to_dict())
        return Response(d)
    else:
        return Response({})


@api_view(['GET'])
def all_years_for_student(request: Request, student_id: int):
    student = get_object_or_404(Student, pk=student_id)
    years = student.get_all_years()
    return Response({'years': [y.to_dict() for y in years]})


@api_view(['GET'])
def own_id(request: Request):
    if request.user.is_authenticated:
        if hasattr(request.user, 'student'):
            return Response({'id': request.user.student.id})
        elif hasattr(request.user, 'teacher'):
            return Response({'id': request.user.teacher.id})
        elif hasattr(request.user, 'caretaker'):
            return Response({'id': request.user.caretaker.id})
    raise RuntimeError('user not logged in')


@api_view(['GET'])
def students_for_caretaker(request: Request):
    if request.user.is_authenticated:
        if hasattr(request.user, 'caretaker'):
            students = request.user.caretaker.students.all()
            students = {'students': [s.to_dict() for s in students]}
            return Response(students)
    raise RuntimeError('user not caretaker')


@api_view(['GET'])
def all_semesters(request: Request):
    if request.user.is_authenticated:
        d = {}
        d['semesters'] = []
        sems = None
        if is_member(request.user, 'Students'):
            sems = request.user.student.get_all_semesters()
        elif is_member(request.user, 'Teachers'):
            sems = request.user.teacher.get_all_semesters()
        elif is_member(request.user, 'Caretakers'):
            raise NotImplemented()
        for semester in sems:
            d['semesters'].append(semester.to_dict())
        return Response(d)
    else:
        return Response({})


@api_view(['GET'])
def semesters_for_year(request: Request, year):
    if request.user.is_authenticated:
        d = {}
        semesters = Semester.get_semesters_for_year(year)
        d['semesters'] = []
        for semester in semesters:
            d['semesters'].append(semester.to_dict())
        return Response(d)
    else:
        return Response({})


@api_view(['GET'])
def all_teachers_for_student(request: Request, semester_id=None):
    if request.user.is_authenticated and auth_tags.has_group(request.user, 'Students'):
        if semester_id is None:
            semester = Semester.get_current_semester()
        else:
            semester = get_object_or_404(Semester, pk=semester_id)
        d = {}
        d["teachers"] = []
        teachers = request.user.student.get_teachers(semester)
        for teacher in teachers:
            d["teachers"].append(
                {
                    "name": teacher.name,
                    "surname": teacher.surname,
                    "id": teacher.id,
                    "user_id": teacher.user.id,
                    "is_form": teacher.is_form_teacher(request.user.student.classId),
                    "courses": [course.to_dict() for course in
                                teacher.get_courses(request.user.student.classId, semester)]
                }
            )
        return Response(d)
    return Response({})


@api_view(['GET'])
def courses_for_semester(request: Request, semester_id=None):
    if request.user.is_authenticated and auth_tags.has_group(request.user, 'Students'):
        if semester_id is None:
            semester = Semester.get_current_semester()
        else:
            semester = get_object_or_404(Semester, pk=semester_id)
        courses = request.user.student.get_courses(semester)
        d = {'courses': [course.to_dict() for course in courses]}
        return Response(d)
    return Response({})


@api_view(['GET'])
def courses_for_student(request: Request, student_id, semester_id=None):
    student = get_object_or_404(Student, pk=student_id)
    if semester_id is None:
        semester = Semester.get_current_semester()
    else:
        semester = get_object_or_404(Semester, pk=semester_id)
    courses = student.get_courses(semester)
    return Response({'courses': [c.to_dict() for c in courses]})


@api_view(['GET'])
def all_grades(request: Request, semester_id=None):
    if request.user.is_authenticated and auth_tags.has_group(request.user, 'Students'):
        if semester_id is None:
            semester = Semester.get_current_semester()
        else:
            semester = get_object_or_404(Semester, pk=semester_id)
        d = {}
        courses = request.user.student.get_courses(semester)
        for course in courses:
            grades = request.user.student.get_grades_for_course(course)
            avg = request.user.student.get_avg_for_course(course)
            expected_grade = request.user.student.expected_grade(course)
            d[str(course.subject)] = {
                'course_id': course.id,
                'teacher': str(course.teacher),
                'average': avg,
                'expected_grade': expected_grade,
                'grades': []
            }
            for grade in grades:
                d[str(course.subject)]['grades'].append({
                    'grade_id': grade.id,
                    'value': grade.value.value,
                    'name': grade.value.name,
                    'type': grade.type.name,
                    'weight': grade.type.value,
                    'color': grade.type.color,
                    'darkerColor': shade_color(grade.type.color, 1.1),
                    'lighterColor': shade_color(grade.type.color, 0.1)
                })
        return Response(d)
    return Response({})


@api_view(['GET'])
def grades(request: Request, student_id, subject):
    return Response({
        'id': student_id,
        'subject': subject
    })


@api_view(['GET'])
def get_remarks(request: Request):
    if request.user.is_authenticated:
        if is_member(request.user, 'Students'):
            remarks = request.user.student.remarks.all()
            d = {'remarks': [remark.to_dict() for remark in remarks]}
            return Response(d)
    return Response()


@api_view(['GET'])
def get_remarks_for_student(request: Request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    remarks = student.remarks.all()
    return Response({'remarks': [r.to_dict() for r in remarks]})


@api_view(['GET'])
def homework(request: Request, month, year):
    if request.user.is_authenticated:
        hw = PlannedHomework.get_for_month_year(month, year, request.user.student.classId)
        d = {}
        d["homework"] = []
        for w in hw:
            d["homework"].append(w.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def tests(request: Request, month, year):
    if request.user.is_authenticated:
        hw = PlannedTest.get_for_month_year(month, year, request.user.student.classId)
        d = {}
        d["tests"] = []
        for w in hw:
            d["tests"].append(w.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def teacher_classes(request: Request, semester_id=None):
    if request.user.is_authenticated:
        classes = request.user.teacher.get_classes(semester_id)
        d = {}
        d['classes'] = []
        for cl in classes:
            d['classes'].append(cl.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def teacher_all_classes(request: Request, semester_id=None):
    if request.user.is_authenticated:
        classes = request.user.teacher.get_classes(semester_id, all_classes=True)
        d = {}
        d['classes'] = []
        for cl in classes:
            d['classes'].append(cl.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def teacher_courses(request: Request, classId, semester_id=None):
    if request.user.is_authenticated:
        cl = get_object_or_404(Class, pk=classId)
        semester = None if semester_id is None else get_object_or_404(Semester, pk=semester_id)
        courses = request.user.teacher.get_courses(cl, semester)
        d = {'courses': [course.to_dict() for course in courses]}
        return Response(d)
    return Response({})


@api_view(['GET'])
def teacher_subjects(request: Request, subject_id, level):
    if request.user.is_authenticated:
        subject = get_object_or_404(Subject, pk=subject_id)
        topics = LessonTopic.get_topics_for_subject_level(subject, level)
        d = {}
        d['topics'] = []
        for topic in topics:
            d['topics'].append(topic.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def teacher_students(request: Request, class_id):
    if request.user.is_authenticated:
        cl = get_object_or_404(Class, pk=class_id)
        students = Student.objects.filter(classId=cl)
        d = {}
        d['students'] = []
        for student in students:
            d['students'].append(student.to_dict())
        return Response(d)
    return Response({})


@api_view(['POST'])
def teacher_new_lesson(request: Request):
    if request.user.is_authenticated:
        print(request.data)

        lesson = Lesson()
        lesson.course = get_object_or_404(Course, pk=request.data['course']['id'])
        lesson.topic = request.data['topic']
        lesson.notes = request.data['notes']
        lesson.save()

        for key, value in request.data['presence'].items():
            presence = CoursePresence()
            presence.student = get_object_or_404(Student, pk=key)
            presence.lesson = lesson
            presence.present = value
            presence.save()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def teacher_new_homework(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        homework = PlannedHomework()
        homework.classId = get_object_or_404(Class, pk=request.data['classId'])
        homework.course = get_object_or_404(Course, pk=request.data['course'])
        homework.description = request.data['description']
        homework.deadline = datetime.strptime(request.data['deadline'], '%a %b %d %Y')
        homework.save()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def teacher_new_test(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        test = PlannedTest();
        test.classId = get_object_or_404(Class, pk=request.data['classId'])
        test.course = get_object_or_404(Course, pk=request.data['course'])
        test.description = request.data['description']
        test.date = datetime.strptime(request.data['date'], '%a %b %d %Y')
        test.save()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def edit_lesson(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        lesson = get_object_or_404(Lesson, pk=request.data['id'])
        lesson.topic = request.data['topic']
        lesson.date = request.data['date']
        lesson.save()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def delete_lesson(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        print(request.data)
        lesson = get_object_or_404(Lesson, pk=request.data['id'])
        lesson.delete()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def add_grade(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        print(request.data)
        grade = Grade() if 'grade_id' not in request.data else get_object_or_404(Grade, pk=request.data['grade_id'])
        grade_value = get_object_or_404(GradeValue, pk=request.data['grade']['id'])
        grade_type = get_object_or_404(GradeType, pk=request.data['type']['id'])
        student = get_object_or_404(Student, pk=request.data['student']['id'])
        course = get_object_or_404(Course, pk=request.data['course']['id'])
        description = request.data['description']
        grade.value = grade_value
        grade.type = grade_type
        grade.student = student
        grade.course = course
        grade.description = description
        grade.save()
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def delete_grade(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        print(request.data)
        if 'grade_id' not in request.data:
            raise RuntimeError('no grade id in request')
        grade = get_object_or_404(Grade, pk=request.data['grade_id'])
        grade.delete()
        return Response(request.data)
    return Response({})


@api_view(['GET'])
def get_received_messages(request: Request):
    if request.user.is_authenticated:
        messages = ReceivedMessage.objects.filter(receiver=request.user).order_by('-date')
        d = {'received': []}
        for message in messages:
            d['received'].append(message.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def get_sent_messages(request: Request):
    if request.user.is_authenticated:
        messages = SentMessage.objects.filter(sender=request.user).order_by('-date')
        d = {'sent': []}
        for message in messages:
            d['sent'].append(message.to_dict())
        return Response(d)
    return Response({})


@api_view(['POST'])
def send_message(request: Request):
    if request.user.is_authenticated:
        sender = request.user
        receiver = get_object_or_404(User, pk=int(request.data['addressee']['user_id']))
        title = request.data['title']
        text = request.data['text']
        send_msg(sender, receiver, title, text)
        return Response(request.data)
    return Response({})


@api_view(['POST'])
def update_received_message(request: Request):
    if request.user.is_authenticated:
        message = get_object_or_404(ReceivedMessage, pk=request.data['id'])
        message.read = request.data['read']
        message.save()
        return Response(request.data)
    return Response({})


@api_view(['GET'])
def grade_values_types(request: Request):
    d = {}
    d['values'] = []
    d['types'] = []
    values = GradeValue.objects.all()
    types = GradeType.objects.all()
    for value in values:
        d['values'].append(value.to_dict())
    for type in types:
        d['types'].append(type.to_dict())
    return Response(d)


@api_view(['GET'])
def all_grades_course(request: Request, course_id, student_id=None):
    if request.user.is_authenticated:
        d = {'grades': []}
        course = get_object_or_404(Course, pk=course_id)
        if is_member(request.user, 'Teachers'):
            students = Student.objects.filter(classId=course.classId)
            for student in students:
                grades = Grade.objects.filter(student=student, course=course)
                grades_ = []
                for grade in grades:
                    grades_.append(grade.to_dict())
                d['grades'].append({
                    'course': course.to_dict(),
                    'student': student.to_dict(),
                    'grades': grades_,
                    'average': student.get_avg_for_course(course),
                    'expected_grade': student.expected_grade(course)
                })
            return Response(d)
        elif is_member(request.user, 'Students'):
            student = request.user.student
            grades = Grade.objects.filter(student=student, course=course)
            grades = [grade.to_dict() for grade in grades]
            d['grades'].append({
                'course': course.to_dict(),
                'student': student.to_dict(),
                'grades': grades,
                'average': student.get_avg_for_course(course),
                'expected_grade': student.expected_grade(course)
            })
            return Response(d)
        elif is_member(request.user, 'Caretakers'):
            student = get_object_or_404(Student, pk=student_id)
            grades = Grade.objects.filter(student=student, course=course)
            grades = [grade.to_dict() for grade in grades]
            d['grades'].append({
                'course': course.to_dict(),
                'student': student.to_dict(),
                'grades': grades,
                'average': student.get_avg_for_course(course),
                'expected_grade': student.expected_grade(course)
            })
            return Response(d)
    return Response({})


@api_view(['GET'])
def all_caretakers_for_class(request: Request, class_id: int):
    if request.user.is_authenticated:
        class_ = get_object_or_404(Class, pk=class_id)
        caretakers = Caretaker.get_all_for_class(class_)
        d = {'caretakers': []}
        for caretaker in caretakers:
            d['caretakers'].append(caretaker.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def all_teachers(request: Request):
    if request.user.is_authenticated:
        teachers = Teacher.objects.all()
        d = {'teachers': []}
        for teacher in teachers:
            d['teachers'].append(teacher.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def lessons_for_course(request: Request, course_id: int):
    if request.user.is_authenticated:
        course = get_object_or_404(Course, pk=course_id)
        lessons = Lesson.objects.filter(course=course)
        d = {}
        d["lessons"] = []
        for lesson in lessons:
            d["lessons"].append(lesson.to_dict())
        return Response(d)
    return Response({})


@api_view(['GET'])
def homework_for_course(request: Request, course_id: int):
    if request.user.is_authenticated:
        course = get_object_or_404(Course, pk=course_id)
        homework = course.homework.all()
        homework = {'homework': [h.to_dict() for h in homework]}
        return Response(homework)
    return Response({})


@api_view(['GET'])
def tests_for_course(request: Request, course_id: int):
    if request.user.is_authenticated:
        course = get_object_or_404(Course, pk=course_id)
        tests = course.tests.all()
        tests = {'tests': [t.to_dict() for t in tests]}
        return Response(tests)
    return Response({})


@api_view(['GET'])
def get_remarks_for_class(request: Request, class_id):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        class_ = get_object_or_404(Class, pk=class_id)
        remarks = StudentRemark.get_all_of_class(class_)
        d = {'remarks': []}
        for remark in remarks:
            d['remarks'].append(remark.to_dict())
        return Response(d)
    return Response({})


@api_view(['POST'])
def create_remark(request: Request):
    if request.user.is_authenticated and is_member(request.user, 'Teachers'):
        teacher = request.user.teacher
        student = get_object_or_404(Student, pk=request.data['student']['id'])
        title = request.data['title']
        description = request.data['description']
        remark = StudentRemark()
        remark.student = student
        remark.teacher = teacher
        remark.title = title
        remark.description = description
        remark.save()
        return Response(request.data)
    return Response({})


class CaretakerList(generics.ListCreateAPIView):
    queryset = Caretaker.objects.all()
    serializer_class = CaretakerSerializer


class CaretakerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Caretaker.objects.all()
    serializer_class = CaretakerSerializer


class TeacherList(generics.ListCreateAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class TeacherDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class ClassList(generics.ListCreateAPIView):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer


class ClassDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer


class StudentList(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class SubjectTypeList(generics.ListCreateAPIView):
    queryset = SubjectType.objects.all()
    serializer_class = SubjectTypeSerializer


class SubjectTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SubjectType.objects.all()
    serializer_class = SubjectTypeSerializer


class SubjectList(generics.ListCreateAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SubjectDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class CourseList(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class GradeValueList(generics.ListCreateAPIView):
    queryset = GradeValue.objects.all()
    serializer_class = GradeValueSerializer


class GradeValueDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = GradeValue.objects.all()
    serializer_class = GradeValueSerializer


class GradeTypeList(generics.ListCreateAPIView):
    queryset = GradeType.objects.all()
    serializer_class = GradeTypeSerializer


class GradeTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = GradeType.objects.all()
    serializer_class = GradeTypeSerializer


class GradeList(generics.ListCreateAPIView):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer


class GradeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer


class PlannedHomeworkList(generics.ListCreateAPIView):
    queryset = PlannedHomework.objects.all()
    serializer_class = PlannedHomeworkSerializer


class PlannedHomeworkDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PlannedHomework.objects.all()
    serializer_class = PlannedHomeworkSerializer


class PlannedTestList(generics.ListCreateAPIView):
    queryset = PlannedTest.objects.all()
    serializer_class = PlannedTestSerializer


class PlannedTestDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PlannedTest.objects.all()
    serializer_class = PlannedTestSerializer


class StudentRemarkList(generics.ListCreateAPIView):
    queryset = StudentRemark.objects.all()
    serializer_class = StudentRemarkSerializer


class StudentRemarkDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StudentRemark.objects.all()
    serializer_class = StudentRemarkSerializer

class SentMessageList(generics.ListCreateAPIView):
    queryset = SentMessage.objects.all()
    serializer_class = SentMessageSerializer

class SentMessageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SentMessage.objects.all()
    serializer_class = SentMessageSerializer
